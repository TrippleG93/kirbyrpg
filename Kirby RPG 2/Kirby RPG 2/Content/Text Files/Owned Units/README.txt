This folder holds all the unit information that are in the current
Dungeon. This only happens when the player exists the game
and the dungeon has not been cleared yet. It will hold all
of the enemies that are still alive and their stats

Reading from file it goes by the following:

String: Name
int: level
int: Max Hit points 
int: Hit points remaining
int: attack points
int: special attack points
int: defence points
int: special defence Points
int: speed points