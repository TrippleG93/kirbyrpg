This text file will be read the following way:

string: Name
int: Hit Point stat growth
int: Attack stat growth
int: Special Attack stat growth
int: Defence stat growth
int: Special Defence stat growth
int: Speed stat growth

The growth of the unit will go by the following.
- A random number from 1 - 100 will be generated
- Based on the growth stat it will determine what the random 
  Number will be modded by
- If the growth stat is 4 (Max) then that random number
  will be modded by 4 to determine its stat boost