#pragma once

#include <string>

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"

class Player;
class Unit;

class HeroSelect
{
public:
	HeroSelect(Player &player);

	~HeroSelect();

	/*  SetUpAnimations
	 *  Loads everything needed for the class in regards to what is
	 *  Seen on the screen
	 */
	void SetUpAnimations();

	/* RenderHeroSelect
	 * Draws the hero select screen 
	 */
	void RenderHeroSelect(float scale_x, float scale_y);

	/*  UpdateHeroSelect
	 *  Updates the animations for the hero select menu
	 */
	void UpdateHeroSelect();

	/* MoveCurosrLeft
	 * Moves the cursor left if it not on the edge
	 * by subtracting the value from its position
	 */
	void MoveCursorLeft();

	/* MoveCursorRight
	 * Moves the cursor right if it not on the edge
	 * by adding the value from its position
	 */
	void MoveCursorRight();

	/* SelectHero
	 * Create the unit to display on the team selection
	 * Also create the unit to be used when going into the dungeon
	 */
	int SelectHero();

private:

	int cursor_choice;												// Holds which unit is currently selected
	int cursor_position;											// Holds the current position of the cursor
	int hero_team_count;											// Keeps track of how many heroes are selected for the team (Max 4)
	int max_choice;													// Holds the max amount of hero unlocked
	bool team_selected;												// When true it will double check if thats the team they want

	Render background;												// Holds the image of the background screen
	Render hero_stand_image;										// Holds the image of the hero stand
	Render cursor_image;											// Holds the image of the cursor

	SpriteAnimation background_crowd_animation;						// Holds the animation of the background crowd for the screen
	SpriteAnimation hero_stands_animation;							// Holds the animation of the stands the hero units will be on when chosen
	SpriteAnimation cursor_animation;								// Holds the animation of the cursor 

	Audio moving_cursor;											// Holds the sound effect for when you move the cursor
	Audio select_option;											// Holds the sound effect for when you select what option you want
		
	std::map <int, Unit*> unlocked_heroes;							// Holds the units that have been unlocked so the l=player can choose which to use
	std::map <int, Unit*> hero_team;								// Holds which heroes are going out for the journey
};

