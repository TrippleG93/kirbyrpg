#pragma once

#include <string>

#include "allegro5\allegro_audio.h"
#include "allegro5\allegro_acodec.h"

class Audio
{
public:
	Audio();
	~Audio();


	/*
	* LoadAudioFiles
	* Loads the audio file
	* audio_name - used to store the audio type associated with the name in the mapping variable
	* file_path - used to load the audio file
	*/
	void LoadAudioFiles(std::string file_path);

	/*
	* PlaySongLoop
	* Plays the audio file over and over again
	* audio_name - used to located the audio file in the mapping variable
	*/
	void PlaySongLoop();

	/*
	* PlaySongOnce
	* Plays the audio file once
	* audio_name - used to locate the audio file in the mapping variable
	*/
	void PlaySongOnce();

	/*
	* StopSong
	* Stos the audio file from playing
	* audio_name - used to find the audio file to in the mapping variable to stop playing
	*/
	void StopSong();

	/*
	* StopAllSound
	* Terminates all sample files that were currently playing
	*/
	void StopAllSound();

	/*
	* DestroyAudios
	* Destory all the audio sample variables that was created for the audio
	* list_to_destroy - holds all the audio file names that locate the audio files to be destroyed
	*/
	void DestroyAudio();

private:

	ALLEGRO_SAMPLE* audio_sample;							// Map variable that takes in the name of the audio and the audio itself																	
	ALLEGRO_SAMPLE_ID audio_sample_id;						// Map variable that takes in name of the audio and numericle value of of file
};


