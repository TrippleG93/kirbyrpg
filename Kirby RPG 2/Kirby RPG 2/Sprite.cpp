#include "Sprite.h"

Sprite::Sprite()
{

}

Sprite::Sprite(int position_x, int position_y, int source_x, int source_y, int width, int height) :
	position_x(position_x),
	position_y(position_y),
	source_x(source_x),
	source_y(source_y),
	width(width),
	height(height)
{
}


Sprite::~Sprite()
{
}

void Sprite::SetPositionX(int position_x)
{
	this->position_x = position_x;
}

void Sprite::SetPositionY(int position_y)
{
	this->position_y = position_y;
}
