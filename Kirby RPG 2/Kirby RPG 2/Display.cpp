#include "Display.h"
#include <stdio.h>

Display::Display()
{									
	CreateDisplay(1280, 720);
	SetDisplayFlags();
	SetWindowTitle("Kirby RPG");
	DisplayWindowMode();
	//DisplayFullScreenWindowMode();
	display_scale_x = (float)GetDisplayWidth() / (float)1280;
	display_scale_y = (float)GetDisplayHeight() / (float)720;
}


Display::~Display()
{
	Destroy();
}

void Display::SetDisplayFlags()
{
	al_set_new_display_flags(ALLEGRO_WINDOWED);
	al_set_new_display_flags(ALLEGRO_RESIZABLE);
	//al_set_new_display_flags(ALLEGRO_MAXIMIZED);
	al_set_new_display_flags(ALLEGRO_FULLSCREEN_WINDOW);
	//al_set_new_display_flags(ALLEGRO_FULLSCREEN);
}

void Display::DisplayFullScreenWindowMode()
{
	if (al_set_display_flag(display, ALLEGRO_FULLSCREEN_WINDOW, true))
	{
		printf("Allegro Full Screen Window Sucess!\n");
	}
	if (al_set_display_flag(display, ALLEGRO_FULLSCREEN, false))
	{
		printf("Allegro Full Screen Off!\n");
	}
	if (al_set_display_flag(display, ALLEGRO_WINDOWED, false))
	{
		printf("Allegro Window Screen Off!\n");
	}
	if (al_set_display_flag(display, ALLEGRO_RESIZABLE, false))
	{
		printf("Allegro Resizable Off!\n");
	}
	if (al_set_display_flag(display, ALLEGRO_MAXIMIZED, false))
	{
		printf("Allegro Maximized Off!\n");
	}
}

void Display::DisplayFullScreenMode()
{
	if (al_set_display_flag(display, ALLEGRO_FULLSCREEN, true))
	{
		printf("Allegro Full Screen Sucess!\n");
	}
	if (al_set_display_flag(display, ALLEGRO_FULLSCREEN_WINDOW, false))
	{
		printf("Allegro Full Screen Window Off!\n");
	}
	if (al_set_display_flag(display, ALLEGRO_WINDOWED, false))
	{
		printf("Allegro Window Screen Off!\n");
	}
	if (al_set_display_flag(display, ALLEGRO_RESIZABLE, false))
	{
		printf("Allegro Resizable Off!\n");
	}
	if (al_set_display_flag(display, ALLEGRO_MAXIMIZED, false))
	{
		printf("Allegro Maximized Off!\n");
	}
}

void Display::DisplayWindowMode()
{
	if (al_set_display_flag(display, ALLEGRO_FULLSCREEN, false))
	{
		printf("Allegro Full Screen Off!\n");
	}
	if (al_set_display_flag(display, ALLEGRO_FULLSCREEN_WINDOW, false))
	{
		printf("Allegro Full Screen Window Off!\n");
	}
	if (al_set_display_flag(display, ALLEGRO_WINDOWED, true))
	{
		printf("Allegro Window Screen Sucess!\n");
	}
	if (al_set_display_flag(display, ALLEGRO_RESIZABLE, true))
	{
		printf("Allegro Resizable Sucess!\n");
	}
	if (al_set_display_flag(display, ALLEGRO_MAXIMIZED, false))
	{
		printf("Allegro Maximized Sucess!\n");
	}
}

void Display::SetDisplayPosition(int position_x, int position_y)
{
	al_set_window_position(display, position_x, position_y);
}

void Display::CreateDisplay(int width, int height)
{
	display = al_create_display(width, height);
}

void Display::Destroy()
{
	al_destroy_display(display);
}

int Display::GetDisplayWidth()
{
	return al_get_display_width(display);
}

int Display::GetDisplayHeight()
{
	return al_get_display_height(display);
}

void Display::SetWindowTitle(std::string name)
{
	al_set_window_title(display, name.c_str());
}

void Display::SetNewWindowTitle(std::string name)
{
	al_set_new_window_title(name.c_str());
}

void Display::FlipDisplay()
{
	al_flip_display();
	al_clear_to_color(al_map_rgb(0, 0, 0));
}