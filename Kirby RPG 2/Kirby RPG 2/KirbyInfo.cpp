#include "KirbyInfo.h"

#include "Player.h"
#include "Stats.h"


KirbyInfo::KirbyInfo()
{

}

KirbyInfo::KirbyInfo(Player &player)
{
	choice = 1;
	scroll_index = 1;

	SetUpAnimation();
	LoadStats(player);
}


KirbyInfo::~KirbyInfo()
{
	for (auto const& x : unlocked_kirbies)
	{
		delete(unlocked_kirbies[x.first]);
	} // End for loop
}

void KirbyInfo::SetUpAnimation()
{
	unkown_kirby_image.LoadBitmap("Content/Miscellaneous/SNES - Kirby Super Star Kirbys Fun Pak - Great Cave Offensive Congratulations Screen.png");

	unknown_kirby_animation.AddIndividualFrame(0, 0, 35, 34, 192, 152);
	unknown_kirby_animation.AddIndividualFrame(0, 0, 294, 34, 192, 152);
	unknown_kirby_animation.AddIndividualFrame(0, 0, 553, 34, 192, 152);
	unknown_kirby_animation.AddIndividualFrame(0, 0, 35, 261, 192, 152);
	unknown_kirby_animation.AddIndividualFrame(0, 0, 294, 261, 192, 152);
	unknown_kirby_animation.AddIndividualFrame(0, 0, 553, 261, 192, 152);
	unknown_kirby_animation.AddIndividualFrame(0, 0, 35, 488, 192, 152);
	unknown_kirby_animation.AddIndividualFrame(0, 0, 294, 488, 192, 152);
	unknown_kirby_animation.AddIndividualFrame(0, 0, 553, 488, 192, 152);
	unknown_kirby_animation.CombineFrames(5);

	cursor_image.LoadBitmap("Content/Miscellaneous/SNES - Kirby Super Star Kirbys Fun Pak - Game Over & Continue Screens.png");

	cursor_animation.AddIndividualFrame(200, 300, 14, 269, 31, 13);
	cursor_animation.CombineFrames(1);

	background_image.LoadBitmap("Content/Backgrounds/layers/parallax-mountain-bg.png");

	vincentio_font36.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 36);

	back_out_text_image.CreateTextRender(vincentio_font36.ObtainFont(), al_map_rgb(255, 255, 255), "Backspace to Main Menu");

	moving_cursor.LoadAudioFiles("Content/Sound Effects/Moving Cursor.wav");
	select_option.LoadAudioFiles("Content/Sound Effects/menu-select.wav");

}

void KirbyInfo::LoadStats(Player &player)
{
	for (int index_type = 1; index_type < player.GetMaxKirbyTypes() + 1; index_type++)
	{
		if (player.DidPlayerUnlockKirby(index_type) == true)
		{
			unlocked_kirbies[index_type] = new Stats(false, index_type);
		}
	}
}

void KirbyInfo::RenderKirbyInfo(Player &player, float scale_x, float scale_y)
{
	background_image.DrawScaledImage(0, 0, 272, 160, 0, 0, 272 * 4.705 * scale_x, 160 * 4.5 * scale_y, NULL);

	back_out_text_image.DrawScaledImage(0, 0,
		back_out_text_image.GetBitmapWidth(),
		back_out_text_image.GetBitmapHeight(),
		800 * scale_x, 650 * scale_y,
		(float)back_out_text_image.GetBitmapWidth() * scale_x,
		(float)back_out_text_image.GetBitmapHeight() * scale_y,
		NULL);

	int box_count = 1;												// Holds the amount of boxes that have been made
	int temp_type = scroll_index;									// Holds the current kirby type
	float position_y = 150;											// Position x of the image when drawn
	float position_x = 150;											// Position y of the image when drawn

	while (box_count < 7)											// keep looping until 6 boxes are made or
	{
		if (temp_type > player.GetMaxKirbyTypes())					// If there are no other kirby types to compare
		{
			break;													// Leave the while loop
		}

		if (player.DidPlayerUnlockKirby(temp_type) == true)			// If the player has unlocked the kirby
		{
			// DRAW THE STATS OF THE KIRBY HERE
			unlocked_kirbies[temp_type]->RenderKirbyInfo(position_x, position_y, scale_x, scale_y);
		}
		else
		{															// Else draw the locked box
			unkown_kirby_image.DrawScaledImage(
				unknown_kirby_animation.GetCurrentFrame().GetSourceX(),
				unknown_kirby_animation.GetCurrentFrame().GetSourceY(),
				unknown_kirby_animation.GetCurrentFrame().GetWidth(),
				unknown_kirby_animation.GetCurrentFrame().GetHeight(),
				position_x * scale_x,
				position_y * scale_y,
				(float) unknown_kirby_animation.GetCurrentFrame().GetWidth() * scale_x,
				(float) unknown_kirby_animation.GetCurrentFrame().GetHeight() * scale_y,
				NULL);
		}

		position_x += 400;											// Incease the x_position for the next box to be drawn

		if (temp_type % 3 == 0)										// After going through every three types
		{
			position_y += 300;										// Push the next three group image location down
			position_x = 150;										// Reset the x_position of the box because its a new row
		}

		box_count += 1;												// Increment how many boxes have been made
		temp_type += 1;												// Increment to the next kirby type to check if unlocked
	}

	cursor_image.DrawScaledImage(cursor_animation.GetCurrentFrame().GetSourceX(),
		cursor_animation.GetCurrentFrame().GetSourceY(),
		cursor_animation.GetCurrentFrame().GetWidth(),
		cursor_animation.GetCurrentFrame().GetHeight(),
		(float) (cursor_animation.GetCurrentFrame().GetPositionX() * scale_x) + ((choice - 1) * 400 * scale_x),
		(float) cursor_animation.GetCurrentFrame().GetPositionY() * scale_y,
		(float) cursor_animation.GetCurrentFrame().GetWidth() * scale_x * 3,
		(float) cursor_animation.GetCurrentFrame().GetHeight() * scale_y * 3,
		NULL);
}

void KirbyInfo::UpdateKirbyInfo()
{
	unknown_kirby_animation.Update();
	for (auto const& x : unlocked_kirbies)
	{
		unlocked_kirbies[x.first]->UpdateAnimation();
	} // End for loop
}

void KirbyInfo::MoveCursorLeft()
{
	if (choice - 1 > 0)
	{
		choice -= 1;
		moving_cursor.PlaySongOnce();
	}
}

void KirbyInfo::MoveCursorRight()
{
	if (choice + 1 < 4)
	{
		choice += 1;
		moving_cursor.PlaySongOnce();
	}
}

void KirbyInfo::ScrollingDown(Player &player)
{
	if (scroll_index + 3 < player.GetMaxKirbyTypes() + 1)
	{
		scroll_index += 3;
		moving_cursor.PlaySongOnce();
	}
}

void KirbyInfo::ScrollingUp()
{
	if (scroll_index - 3 > 0)
	{
		scroll_index -= 3;
		moving_cursor.PlaySongOnce();
	}
}

int KirbyInfo::SelectKirby()
{
	select_option.PlaySongOnce();
	return choice + scroll_index;
}