#pragma once

#include <string>

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"

class Unit
{
public:
	Unit();
	~Unit();

	/* SetUpAnimationHero
	 * Loads all the specific unit stats
	 * Also loads in the information of the unit
	 * As well as audio ect.
	 */
	void SetUpAnimationHeroStats(int type);

	/* SetUpAnimationHeroUnit
	 * Loads all the specific animations for the unit
	 * This includes audio and everything else for each 
	 * Hero type
	 */
	void SetUpAnimationHeroUnit(int type);

	/* SetUpAnimationEnemy
	 * Does the same thing as SetUpAnimationHero 
	 * but does it for enemy units
	 */
	void SetUpAnimationEnemy();

	/* CreateUnit
	 * Creates a file document 
	 * Stores all the stats the unit has
	 * Also puts it in the class object as well
	 */
	void CreateHeroUnit(int type);

	/* CreateEnemyUnit
	 * Creats a file document
	 * Stores all the stats the unit has
	 * Also puts it in the class object as well
	 */
	void CreateEnemyUnit(int type, int level);

	/* SaveUnit
	 * Saves all the current inforamtion of the unit in the text file
	 * deletes the text file if the unit is destroyed
	 */
	void SaveHeroUnit(int type);

	/* SaveEnemyUnit
	 * Saves all the current information of the unit in the text file
	 * Deletes the text file if the unit is destroyed
	 */
	void SaveEnemyUnit(int type);

	/* Load Unit
	 * Reads from a file all the information of the unit
	 * and stores it in this object
	 */
	void LoadHeroUnit(int type);

	/* LoadEnemyUnit
	 * Reads from a file all the information of the unit
	 * and stores it in this object
	 */
	void LoadEnemyUnit(int type);

	/* UpdateUnitAnimation
	 * updates the animations of the unit
	 */
	void UpdateUnitAnimation();

	/* RenderUnitInfo
	 * Draws all the information of the unit onto the screen
	 */
	void RenderUnitInfo(float position_x, float position_y, float scale_x, float scale_y );

	/* RednerUnit
	 * Draws the unit based on the current action state the unit is in
	 */
	void RenderUnit(float position_x, float position_y, float scale_x, float scale_y);

private:

	std::string name;											// Holds the name of the unit

	int action_state;											// Holds which action the current unit is in to be used for drawing

	int hit_points;												// Holds how much hit points (HP) the unit has left
	int max_hit_points;											// Holds how much hit points the unit is allowed to have
	int attack_points;											// Holds how much attack points the unit has
	int special_attack_points;									// Holds how much special attack points the unit has
	int defence_points;											// Holds how much defence points the unit has
	int special_defence_points;									// Holds how much special defence points the unit has
	int speed_points;											// Holds how much speed points the unit has 
	int experience_points;										// Holds how much experience points the unit has
	int level;													// Holds what level the unit is

	//==========================================================
	//	STATS SECTION
	//==========================================================

	SpriteAnimation background_stat_animation;					// Holds the coordinates of the background image animation

	Render background_stat_image;								// Holds the background image in which the stats will be on
	Render health_stat_text;									// Holds the text image of the number of health points left 
	Render health_text;											// Holds the text image of the word "Health: "
	Render level_stat_text;										// Holds the text image of the level number of the unit
	Render level_text;											// Holds the text image of the word "Level: 
	Render name_text;											// Holds the text image of the name of the unit
	Render attack_stat_text;									// Holds the text image of the number of attack points the unit has
	Render attack_text;											// Holds the text image of the word "Attack: "
	Render special_attack_stat_text;							// Holds the text image of the number of special attack points
	Render special_attack_text;									// Holds the text image of the word "Special Attack: "
	Render defence_stat_text;									// Holds the text image of the number of defence points
	Render defence_text;										// Holds the text image of the word "Defence"
	Render special_defence_special_text;						// Holds the text image of the number of special attack points
	Render special_defence_text;								// Holds the text image of the word "Special Defence: "
	Render speed_stat_text;										// Holds the text image of the number of speed points
	Render speed_text;											// Holds the text image of the word "Speed: "
	Render experience_stat_text;								// Holds the text image of the number of experience points
	Render experience_text;										// Holds the text image of the word "Experience: 

	Font vincentio_font16;										// Vincentio font size 36 to be used for text images
	Font vincentio_font24;										// Vincentio font size 36 to be used for text images

	//=========================================================
	// UNIT SECTION
	//=========================================================

	SpriteAnimation walking_animation;							// Holds the walking animation from the image
	SpriteAnimation running_animation;							// Holds the running animation from the image
	SpriteAnimation physical_attack_animation;					// Holds the physical attack animation from the image
	SpriteAnimation range_attack_animation;						// Holds the range attack animation from the image
	SpriteAnimation getting_hit_animation;						// Holds the getting hit animation from the image

	Render unit_image;											// Holds the image of the unit (Animations come from here)
};

