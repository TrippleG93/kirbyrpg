#pragma once

#include "Image.h"

class Render:
	public Image
{
public:
	Render();
	~Render();

	/* Draw Image
	 * ALLEGRO_FLIP_HORIZONTAL - flip the bitmap about the y-axis
     * ALLEGRO_FLIP_VERTICAL - flip the bitmap about the x-axis
	 * dx - Destination x position
	 * dy - Destination y position
	 */
	void DrawImage(float dx, float dy, int flag);

	/* DrawTintedImage
	 * ALLEGRO_FLIP_HORIZONTAL - flip the bitmap about the y-axis
     * ALLEGRO_FLIP_VERTICAL - flip the bitmap about the x-axis
	 * ALLEGRO_COLOR - draws only that color or make it transparent ect...
	 * dx - Destination x position
	 * dy - Destination y position
	 */
	void DrawTintedImage(ALLEGRO_COLOR tint,
		float dx, float dy, int flags);

	/* DrawScaledImage
	 * ALLEGRO_FLIP_HORIZONTAL - flip the bitmap about the y-axis
     * ALLEGRO_FLIP_VERTICAL - flip the bitmap about the x-axis
	 * sx - source x
	 * sy - source y
	 * sw - source width
	 * sh - source height
	 * dx - destination x
	 * dy - destination y
	 * dw - destination width
	 * dh - destination height
	 */
	void DrawScaledImage(float sx, float sy, float sw, float sh,
		float dx, float dy, float dw, float dh, int flags);

	/* DrawTintedScaledImage
	 * ALLEGRO_FLIP_HORIZONTAL - flip the bitmap about the y-axis
     * ALLEGRO_FLIP_VERTICAL - flip the bitmap about the x-axis
	 * ALLEGRO_COLOR - draws only that color or make it transparent ect...
	 * sx - source x
	 * sy - source y
	 * sw - source width
	 * sh - source height
	 * dx - destination x
	 * dy - destination y
	 * dw - destination width
	 * dh - destination height
	 */
	void DrawTintedScaledImage(ALLEGRO_COLOR tint,
		float sx, float sy, float sw, float sh,
		float dx, float dy, float dw, float dh, int flags);

	/* DrawRotatedImage
	 * ALLEGRO_FLIP_HORIZONTAL - flip the bitmap about the y-axis
	 * ALLEGRO_FLIP_VERTICAL - flip the bitmap about the x-axis
	 * cx - center x (relative to the bitmap)
     * cy - center y (relative to the bitmap)
     * dx - destination x
     * dy - destination y
     * angle - angle by which to rotate (radians)
     * flags - same as for al_draw_bitmap
	 */
	void DrawRotatedImage(float cx, float cy, float dx, float dy, float angle, int flags);

	/* DrawTintedRotatedImage
	 * ALLEGRO_FLIP_HORIZONTAL - flip the bitmap about the y-axis
     * ALLEGRO_FLIP_VERTICAL - flip the bitmap about the x-axis
	 * ALLEGRO_COLOR - draws only that color or make it transparent ect...
	 * cx - center x (relative to the bitmap)
	 * cy - center y (relative to the bitmap)
	 * dx - destination x
	 * dy - destination y
	 * angle - angle by which to rotate (radians)
	 * flags - same as for al_draw_bitmap
	 */
	void DrawTintedRotatedImage(ALLEGRO_COLOR tint,
		float cx, float cy, float dx, float dy, float angle, int flags);
	
	/* DrawScaledRotatedBitmap
	 * ALLEGRO_FLIP_HORIZONTAL - flip the bitmap about the y-axis
	 * ALLEGRO_FLIP_VERTICAL - flip the bitmap about the x-axis
	 * cx - center x
     * cy - center y
     * dx - destination x
     * dy - destination y
     * xscale - how much to scale on the x-axis (e.g. 2 for twice the size)
     * yscale - how much to scale on the y-axis
     * angle - angle by which to rotate (radians)
	 */
	void DrawScaledRotatedBitmap(float cx, float cy, float dx, float dy, float xscale, float yscale,
		float angle, int flags);

	/* DrawTintedScaledRotatedBitmap
	 * ALLEGRO_FLIP_HORIZONTAL - flip the bitmap about the y-axis
	 * ALLEGRO_FLIP_VERTICAL - flip the bitmap about the x-axis
	 * sx - source x
	 * sy - source y
	 * sw - source width
	 * sh - source height
	 * ALLEGRO_COLOR - draws only that color or make it transparent ect...
	 * cx - center x
	 * cy - center y
	 * dx - destination x
	 * dy - destination y
	 * xscale - how much to scale on the x-axis (e.g. 2 for twice the size)
	 * yscale - how much to scale on the y-axis
	 * angle - angle by which to rotate (radians)
	 */
	void DrawTintedScaledRotatedBitmap(float sx, float sy, float sw, float sh,
		ALLEGRO_COLOR tint, float cx, float cy, float dx, float dy, float xscale, float yscale,
		float angle, int flag);

};

