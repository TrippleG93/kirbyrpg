#pragma once

#include <string>

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"

class Stats
{
public:
	/* Basic Contructor
	 * - If it is an enemy or not
	 * - Type determine which kirby/enemy stat we are loading
	 */
	Stats(bool enemy, int type);

	Stats();
	~Stats();

	/* SetUpAnimationKirbies
	 * Loads everything needed for the kirbies
	 * Images, audio ect.
	 */
	void SetUpAnimationKirbies(int type);

	/* SetUpAnimationEnemies
	 * Loades everything needed for the enemies
	 * Images, Audio, ect
	 */
	void SetUpAnimationEnemies(int type);

	/* Load Stats
	 * Reads from a file all the information needed for that character
	 */
	void LoadStatKirbies(int type);

	/* LoadStatEnemies
	 * Reads from a file all the information needed for that enemy 
	 */
	void LoadStatEnemies(int type);

	/* RenderKirbyInfo
	 * Draws everything needed for the class
	 */
	void RenderKirbyInfo(float position_x, float position_y, float scale_x, float scale_y);

	/* UpdateKirbyInfo
	 * Updates the animations and anything else
	 * associated with the class
	 */
	void UpdateAnimation();

	/* Get growth stats
	 * What this does is return the applied upgrade stats
	 * For instance if the growth stat for HP is 4 then it will 
	 * Get a random number from 1 - 100 and mod that by 4 and add 1
	 * This number represents how much that stat will go up by when
	 * leveling up for that first time
	 */
	int GetHitPointsGrowth();
	int GetAttackGrowth();
	int GetSpecialAttackGrowth();
	int GetDefenceGrowth();
	int GetSpecialDefenceGrowth();
	int GetSpeedGrowth();
	std::string GetName() { return Stats::name; }

private:

	std::string name;											// Holds the name of the character

	int hit_points_growth;										// Holds how much potential for leveling up hit points
	int attack_growth;											// Holds how much potential for leveling up attack 
	int special_attack_growth;									// Holds how much potential for leveling up special attack
	int defence_growth;											// Holds how much potential for leveling up defence
	int special_defence_growth;									// Holds how much potential for leveling up special defence
	int speed_growth;											// Holds how much potential for leveling up speed growth

	SpriteAnimation character_animation;						// Holds the coordinates of the animation for the character/Enemy
	SpriteAnimation health_animation;							// Holds the coordinates of the health bar animation
	SpriteAnimation attack_animation;							// Holds the coordiantes of the attack bar animation
	SpriteAnimation special_attack_animation;					// Holds the coordinates of the special attack bar animation
	SpriteAnimation defence_animation;							// Holds the coordinates of the defence bar animation
	SpriteAnimation special_defence_animation;					// Holds the coordinates of the special defence bar animation
	SpriteAnimation speed_animation;							// Hol

	Render character_image;										// Holds the image for the character/Enemy
	Render stat_background_image;								// Holds the background box of the stats
	Render stats_bar_image;										// Holds the image for all the stat bars

	Font vincentio_font16;										// Vincentio font size 24 to be used for text images
	Font vincentio_font12;										// Vincentio font size 16 to be used for text images

	Render kirby_name_image;									// Holds the text image of the name of the kirby
	Render health_text_image;									// Holds the image of the text "Health:"
	Render attack_text_image;									// Holds the image of the text "Attack:"
	Render special_attack_text_image;							// Holds the image of the text "Special Attack:"
	Render defence_text_image;									// Holds the image of the text "Defence:"
	Render special_defence_text_image;							// Holds the image of the text "Special Defence:"
	Render speed_text_image;									// Holds the image of the text "Speed"
};

