#pragma once

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"

class Player;
class Stats;

class EnemyInfo
{
public:
	EnemyInfo();
	EnemyInfo(Player &player);
	~EnemyInfo();

	/* SetUpAnimation
	 * Loads everything needed for this class
	 * Images, audio ect.
	 */
	void SetUpAnimation();

	/* LoadStats
	 * Reads from a file of all the unlocked kirby growth stats
	 * Creates the object for each one to display in this screen
	 */
	void LoadStats(Player &player);

	/* RenderKirbyInfo
	 * Draws everything needed for the class
	 */
	void RenderEnemyInfo(Player &player, float scale_x, float scale_y);

	/* UpdateEnemyInfo
	 * Updates all the animations for the enemy info menu
	 */
	void UpdateEnemyInfo();

	/* MoveCursorLeft
	* Subtracts 1 from the cursor unless at the edge
	*/
	void MoveCursorLeft();

	/* MoveCursorRight
	* Adds 1 from the cursor unless at the edge
	*/
	void MoveCursorRight();

	/* ScrollingDown
	* Adds three to scroll Index to indicate the next row
	*/
	void ScrollingDown(Player &player);

	/* ScrollingUp
	* Subtracts 1 to scroll index to indeicate the previous row
	*/
	void ScrollingUp();

	/* SelectKirby
	* return which kirby that was selected
	*/
	int SelectKirby();

private:
	int choice;												// Holds which current kirby they are currently at
	int scroll_index;										// This will allow the "scrolliing effect" when looking at the kirby infos

	std::map <int, Stats*> unlocked_enemies;				// Holds the growth stats of all the unlocked kirbies

	SpriteAnimation cursor_animation;						// Holds the coordinates for the cursor image
	SpriteAnimation unknown_enemy_animation;				// Holds the coordinates for the unkown enemy blocks

	Render background_image;								// Holds the back ground image the enemy info screen
	Render back_out_text_image;								// Holds the text image "Backspace to Main Menu"
	Render cursor_image;									// Holds the image of the cursor (Fist one)
	Render unknown_enemy;									// Holds the image of what an unkown enemy block is

	Font vincentio_font36;									// Vincentio font size 36 to be used for text images

	Audio moving_cursor;									// Holds the sound effect for when you move the cursor
	Audio select_option;									// Holds the sound effect for when you select what option you want

};

