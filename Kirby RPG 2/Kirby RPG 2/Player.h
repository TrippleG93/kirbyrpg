#pragma once

#include <map>

class Player
{
public:
	Player();
	~Player();

	/* LoadPlayer
	 * Reads from a file and loads everything about the player
	 * Enemies encountered 
	 * Enemies unlocked
	 */
	void LoadPlayer();

	/* DidPlayerEncounterEnemy
	 * returns true or false based on if the player has engaged that enemy
	 */
	bool DidPlayerEncounterEnemy(int enemy_type);

	/* DidPlayerUnlockKirby
	 * returns true or false based on if the player has unlocked the kirby or not
	 */
	bool DidPlayerUnlockKirby(int kirby_type);

	/* HasPlayerUnlockedAKirby
	 * returns true or false based on if a kirby has unlocked any kirby or not 
	 */
	bool HasPlayerUnlockedAKirby();

	/* ReadEnemiesEnocunteredFile
	 * Does exactly as it says  
	 */
	void ReadEnemiesEncounteredFile();

	/* ReadKirbiesUnlockedFile
	 * Does exactly as it says 
	 */
	void ReadKirbiesUnlockedFile();

	/* SaveUnlockedKirbiesFile
	 * Saves on a text file the unlocked kirbies the player has
	 */
	void SaveUnlockedKirbiesFile();

	/* SaveEnemiesEncounteredFile
	 * Saves on a text file the unlocked enemies the player has	
	 */
	void SaveEnemiesEncounteredFile();

	/* UnlockRandomKirby
	 * Unlocks a random kirby using a random number generator
	 * Will only be called if the player does not have any kirby they unlocked
	 */
	void UnlockRandomKirby();

	/* UnlockAKirby
	 * Unlocks a kirby that the player has unlocked  
	 */
	void UnlockAKirby(int type);

	/* UnlockAnEnemy
	 * unlocks an enemy that the player has encountered 
	 */
	void UnlockAnEnemy(int type);

	int GetMaxEnemyTypes() { return Player::max_enemy_types; }
	int GetMaxKirbyTypes() { return Player::max_kirbie_types; }

private:

	std::map <int, bool> enemies_encountered;						// Holds what enemies have been encountered and what has not
	std::map <int, bool> kirbies_unlocked;							// Holds what kirbies have been unlocked

	int max_enemy_types;											// Holds the max amount of enemy types
	int max_kirbie_types;											// Holds the max amount of kirby types
};

