#include "Render.h"

#include "allegro5\bitmap_draw.h"

Render::Render()
{
}


Render::~Render()
{
}

void Render::DrawImage(float dx, float dy, int flag)
{
	al_draw_bitmap(GetImage(), dx, dy, flag);
}

void Render::DrawScaledImage(float sx, float sy, float sw, float sh,
	float dx, float dy, float dw, float dh, int flags)
{
	al_draw_scaled_bitmap(GetImage(), sx, sy, sw, sh, dx, dy, dw, dh, flags);
}

void Render::DrawTintedScaledImage(ALLEGRO_COLOR tint,
	float sx, float sy, float sw, float sh,
	float dx, float dy, float dw, float dh, int flags)
{
	al_draw_tinted_scaled_bitmap(GetImage(), tint, sx, sy, sw, sh,
		dx, dy, dw, dh, flags);
}

void Render::DrawTintedImage(ALLEGRO_COLOR tint,
	float dx, float dy, int flags)
{
	al_draw_tinted_bitmap(GetImage(), tint, dx, dy, flags);
}

void Render::DrawRotatedImage(float cx, float cy, float dx, float dy, float angle, int flags)
{
	al_draw_rotated_bitmap(GetImage(), cx, cy, dx, dy, angle, flags);
}

void Render::DrawTintedRotatedImage(ALLEGRO_COLOR tint,
	float cx, float cy, float dx, float dy, float angle, int flags)
{
	al_draw_tinted_rotated_bitmap(GetImage(), tint, cx, cy, dx, dy, angle, flags);
}

void Render::DrawScaledRotatedBitmap(float cx, float cy, float dx, float dy, float xscale, float yscale,
	float angle, int flags)
{
	al_draw_scaled_rotated_bitmap(GetImage(), cx, cy, dx, dy, xscale, yscale, angle, flags);
}

void Render::DrawTintedScaledRotatedBitmap(float sx, float sy, float sw, float sh,
	ALLEGRO_COLOR tint, float cx, float cy, float dx, float dy, float xscale, float yscale,
	float angle, int flag)
{
	al_draw_tinted_scaled_rotated_bitmap(GetImage(), tint, cx, cy, dx, dy, xscale, yscale, angle, flag);
}