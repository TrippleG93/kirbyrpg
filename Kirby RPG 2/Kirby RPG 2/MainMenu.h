#pragma once

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"

class MainMenu
{
public:
	MainMenu();
	~MainMenu();

	/* SetUpMainMenu
	 * Loads everything needed for the main menu
	 */
	void SetUpMainMenu();

	/* RenderMainMenu
	 * Draws everything needed for the main menu
	 */
	void RenderMainMenu(float scale_x, float scale_y);

	/* UpdateMainMenu
	 * Updates the animation for the main menu
	 */
	void UpdateMainMenu();

	/* MoveCursorDown
	 * Adds 1 to the cursor_y_position to move to the next 
	 * selection downward on the main menu
	 */
	void MoveCursorDown();

	/* MoveCursorUp
	 * Subtracts 1 to the cursor_y_positon to move to the 
	 *  next selection upward on the main menu
	 */
	void MoveCursorUp();
	
	/* GetCursorSelection
	 * Return the int value of what the user wanted
	 * Play the sound effect of clicking the cursor
	 */
	int GetCursorSelection();

private:

	int cursor_y_position;											// Holds which selection the cursor is at and guides the y position

	SpriteAnimation background_animation;							// Holds all the coordinates for the background image
	SpriteAnimation cursor_animation;								// Holds all the coordinates for the cursor image

	Render background_image;										// The background image itself
	Render new_game_text;											// Create an image text for "new game"
	Render load_game_text;											// Create an image text for "load game"
	Render option_text;												// Create an image text for "Options"
	Render enemies_text;											// Create an image text for "enemy infos"
	Render kirby_text;												// Create an image text for "owned kirbies"
	Render quit_text;												// Create an image text for "quit"
	Render cursor_image;											// The cursor image itself

	Font vincentio_font36;											// Vincentio font size 36 to be used for text images

	Audio moving_cursor;											// Holds the sound effect for when you move the cursor
	Audio select_option;											// Holds the sound effect for when you select what option you want
	Audio main_menu_song;											// Holds the soung of the main menu
};

