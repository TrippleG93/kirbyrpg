#include "Font.h"

#include "allegro5\allegro_font.h"
#include "allegro5\allegro_ttf.h"

Font::Font()
{
}


Font::~Font()
{
	DestroyFont();
}

void Font::CreateFont(const std::string &file_path, int size)
{
	font = al_load_font(file_path.c_str(), size, 0);
}

void Font::DestroyFont()
{	
	al_destroy_font(font);													
}

ALLEGRO_FONT *Font::ObtainFont()
{
	return font;
}