#pragma once

#include <map>

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"

class Player;
class Stats;

class KirbyInfo
{
public:
	
	KirbyInfo();

	KirbyInfo(Player &player);
	~KirbyInfo();

	/* SetUpAnimation
	 * Loads everything needed for this class
	 * Images, audio ect.
	 */
	void SetUpAnimation();

	/* RenderKirbyInfo
	 * Draws everything needed for the class
	 */
	void RenderKirbyInfo(Player &player, float scale_x, float scale_y);

	/* LoadStats
	 * Reads from a file of all the unlocked kirby growth stats
	 * Creates the object for each one to display in this screen
	 */
	void LoadStats(Player &player);

	/* UpdateKirbyInfo
	 * Updates the animations and anything else 
	 * associated with the class
	 */
	void UpdateKirbyInfo();

	/* MoveCursorLeft
	 * Subtracts 1 from the cursor unless at the edge
	 */
	void MoveCursorLeft();

	/* MoveCursorRight
	 * Adds 1 from the cursor unless at the edge
	 */
	void MoveCursorRight();

	/* ScrollingDown
	 * Adds three to scroll Index to indicate the next row
	 */
	void ScrollingDown(Player &player);

	/* ScrollingUp
	 * Subtracts 1 to scroll index to indeicate the previous row
	 */
	void ScrollingUp();

	/* SelectKirby
	 * return which kirby that was selected
	 */
	int SelectKirby();

private:

	int choice;												// Holds which current kirby they are currently at
	int scroll_index;										// This will allow the "scrolliing effect" when looking at the kirby infos

	std::map <int, Stats*> unlocked_kirbies;					// Holds the growth stats of all the unlocked kirbies

	SpriteAnimation unknown_kirby_animation;				// Holds the coordinates for an unkown kirby image
	SpriteAnimation cursor_animation;						// Holds the coordinates for the cursor image
	
	Render unkown_kirby_image;								// Holds the image of the unknow kirby 
	Render cursor_image;									// Holds the image of the cursor (Fist one)
	Render background_image;								// Holds the background image
	Render back_out_text_image;								// Holds the text image "Backspace to Main Menu"

	Font vincentio_font36;									// Vincentio font size 36 to be used for text images

	Audio moving_cursor;									// Holds the sound effect for when you move the cursor
	Audio select_option;									// Holds the sound effect for when you select what option you want
};

