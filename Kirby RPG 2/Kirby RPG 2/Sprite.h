#pragma once
class Sprite
{
public:
	Sprite();
	Sprite(int position_x, int position_y, int source_x, int source_y, int width, int height);
	~Sprite();

	void SetPositionX(int position_x);
	void SetPositionY(int position_y);

	int GetSourceX() { return source_x; }
	int GetSourceY() { return source_y; }
	int GetWidth() { return width; }
	int GetHeight() { return height; }
	int GetPositionX() { return position_x; }
	int GetPositionY() { return position_y; }

private:

	int position_x;														// X position the image will be drawn on
	int position_y;														// Y position the image will be drawn on
	int source_x;														// Where from the image you start from x position
	int source_y;														// Where fromt he image you start from y position
	int width;															// How wide the image is
	int height;															// How tall the image is
};

