#pragma once

#include <string>

struct ALLEGRO_FONT;		// Forward declaring the ALLEGRO_FONT variable

class Font
{
public:
	Font();
	~Font();

	/*
	 * CreateFont
	 * Create a specific font with a specifc size and stores it in the map
	 * each font will be loaded once if it is not the same size
	 * file_path - used to load the font file from the file location
	 * size - how long the text string is
	 */
	void CreateFont(const std::string &file_path, int size);

	/*
	* DestroyFonts
	* Destroys certain font variables from the _fontSheet Map
	*/
	void DestroyFont();

	/*
	* obtainFont
	* obtain the font that is stored in _fontSheets
	*/
	ALLEGRO_FONT *ObtainFont();

private:
	// Map variable that takes in the name of the font and the font itself
	ALLEGRO_FONT* font;
};

