#include "allegro5\allegro.h"
#include "allegro5\allegro_image.h"
#include "allegro5\allegro_native_dialog.h"
#include "allegro5\allegro_font.h"
#include "allegro5\allegro_ttf.h"
#include "allegro5\allegro_audio.h"
#include "allegro5\allegro_acodec.h"

#include "Display.h"
#include "Input.h"

#include "TitleScreen.h"
#include "MainMenu.h"
#include "KirbyInfo.h"
#include "EnemyInfo.h"
#include "HeroSelect.h"

#include "Player.h"
#include "Unit.h"

//*******************************************
// PROJECT VARIABLE
//*******************************************

enum States
{
	SHUTDOWN, TITLE, MAINMENU, ENEMYINFO, KIRBYSTATS, HEROSELECT
};

int state = TITLE;										// Keeps track of where the current game state is at
int current_controller = 0;								// Keeps track of what controller will currently make the input
bool game_loop = true;									// Used for the game loop
bool render = false;									// Lets the program know it needs to draw to the screen

ALLEGRO_EVENT_QUEUE *event_queue = NULL;				// Used to hold all events from allegro
ALLEGRO_EVENT events;									// Holds and individual event from allegro
ALLEGRO_TIMER *timer;									// Timer that allows 60 fps to happen

Display *display = nullptr;								// Pointer that holds the display class
Input *input = nullptr;									// Pointer that holds the input class
TitleScreen *title_screen = nullptr;					// Pointer that holds the Title screen class
MainMenu *main_menu_screen = nullptr;					// Pointer to the main menu class
Player *player = nullptr;								// Used to hold all the information for the player
KirbyInfo *kirby_info_screen = nullptr;					// Used to hold the kirby info class
EnemyInfo *enemy_info_menu = nullptr;					// Used to hold the enmy info class
Unit hero_squad[4];										// Holds the 4 kirbies that would be selected for the kirby squad
HeroSelect *hero_select;								// Pointer that holds the hero select class

//*******************************************
// PROJECT FUNCTIONS
//*******************************************

void InitAllegro();										// Used to load all the add-ons for Allegro4
void RenderEnemyInfoState();							// Draws the enemy info menu
void RenderMainMenuState();								// Draws the main menu
void RenderTitleState();								// Draws the title screen
void RenderKirbyInfoState();							// Draws the kirby info menu
void RenderHeroSelectState();							// Draws the hero select state
void ShutDown();										// Deletes remaining pointers that were used
void UpdateEnemyInfoState();							// Updates input for the Enemy info menu
void UpdateKirbyInfoState();							// Updates input for the kirby info menu
void UpdateMainMenuState();								// Updates input for the main menu
void UpdateTitleState();								// Updates input for the title state
void UpdateHeroSelectState();							// Updates input for the hero select state


int main()
{
	InitAllegro();

	//*******************************************
	// PROJECT Init
	//*******************************************
	display = new Display();														// Create the Display object
	input = new Input();															// Create the Input object
	input->SetUpControllers();														// Initliaze the controllers in the game

	title_screen = new TitleScreen();												// Create the titlescreen object
	timer = al_create_timer(1.0 / 60.0);											// Game run 60 frames a second

	//*******************************************
	// TIMER Init AND STARTUP
	//*******************************************

	event_queue = al_create_event_queue();
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());			// Registers the keyboard to the queue
	al_register_event_source(event_queue, al_get_mouse_event_source());				// Registers the mouse to the queue
	al_register_event_source(event_queue, al_get_joystick_event_source());			// Register the joystick to the queue																
	al_register_event_source(event_queue,
		al_get_display_event_source(display->GetDisplay()));						// Registers the display to the queue

	al_start_timer(timer);

	//*******************************************
	// Game Loop
	//*******************************************
	while (game_loop == true)
	{
		al_wait_for_event(event_queue, &events);

		if (input->GetHowManyControllers() != 0)
		{
			input->UpdateControllerInput(events);
		}

		input->UpdateKeyboardInput(events);

		//_input->update_Keyboard_Input(Events);

		if (events.type == ALLEGRO_EVENT_JOYSTICK_CONFIGURATION)
		{
			input->ResyncControllers();
		}

		//*******************************************
		// GAME UPDATE
		//*******************************************

		if (events.type == ALLEGRO_EVENT_TIMER)
		{
			render = true;

			//*******************************************
			// UPDATE ANIMATIONS HERE
			//*******************************************

			if (state == TITLE)
			{
				title_screen->UpdateTitleScreen();
			}

			if (state == MAINMENU)
			{
				main_menu_screen->UpdateMainMenu();
			}

			if (state == KIRBYSTATS)
			{
				kirby_info_screen->UpdateKirbyInfo();
			}

			if (state == ENEMYINFO)
			{
				enemy_info_menu->UpdateEnemyInfo();
			}

			if (state == HEROSELECT)
			{
				hero_select->UpdateHeroSelect();
			}


			/*
			if (state == PLAYING || state == GAMEOPTIONS ||
				state == PAUSEMENU || state == UNITACTION ||
				state == SHOWATTACK)
			{
				for (auto const& x : Buildings)
				{
					Buildings[x.first]->Update();
				} // End for loop
				for (auto const&x : AllUnitAnimations)
				{
					AllUnitAnimations[x.first]->Update();
				}
			}
			*/
			//*******************************************
			// END UPDATE ANIMATIONS
			//*******************************************

			//====================================================================
			// TITLE STATE
			//====================================================================

			if (state == TITLE)
			{
				UpdateTitleState();
			} 

			if (state == MAINMENU)
			{
				UpdateMainMenuState();
			}

			if (state == KIRBYSTATS)
			{
				UpdateKirbyInfoState();
			}

			if (state == ENEMYINFO)
			{
				UpdateEnemyInfoState();
			}

			if (state == HEROSELECT)
			{
				UpdateHeroSelectState();
			}

			input->ButtonHeldEvent();
			input->KeyHeldEvent();
		}

		//*******************************************
		// RENDER
		//*******************************************

		if (render == true && al_event_queue_is_empty(event_queue))
		{
			render = false;
			//al_draw_textf(font18, al_map_rgb(255, 0, 255), 5, 5, 0, "FPS: %i", game_fps);	//display FPS on screen

			// Begin the project render based on the state
			if (state == TITLE)
			{
				RenderTitleState();
			}

			if (state == MAINMENU)
			{
				RenderMainMenuState();
			}

			if (state == KIRBYSTATS)
			{
				RenderKirbyInfoState();
			}

			if (state == ENEMYINFO)
			{
				RenderEnemyInfoState();
			}

			if (state == HEROSELECT)
			{
				RenderHeroSelectState();
			}
		
			display->FlipDisplay();
		}

	}


	ShutDown();
	return 0;
}

void InitAllegro() 
{
	if (!al_init())
	{
		exit(1);
	}

	// Initlizes the ability to use images
	if (!al_init_image_addon())
	{
		printf("Error loading image addon!\n");
		exit(1);
	}

	// Initialize the ability to use fonts
	al_init_font_addon();

	// Initialize the ability to use true type fonts
	if (!al_init_ttf_addon())
	{
		printf("Error loading font addon!\n");
		exit(1);
	}

	al_install_audio();				// Allows audio to be played

	if (!al_is_audio_installed())
	{
		printf("Error loading audio addon!\n");
		exit(1);
	}

	// Allows the ability to load diffrent audio files
	if (!al_init_acodec_addon())
	{
		printf("Error loading accodec addon!\n");
		exit(1);
	}

	// How many sounds I am allowed to play (20)
	if (!al_reserve_samples(20))
	{
		printf("Error loading samples!\n");
		exit(1);
	}

	// Allows the ability to use joysticks in the game.
	if (!al_install_joystick())
	{
		printf("Error loading joystick addon!\n");
		exit(1);
	}

	// Allows the aility to use keyboard in the game
	if (!al_install_keyboard())
	{
		printf("Error loading keyboard addon!\n");
		exit(1);
	}

	// Allows the ability to use a mouse in the game
	if (!al_install_mouse())
	{
		printf("Error loading mouse addon!\n");
		exit(1);
	}
}

void RenderEnemyInfoState()
{
	enemy_info_menu->RenderEnemyInfo(*player, display->GetDisplayScaleX(), display->GetDisplayScaleY());
}

void RenderKirbyInfoState()
{
	kirby_info_screen->RenderKirbyInfo(*player, display->GetDisplayScaleX(), display->GetDisplayScaleY());
}

void RenderMainMenuState()
{
	main_menu_screen->RenderMainMenu(display->GetDisplayScaleX(), display->GetDisplayScaleY());
}

void RenderTitleState()
{
	title_screen->RenderTitleScreen(display->GetDisplayScaleX(), display->GetDisplayScaleY());
}

void RenderHeroSelectState()
{
	hero_select->RenderHeroSelect(display->GetDisplayScaleX(), display->GetDisplayScaleY());
}

void ShutDown()
{
	delete(display);
	delete(input);
}

void UpdateEnemyInfoState()
{
	if (input->WasKeyPressed(KEYCODE_BACKSPACE) == true &&
		input->WasKeyHeld(KEYCODE_BACKSPACE) == false)
	{
		state = MAINMENU;											// User backed out to the main menu
		delete(enemy_info_menu);									// Delete the enemy info menu
	}

	if (input->WasKeyPressed(KEYCODE_D) == true &&
		input->WasKeyHeld(KEYCODE_D) == false)
	{
		enemy_info_menu->MoveCursorRight();
	}

	if (input->WasKeyPressed(KEYCODE_A) == true &&
		input->WasKeyHeld(KEYCODE_A) == false)
	{
		enemy_info_menu->MoveCursorLeft();
	}

	if (input->WasKeyPressed(KEYCODE_W) == true &&
		input->WasKeyHeld(KEYCODE_W) == false)
	{
		enemy_info_menu->ScrollingUp();
	}

	if (input->WasKeyPressed(KEYCODE_S) == true &&
		input->WasKeyHeld(KEYCODE_S) == false)
	{
		enemy_info_menu->ScrollingDown(*player);
	}
}

void UpdateKirbyInfoState()
{
	if (input->WasKeyPressed(KEYCODE_BACKSPACE) == true &&
		input->WasKeyHeld(KEYCODE_BACKSPACE) == false)
	{
		state = MAINMENU;
		delete(kirby_info_screen);
	}

	if (input->WasKeyPressed(KEYCODE_D) == true &&
		input->WasKeyHeld(KEYCODE_D) == false)
	{
		kirby_info_screen->MoveCursorRight();
	}

	if (input->WasKeyPressed(KEYCODE_A) == true &&
		input->WasKeyHeld(KEYCODE_A) == false)
	{
		kirby_info_screen->MoveCursorLeft();
	}

	if (input->WasKeyPressed(KEYCODE_W) == true &&
		input->WasKeyHeld(KEYCODE_W) == false)
	{
		kirby_info_screen->ScrollingUp();
	}

	if (input->WasKeyPressed(KEYCODE_S) == true &&
		input->WasKeyHeld(KEYCODE_S) == false)
	{
		kirby_info_screen->ScrollingDown(*player);
	}
}

void UpdateMainMenuState()
{
	if (input->WasKeyPressed(KEYCODE_BACKSPACE) == true &&
		input->WasKeyHeld(KEYCODE_BACKSPACE) == false)
	{
		title_screen = new TitleScreen();
		state = TITLE;
		delete(main_menu_screen);
		delete(player);
	}

	if (input->WasKeyPressed(KEYCODE_S) == true &&
		input->WasKeyHeld(KEYCODE_S) == false)
	{
		main_menu_screen->MoveCursorDown();
	}

	if (input->WasKeyPressed(KEYCODE_W) == true &&
		input->WasKeyHeld(KEYCODE_W) == false)
	{
		main_menu_screen->MoveCursorUp();
	}

	if (input->WasKeyPressed(KEYCODE_ENTER) == true &&
		input->WasKeyHeld(KEYCODE_ENTER) == false)
	{
		switch (main_menu_screen->GetCursorSelection())
		{
		case 0:														// Selected new game
			state = HEROSELECT;										// Selected to start the new game and will go to hero select
			input->ClearKeyBoardInputs();
			if (player->HasPlayerUnlockedAKirby() == false)
			{
				srand(time(NULL));
				player->UnlockAKirby((rand() % 10) + 1);
			}
			hero_select = new HeroSelect(*player);
			delete(main_menu_screen);								// Delete the main menu object
			break;
		case 1:														// Selected load game
			break;
		case 2:														// Selected options
			break;
		case 3:														// Selected to look at enemies info
			state = ENEMYINFO;										// Change the state to enemy info 
			input->ClearKeyBoardInputs();
			enemy_info_menu = new EnemyInfo(*player);				// Create the enemy info menu
			break;
		case 4:														// Select to look at kirbies currently unlocked
			state = KIRBYSTATS;										// Change the state to kirby stats
			input->ClearKeyBoardInputs();
			kirby_info_screen = new KirbyInfo(*player);				// Create the kirby info screen
			break;
		case 5:														// Selected to quit the game
			game_loop = false;										// Change to false to exit the game loop
			state = SHUTDOWN;										// Change the state to shut down mode
			delete(main_menu_screen);								// Delete the main menu object
			delete(player);											// Delete the player object
			break;
		default:
			break;
		}
	}
}

void UpdateTitleState()
{
	if (input->WasButtonPressed(current_controller, AINDEX_START_BUTTON) == true &&
		input->WasButtonHeld(current_controller, AINDEX_A_BUTTON) == false)
	{
		main_menu_screen = new MainMenu();
		state = MAINMENU;
		delete(title_screen);
	}

	if (input->WasKeyPressed(KEYCODE_ENTER) == true &&
		input->WasKeyHeld(KEYCODE_ENTER) == false)
	{
		main_menu_screen = new MainMenu();
		player = new Player();
		state = MAINMENU;
		input->ClearKeyBoardInputs();
		delete(title_screen);
	}
}

void UpdateHeroSelectState()
{
	if (input->WasKeyPressed(KEYCODE_BACKSPACE) == true &&
		input->WasKeyHeld(KEYCODE_BACKSPACE) == false)
	{
		state = MAINMENU;
		main_menu_screen = new MainMenu();
		delete(hero_select);
	}

	if (input->WasKeyPressed(KEYCODE_A) == true &&
		input->WasKeyHeld(KEYCODE_A) == false)
	{
		hero_select->MoveCursorLeft();
	}

	if (input->WasKeyPressed(KEYCODE_D) == true &&
		input->WasKeyHeld(KEYCODE_D) == false)
	{
		hero_select->MoveCursorRight();
	}

	if (input->WasKeyPressed(KEYCODE_ENTER) == true &&
		input->WasKeyHeld(KEYCODE_ENTER) == false)
	{
		hero_select->SelectHero();
	}
}
