#include "HeroSelect.h"

#include "Player.h"
#include "Unit.h"


HeroSelect::HeroSelect(Player &player)
{
	cursor_position = 275;
	cursor_choice = 1;
	max_choice = 0;
	hero_team_count = 0;												// Represents index zero of the hero selection stage


	for (int i = 1; i < player.GetMaxKirbyTypes() + 1; i++)
	{
		if (player.DidPlayerUnlockKirby(i) == true)
		{
			unlocked_heroes[i] = new Unit();							// Create the unit that can be playable
			unlocked_heroes[i]->LoadHeroUnit(i);						// Loads all the stats for the hero unit
			unlocked_heroes[i]->SetUpAnimationHeroStats(i);					// Set up the animations for the unit
			max_choice += 1;
		}
	}

	SetUpAnimations();
}

HeroSelect::~HeroSelect()
{
	for (auto const& x : unlocked_heroes)
	{
		delete(x.second);
	} // End for loop

	for (auto const& x : hero_team)
	{
		delete(x.second);
	} // End for loop
}

void HeroSelect::SetUpAnimations()
{
	background.LoadBitmap("Content/Backgrounds/DS DSi - Kirby Super Star Ultra - Dedede Stadium.png");

	background_crowd_animation.AddIndividualFrame(0, 0, 159, 5, 320, 256);
	background_crowd_animation.AddIndividualFrame(0, 0, 484, 5, 320, 256);
	background_crowd_animation.AddIndividualFrame(0, 0, 159, 266, 320, 256);
	background_crowd_animation.AddIndividualFrame(0, 0, 484, 266, 320, 256);
	background_crowd_animation.CombineFrames(10);

	hero_stand_image.LoadBitmap("Content/Miscellaneous/SNES - Kirby Super Star Kirbys Fun Pak - Megaton Punch.png");

	hero_stands_animation.AddIndividualFrame(0, 0, 2, 350, 256, 194);
	hero_stands_animation.AddIndividualFrame(0, 0, 260, 350, 256, 194);
	hero_stands_animation.AddIndividualFrame(0, 0, 518, 350, 256, 194);
	hero_stands_animation.CombineFrames(10);

	cursor_image.LoadBitmap("Content/Miscellaneous/SNES - Kirby Super Star Kirbys Fun Pak - Game Over & Continue Screens.png");

	cursor_animation.AddIndividualFrame(0, 325, 14, 269, 31, 13);
	cursor_animation.CombineFrames(1);

	moving_cursor.LoadAudioFiles("Content/Sound Effects/Moving Cursor.wav");
	select_option.LoadAudioFiles("Content/Sound Effects/menu-select.wav");
}

void HeroSelect::RenderHeroSelect(float scale_x, float scale_y)
{
	int box_counter = 1;												// Holds how many boxes will be drawn in the hero select section
	int position_x = 0;
	int which_hero = 1;									// Holds which hero is being selected

	background.DrawScaledImage(background_crowd_animation.GetCurrentFrame().GetSourceX(),
		background_crowd_animation.GetCurrentFrame().GetSourceY(),
		background_crowd_animation.GetCurrentFrame().GetWidth(),
		background_crowd_animation.GetCurrentFrame().GetHeight(),
		0, 0,
		background_crowd_animation.GetCurrentFrame().GetWidth() * scale_x * 4,
		background_crowd_animation.GetCurrentFrame().GetHeight() * scale_y * 2.81,
		NULL);

	for (auto const& x : unlocked_heroes)
	{
		if (max_choice > 3)											// We will start the scrolling
		{
			if (cursor_choice < 4)
			{
				x.second->RenderUnitInfo(position_x + (200 * box_counter - 1), 100, scale_x, scale_x);
				box_counter += 1;												// Increment the amount of boxes that have been drawn
				position_x += 100;
			}
			else if (cursor_choice > 3 && which_hero == cursor_choice - 2)
			{
				x.second->RenderUnitInfo(position_x + (200 * box_counter - 1), 100, scale_x, scale_x);
				box_counter += 1;												// Increment the amount of boxes that have been drawn
				position_x += 100;
			}
		}
		else
		{
			x.second->RenderUnitInfo(position_x + (200 * box_counter - 1), 100, scale_x, scale_x);
			box_counter += 1;												// Increment the amount of boxes that have been drawn
			position_x += 100;
		}

		if (which_hero == cursor_choice - 2)
		{
			which_hero = cursor_choice - 2;
		}
		else
		{
			which_hero += 1;
		}
		

		if (box_counter == 4)											// Stops the program from showing three stat boxes
		{
			break;
		}
	} // End for loop

	cursor_image.DrawScaledImage(cursor_animation.GetCurrentFrame().GetSourceX(),
		cursor_animation.GetCurrentFrame().GetSourceY(),
		cursor_animation.GetCurrentFrame().GetWidth(),
		cursor_animation.GetCurrentFrame().GetHeight(),
		cursor_position * scale_x ,
		(float)cursor_animation.GetCurrentFrame().GetPositionY() * scale_y,
		(float)cursor_animation.GetCurrentFrame().GetWidth() * scale_x * 3,
		(float)cursor_animation.GetCurrentFrame().GetHeight() * scale_y * 3,
		NULL);

	position_x = 75;

	for (int i = 0; i < 4; i++)
	{
		hero_stand_image.DrawScaledImage(hero_stands_animation.GetCurrentFrame().GetSourceX(),
			hero_stands_animation.GetCurrentFrame().GetSourceY(),
			hero_stands_animation.GetCurrentFrame().GetWidth(),
			hero_stands_animation.GetCurrentFrame().GetHeight(),
			position_x + (200 * i),
			500,
			hero_stands_animation.GetCurrentFrame().GetWidth() * scale_x,
			hero_stands_animation.GetCurrentFrame().GetHeight() * scale_y,
			NULL);

		position_x += 100;
	}
}

void HeroSelect::UpdateHeroSelect()
{
	background_crowd_animation.Update();
	hero_stands_animation.Update();

	for (auto const& x : unlocked_heroes)
	{
		x.second->UpdateUnitAnimation();
	} // End for loop
}

void HeroSelect::MoveCursorLeft()
{
	if (cursor_choice > 1)
	{
		cursor_choice -= 1;
		printf("Current Choice: %d\n", cursor_choice);
		moving_cursor.PlaySongOnce();
	}
	if (cursor_position > 275 && cursor_choice < 3)
	{
		cursor_position -= 300;
	}
}

void HeroSelect::MoveCursorRight()
{
	if (cursor_position < 875)
	{
		cursor_position += 300;
	}
	if (cursor_choice < max_choice)
	{
		cursor_choice += 1;
		printf("Current Choice: %d\n", cursor_choice);
		moving_cursor.PlaySongOnce();
	}
}

int HeroSelect::SelectHero()
{
	select_option.PlaySongOnce();

	int which_hero =  1;											// Holds which hero is being selected
		
	for (auto const& x : unlocked_heroes)
	{
		if (which_hero == cursor_choice)
		{
			which_hero = x.first;
			break;
		}

		which_hero += 1;

	} // End for loop

	if (hero_team.count(which_hero) == 0)
	{
		if (hero_team_count == 4 || hero_team_count == max_choice)
		{
			team_selected = true;										// Full team has been selected &&
			return 0;													// Unit will not be selected for team 
		}
		else
		{
			hero_team[which_hero] = new Unit();							// Create the unit so it can be shown on the screen
																		// Set up the animation for which animation to use
			printf("Hero Selected: %d\n", which_hero);
			hero_team_count += 1;										// Increment the hero count by 1


			return which_hero;
		}
	}
	else
	{
		return -1;														// Unit will not be selected for team
	}
}