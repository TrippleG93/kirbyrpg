#pragma once

/*
* Input class
* Takes in input from the controller
* Takes in input from a keyboard
* Does not allow mouse support
* Does allow re-sync with controllers
*/

#include "allegro5\allegro.h"

#include <map>

enum KeyCodes
{
	KEYCODE_A = 1,
	KEYCODE_B,
	KEYCODE_C,
	KEYCODE_D,
	KEYCODE_E,
	KEYCODE_F,
	KEYCODE_G,
	KEYCODE_H,
	KEYCODE_I,
	KEYCODE_J,
	KEYCODE_K,
	KEYCODE_L,
	KEYCODE_M,
	KEYCODE_N,
	KEYCODE_O,
	KEYCODE_P,
	KEYCODE_Q,
	KEYCODE_R,
	KEYCODE_S,
	KEYCODE_T,
	KEYCODE_U,
	KEYCODE_V,
	KEYCODE_W,
	KEYCODE_X,
	KEYCODE_Y,
	KEYCODE_Z,
	KEYCODE_0,
	KEYCODE_1,
	KEYCODE_2,
	KEYCODE_3,
	KEYCODE_4,
	KEYCODE_5,
	KEYCODE_6,
	KEYCODE_7,
	KEYCODE_8,
	KEYCODE_9,
	KEYCODE_0_PAD,
	KEYCODE_1_PAD,
	KEYCODE_2_PAD,
	KEYCODE_3_PAD,
	KEYCODE_4_PAD,
	KEYCODE_5_PAD,
	KEYCODE_6_PAD,
	KEYCODE_7_PAD,
	KEYCODE_8_PAD,
	KEYCODE_9_PAD,
	KEYCODE_F1,
	KEYCODE_F2,
	KEYCODE_F3,
	KEYCODE_F4,
	KEYCODE_F5,
	KEYCODE_F6,
	KEYCODE_F7,
	KEYCODE_F8,
	KEYCODE_F9,
	KEYCODE_F10,
	KEYCODE_F11,
	KEYCODE_F12,
	KEYCODE_ESCAPE,
	KEYCODE_TILDE,
	KEYCODE_DASH,
	KEYCODE_EQUAL,
	KEYCODE_BACKSPACE,
	KEYCODE_TAB,
	KEYCODE_LEFTBRACKET,
	KEYCODE_RIGHTBRACKET,
	KEYCODE_ENTER,
	KEYCODE_SEMICOLON,
	KEYCODE_SINGLEQUOTE,
	KEYCODE_SLASH,
	KEYCODE_COMMA = 72,
	KEYCODE_PERIOD,
	KEYCODE_QUESTIONMARK,
	KEYCODE_SPACE,
	KEYCODE_LEFTARROW = 82,
	KEYCODE_RIGHTARROW,
	KEYCODE_UPARROW,
	KEYCODE_DOWNARROW,
	KEYCDOE_LEFTSHIFT = 215,
	KEYCODE_RIGHTSHIFT




};

// enum that handles all controller input
enum AbstractControlIndex
{
	// Buttons 
	AINDEX_A_BUTTON,																// A button on xbox controller
	AINDEX_B_BUTTON,																// B button on xbox controller
	AINDEX_X_BUTTON,																// X button on xbox controller
	AINDEX_Y_BUTTON,																// Y button on xbox controller

																					// Bumber
	AINDEX_RIGHTBUMPER_BUTTON,														// Right Bumper button on xbox controller
	AINDEX_LEFTBUMPER_BUTTON,														// Left Bumper button on xbox controller

																					// Thumbstick Buttons
	AINDEX_RIGHTTHUMBSTICK_BUTTONS,													// Right thumbstick being pressed on xbox controller
	AINDEX_LEFTTHUMBSTICK_BUTTONS,													// Left thumbstick being pressed on xbox controller

																					// Start & select buttons
	AINDEX_SELECT_BUTTON,															// Select button on xbox controller 
	AINDEX_START_BUTTON,															// Start button on xbox controller

																					// D-Pad
	AINDEX_RIGHT_DPAD,																// Right press on d-pad on xbox controller
	AINDEX_LEFT_DPAD,																// Left press on d-pad on xbox controller
	AINDEX_DOWN_DPAD,																// Down press on d-pad on xbox controller
	AINDEX_UP_DPAD,																	// Up press on d-pad on xbox controller

	AINDEX_LEFT_LANALOG = 20,														// Left Analog Movement Left
	AINDEX_RIGHT_LANALOG,															// Left Analog Movement Right
	AINDEX_UP_LANALOG,																// Left Analog Movement UP 
	AINDEX_DOWN_LANALOG,															// Left Analog Movement Down

	AINDEX_LEFT_RANALOG,															// Right Analog Movement Left
	AINDEX_RIGHT_RANALOG,															// Right Analog Movement Right
	AINDEX_UP_RANALOG,																// Right Analog Movement Up
	AINDEX_DOWN_RANALOG,															// Right Analog Movement Down

	AINDEX_LEFT_TRIGGER,															// Left Trigger 
	AINDEX_RIGHT_TRIGGER,															// Right Trigger
};

class Input
{
public:
	/*
	* Default constructor that will Initalize the number of current controllers and
	* if a controller is being used
	*/
	Input();
	~Input();

	/*
	* SetUpControllers
	* Syncs up what devices that are pluged into the computer (Controllers)
	* If controllers are not set up then it is ok
	* Call this when Initializing the input
	*/
	void SetUpControllers();

	/*
	* ResyncControllers();
	* resync controllers that have been disconected
	* It is ok if all controllers have been removed because keyboard will work
	* This is only for the case of a game that supports both inputs
	* Call this while the game is running
	*/
	void ResyncControllers();

	/*
	* get_Joystick
	* Will Return a joystick based on what contoller number you ask for
	*/
	ALLEGRO_JOYSTICK* GetJoystick(int controller);

	/*
	* UpdateKeyboardInput
	* updates the mapping variables based on the keys that were presses
	* It will update pressed keys and released keys
	* If a key is still pressed then it is considered as held as well
	*/
	void UpdateKeyboardInput(const ALLEGRO_EVENT &events);

	/*
	* UpdateControllerInput
	* updates the mapping variables based on the input from the controller
	* It will update pressed buttons and released buttons
	* If a button is pressed then it is considered as held
	* It will also handle analog movement
	*/
	void UpdateControllerInput(const ALLEGRO_EVENT &events);

	/*
	* keyDownEvent
	* When a key has been pressed it will store it in _pressedKey
	*/
	void KeyDownEvent(const ALLEGRO_EVENT& events);

	/*
	* keyUpEvent
	* This gets called when a key is released
	*/
	void KeyUpEvent(const ALLEGRO_EVENT& events);

	/*
	* ButtonDownEvent
	* This gets called when a button is pressed
	*/
	void ButtonDownEvent(const ALLEGRO_EVENT &events);

	/*
	* ButtonUpEvent
	* This gets called when a button is released
	*/
	void ButtonUpEvent(const ALLEGRO_EVENT &events);

	/*
	* AnalogMoveEvent
	* This gets called when an analog is moved or one of the triggers
	* on the controller
	*/
	void AnalogMoveEvent(const ALLEGRO_EVENT &events, AbstractControlIndex analog);

	/*
	* AnalogNotMoveEvent
	* This gets called when the analog or trigger is at rest position
	* on the controller
	*/
	void AnalogNotMoveEvent(const ALLEGRO_EVENT &events, AbstractControlIndex analog);

	/*
	* ClearControllerInput
	* clear the mapping variable of a specific controller input
	*/
	void ClearControllerInput(int controller);

	/*ClearKeyBoardInputs
	 * Clears all the inputs made by the user that is currently active
	 */
	void ClearKeyBoardInputs();

	/*
	* MatchController
	* returns which controller made the input (Numerical value)
	*/
	int MatchController(const ALLEGRO_EVENT &events);

	/*
	* ButtonHeldEvent
	* If the pressed button mapping variable is true for that button value
	* then the held key value will be set to true in the mapping variable
	*/
	void ButtonHeldEvent();

	/*
	* key_Held_Event
	* if the pressed key mapping variable is true for that key value
	* then the held key value will be set to true in the mapping variable
	*/
	void KeyHeldEvent();


	/*
	* 	Getters
	*/
	int GetHowManyControllers() { return Input::how_many_controllers; }
	bool WasButtonPressed(int current_controller, int button);
	bool WasButtonHeld(int current_controller, int button);
	bool WasKeyPressed(int key);
	bool WasKeyHeld(int key);

private:
	bool using_controllers;					// Is the user using a controller
	int how_many_controllers;				// How many controllers are being used in the program

	ALLEGRO_JOYSTICK *joysticks[4];

	/*
	* Used to store keyboard input
	*/

	/*
	* Used to store controller input
	*/

	typedef struct x_Input
	{
		// Axis Values
		float left_stick_x;
		float left_stick_y;
		float left_trigger;
		float right_stick_x;
		float right_stick_y;
		float right_trigger;
		float dpad_x;
		float dpad_y;

		// Buttons Pressed
		bool button_a;
		bool button_b;
		bool button_x;
		bool button_y;
		bool button_left_stick;
		bool button_left_stick_left;
		bool button_left_stick_right;
		bool button_left_stick_up;
		bool button_left_stick_down;
		bool button_right_stick;
		bool button_right_stick_left;
		bool button_right_stick_right;
		bool button_right_stick_up;
		bool button_right_stick_down;
		bool button_left_bumper;
		bool button_left_trigger;
		bool button_right_bumber;
		bool button_right_trigger;
		bool button_start;
		bool button_select;
		bool dpad_left;
		bool dpad_right;
		bool dpad_up;
		bool dpad_down;

		// Buttons Held;
		bool held_button_a;
		bool held_button_b;
		bool held_button_x;
		bool held_button_y;
		bool held_button_left_stick;
		bool held_button_left_stick_left;
		bool held_button_left_stick_right;
		bool held_button_left_stick_up;
		bool held_button_left_stick_down;
		bool held_button_right_stick;
		bool held_button_right_stick_left;
		bool held_button_right_stick_right;
		bool held_button_right_stick_up;
		bool held_button_right_stick_down;
		bool held_button_left_bumper;
		bool held_button_left_trigger;
		bool held_button_right_bumber;
		bool held_button_right_trigger;
		bool held_button_start;
		bool held_button_select;
		bool held_dpad_left;
		bool held_dpad_right;
		bool held_dpad_up;
		bool held_dpad_down;

		struct x_Input *next;
	}x_Input;

	x_Input *head = nullptr;						// Holds the beginning of the linked list that holds information of the controller

	typedef struct k_input
	{
		// Keyboard pressed
		bool a_key;
		bool b_key;
		bool c_key;
		bool d_key;
		bool e_key;
		bool f_key;
		bool g_key;
		bool h_key;
		bool i_key;
		bool j_key;
		bool k_key;
		bool l_key;
		bool m_key;
		bool n_key;
		bool o_key;
		bool p_key;
		bool q_key;
		bool r_key;
		bool s_key;
		bool t_key;
		bool u_key;
		bool v_key;
		bool w_key;
		bool x_key;
		bool y_key;
		bool z_key;
		bool zero_key;
		bool zero_pad_key;
		bool one_key;
		bool one_pad_key;
		bool two_key;
		bool two_pad_key;
		bool three_key;
		bool three_pad_key;
		bool four_key;
		bool four_pad_key;
		bool five_key;
		bool five_pad_key;
		bool six_key;
		bool six_pad_key;
		bool seven_key;
		bool seven_pad_key;
		bool eight_key;
		bool eight_pad_key;
		bool nine_key;
		bool nine_pad_key;
		bool shift_left_key;
		bool shift_right_key;
		bool minus_key;
		bool f1_key;
		bool f2_key;
		bool f3_key;
		bool f4_key;
		bool f5_key;
		bool f6_key;
		bool f7_key;
		bool f8_key;
		bool f9_key;
		bool f10_key;
		bool f11_key;
		bool f12_key;
		bool escape_key;
		bool tilde_key;
		bool dash_key;
		bool equal_key;
		bool backspace_key;
		bool tab_key;
		bool left_bracket_key;
		bool right_bracket_key;
		bool enter_key;
		bool semicolon_key;
		bool single_quote_key;
		bool slash_key;
		bool comma_key;
		bool period_key;
		bool questionmark_key;
		bool space_key;
		bool left_arrow_key;
		bool right_arrow_key;
		bool up_arrow_key;
		bool down_arrow_key;
		bool left_shift_key;
		bool right_shift_key;

		// Keyboard Held
		// Keyboard pressed
		bool a_key_held;
		bool b_key_held;
		bool c_key_held;
		bool d_key_held;
		bool e_key_held;
		bool f_key_held;
		bool g_key_held;
		bool h_key_held;
		bool i_key_held;
		bool j_key_held;
		bool k_key_held;
		bool l_key_held;
		bool m_key_held;
		bool n_key_held;
		bool o_key_held;
		bool p_key_held;
		bool q_key_held;
		bool r_key_held;
		bool s_key_held;
		bool t_key_held;
		bool u_key_held;
		bool v_key_held;
		bool w_key_held;
		bool x_key_held;
		bool y_key_held;
		bool z_key_held;
		bool zero_key_held;
		bool zero_pad_key_held;
		bool one_key_held;
		bool one_pad_key_held;
		bool two_key_held;
		bool two_pad_key_held;
		bool three_key_held;
		bool three_pad_key_held;
		bool four_key_held;
		bool four_pad_key_held;
		bool five_key_held;
		bool five_pad_key_held;
		bool six_key_held;
		bool six_pad_key_held;
		bool seven_key_held;
		bool seven_pad_key_held;
		bool eight_key_held;
		bool eight_pad_key_held;
		bool nine_key_held;
		bool nine_pad_key_held;
		bool shift_left_key_held;
		bool shift_right_key_held;
		bool tab_key_held;
		bool minus_key_held;
		bool f1_key_held_held;
		bool f2_key_held_held;
		bool f3_key_held_held;
		bool f4_key_held_held;
		bool f5_key_held_held;
		bool f6_key_held_held;
		bool f7_key_held_held;
		bool f8_key_held_held;
		bool f9_key_held_held;
		bool f10_key_held_held;
		bool f11_key_held_held;
		bool f12_key_held_held;
		bool escape_key_held;
		bool tilde_key_held;
		bool dash_key_held;
		bool equal_key_held;
		bool backspace_key_held;
		bool tab_key_held_held;
		bool left_bracket_key_held;
		bool right_bracket_key_held;
		bool enter_key_held;
		bool semicolon_key_held;
		bool single_quote_key_held;
		bool slash_key_held;
		bool comma_key_held;
		bool period_key_held;
		bool questionmark_key_held;
		bool space_key_held;
		bool left_arrow_key_held;
		bool right_arrow_key_held;
		bool up_arrow_key_held;
		bool down_arrow_key_held;
		bool left_shift_key_held;
		bool right_shift_key_held;
	}k_input;

	k_input Keyboard_Controls;
};


