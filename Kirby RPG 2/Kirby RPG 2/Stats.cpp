#include "Stats.h"

#include <fstream>
#include <cstdlib>

Stats::Stats()
{

}

Stats::Stats(bool enemy, int type)
{
	if (enemy == true)
	{
		LoadStatEnemies(type);
		SetUpAnimationEnemies(type);
	}
	else
	{
		LoadStatKirbies(type);
		SetUpAnimationKirbies(type);
	}
}


Stats::~Stats()
{
}

void Stats::SetUpAnimationKirbies(int type)
{
	switch (type)
	{
	case 1:		// Loading animal kirby character
		character_image.LoadBitmap("Content/Kirby Characters/DS DSi - Kirby Squeak Squad - Animal Kirby.png");

		character_animation.AddIndividualFrame(0, 0, 8, 51, 27, 25);
		character_animation.AddIndividualFrame(0, 0, 40, 51, 23, 24);
		character_animation.AddIndividualFrame(0, 0, 71, 50, 22, 25);
		character_animation.AddIndividualFrame(0, 0, 100, 50, 22, 25);
		character_animation.AddIndividualFrame(0, 0, 129, 50, 23, 25);
		character_animation.AddIndividualFrame(0, 0, 162, 50, 22, 24);
		character_animation.AddIndividualFrame(0, 0, 193, 50, 23, 24);
		character_animation.AddIndividualFrame(0, 0, 225, 50, 24, 24);
		character_animation.CombineFrames(10);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);

		break;
	case 2:		// Loading Fighter kirby character
		character_image.LoadBitmap("Content/Kirby Characters/DS DSi - Kirby Super Star Ultra - Fighter Kirby.png");

		character_animation.AddIndividualFrame(0, 0, 133, 47, 35, 25);
		character_animation.AddIndividualFrame(0, 0, 174, 48, 31, 24);
		character_animation.AddIndividualFrame(0, 0, 213, 50, 30, 22);
		character_animation.AddIndividualFrame(0, 0, 251, 49, 31, 23);
		character_animation.AddIndividualFrame(0, 0, 289, 49, 33, 23);
		character_animation.AddIndividualFrame(0, 0, 330, 49, 32, 23);
		character_animation.AddIndividualFrame(0, 0, 371, 49, 30, 23);
		character_animation.AddIndividualFrame(0, 0, 407, 49, 33, 23);
		character_animation.CombineFrames(10);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 3:		// Loading Cutter kirby character
		character_image.LoadBitmap("Content/Kirby Characters/DS DSi - Kirby Super Star Ultra - Cutter Kirby.png");

		character_animation.AddIndividualFrame(0, 0, 227, 5, 30, 34);
		character_animation.AddIndividualFrame(0, 0, 262, 6, 29, 33);
		character_animation.AddIndividualFrame(0, 0, 293, 7, 28, 32);
		character_animation.AddIndividualFrame(0, 0, 323, 8, 28, 31);
		character_animation.AddIndividualFrame(0, 0, 354, 7, 26, 32);
		character_animation.AddIndividualFrame(0, 0, 386, 5, 26, 34);
		character_animation.AddIndividualFrame(0, 0, 416, 7, 28, 32);
		character_animation.AddIndividualFrame(0, 0, 447, 9, 28, 30);
		character_animation.AddIndividualFrame(0, 0, 483, 8, 29, 31);
		character_animation.AddIndividualFrame(0, 0, 520, 6, 30, 33);
		character_animation.CombineFrames(10);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 4:		// Loading Fire kirby character
		character_image.LoadBitmap("Content/Kirby Characters/DS DSi - Kirby Super Star Ultra - Fire Kirby.png");

		character_animation.AddIndividualFrame(0, 0, 118, 852, 35, 39);
		character_animation.AddIndividualFrame(0, 0, 158, 852, 32, 39);
		character_animation.AddIndividualFrame(0, 0, 195, 853, 31, 38);
		character_animation.AddIndividualFrame(0, 0, 235, 851, 27, 40);
		character_animation.AddIndividualFrame(0, 0, 267, 854, 29, 37);
		character_animation.AddIndividualFrame(0, 0, 301, 852, 30, 39);
		character_animation.AddIndividualFrame(0, 0, 336, 852, 29, 39);
		character_animation.AddIndividualFrame(0, 0, 370, 851, 27, 40);
		character_animation.CombineFrames(10);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 5:		// Loading for regular kirby
		character_image.LoadBitmap("Content/Kirby Characters/DS DSi - Kirby Super Star Ultra - Kirby.png");

		character_animation.AddIndividualFrame(0, 0, 116, 198, 26, 23);
		character_animation.AddIndividualFrame(0, 0, 147, 199, 24, 22);
		character_animation.AddIndividualFrame(0, 0, 176, 201, 23, 20);
		character_animation.AddIndividualFrame(0, 0, 204, 200, 23, 21);
		character_animation.AddIndividualFrame(0, 0, 232, 199, 24, 22);
		character_animation.AddIndividualFrame(0, 0, 261, 198, 26, 23);
		character_animation.AddIndividualFrame(0, 0, 292, 199, 24, 22);
		character_animation.AddIndividualFrame(0, 0, 321, 201, 23, 20);
		character_animation.AddIndividualFrame(0, 0, 349, 200, 21, 21);
		character_animation.AddIndividualFrame(0, 0, 375, 199, 22, 22);
		character_animation.CombineFrames(12);


		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 6:		// Loading for ninja kirby
		character_image.LoadBitmap("Content/Kirby Characters/DS DSi - Kirby Super Star Ultra - Ninja Kirby.png");

		character_animation.AddIndividualFrame(0, 0, 117, 357, 36, 31);
		character_animation.AddIndividualFrame(0, 0, 158, 357, 31, 31);
		character_animation.AddIndividualFrame(0, 0, 194, 358, 30, 30);
		character_animation.AddIndividualFrame(0, 0, 229, 358, 30, 30);
		character_animation.AddIndividualFrame(0, 0, 264, 357, 30, 31);
		character_animation.AddIndividualFrame(0, 0, 299, 358, 29, 30);
		character_animation.AddIndividualFrame(0, 0, 333, 358, 30, 30);
		character_animation.AddIndividualFrame(0, 0, 368, 357, 31, 31);
		character_animation.CombineFrames(10);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 7:		// Loading for jet kirby
		character_image.LoadBitmap("Content/Kirby Characters/DS DSi - Kirby Super Star Ultra - Jet Kirby.png");

		character_animation.AddIndividualFrame(0, 0, 225, 6, 28, 31);
		character_animation.AddIndividualFrame(0, 0, 260, 7, 26, 30);
		character_animation.AddIndividualFrame(0, 0, 293, 6, 28, 31);
		character_animation.AddIndividualFrame(0, 0, 326, 7, 29, 30);
		character_animation.AddIndividualFrame(0, 0, 360, 6, 29, 31);
		character_animation.AddIndividualFrame(0, 0, 395, 5, 30, 32);
		character_animation.AddIndividualFrame(0, 0, 431, 6, 29, 31);
		character_animation.AddIndividualFrame(0, 0, 465, 8, 29, 29);
		character_animation.AddIndividualFrame(0, 0, 499, 7, 28, 30);
		character_animation.AddIndividualFrame(0, 0, 534, 7, 25, 30);
		character_animation.CombineFrames(12);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 8:		// Loading Ice kirby
		character_image.LoadBitmap("Content/Kirby Characters/DS DSi - Kirby Super Star Ultra - Ice Kirby.png");

		character_animation.AddIndividualFrame(0, 0, 114, 5, 30, 34);
		character_animation.AddIndividualFrame(0, 0, 152, 5, 27, 34);
		character_animation.AddIndividualFrame(0, 0, 189, 6, 25, 33);
		character_animation.AddIndividualFrame(0, 0, 226, 5, 25, 34);
		character_animation.AddIndividualFrame(0, 0, 261, 5, 27, 34);
		character_animation.AddIndividualFrame(0, 0, 297, 4, 27, 35);
		character_animation.AddIndividualFrame(0, 0, 330, 5, 27, 34);
		character_animation.AddIndividualFrame(0, 0, 366, 6, 25, 33);
		character_animation.AddIndividualFrame(0, 0, 396, 6, 25, 33);
		character_animation.AddIndividualFrame(0, 0, 430, 5, 25, 34);
		character_animation.CombineFrames(12);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 9:		// Loading Stone Kirby
		character_image.LoadBitmap("Content/Kirby Characters/DS DSi - Kirby Super Star Ultra - Stone Kirby.png");

		character_animation.AddIndividualFrame(0, 0, 50, 276, 35, 32);
		character_animation.AddIndividualFrame(0, 0, 90, 276, 34, 32);
		character_animation.AddIndividualFrame(0, 0, 129, 277, 27, 31);
		character_animation.AddIndividualFrame(0, 0, 161, 278, 26, 30);
		character_animation.AddIndividualFrame(0, 0, 192, 278, 25, 30);
		character_animation.AddIndividualFrame(0, 0, 222, 277, 26, 31);
		character_animation.AddIndividualFrame(0, 0, 253, 276, 27, 32);
		character_animation.AddIndividualFrame(0, 0, 285, 276, 27, 32);
		character_animation.AddIndividualFrame(0, 0, 317, 277, 26, 31);
		character_animation.AddIndividualFrame(0, 0, 348, 278, 25, 30);
		character_animation.AddIndividualFrame(0, 0, 378, 278, 25, 30);
		character_animation.AddIndividualFrame(0, 0, 408, 277, 29, 31);
		character_animation.CombineFrames(8);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 10:	// Loading Plasma Kirby
		character_image.LoadBitmap("Content/Kirby Characters/DS DSi - Kirby Super Star Ultra - Plasma Kirby.png");

		character_animation.AddIndividualFrame(0, 0, 118, 852, 35, 39);
		character_animation.AddIndividualFrame(0, 0, 158, 852, 32, 39);
		character_animation.AddIndividualFrame(0, 0, 195, 853, 31, 38);
		character_animation.AddIndividualFrame(0, 0, 235, 851, 27, 40);
		character_animation.AddIndividualFrame(0, 0, 267, 855, 29, 36);
		character_animation.AddIndividualFrame(0, 0, 301, 853, 30, 38);
		character_animation.AddIndividualFrame(0, 0, 336, 853, 29, 38);
		character_animation.AddIndividualFrame(0, 0, 370, 851, 27, 40);
		character_animation.CombineFrames(12);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);

		break;
	default:
		printf("Kirby type %d does not exist\n", type);
		break;
	}

	stat_background_image.LoadBitmap("Content/Backgrounds/Stat Background.png");
	
	stats_bar_image.LoadBitmap("Content/Miscellaneous/SNES - Kirby Super Star Kirbys Fun Pak - Dyna Blade Bonus Game.png");

	health_animation.AddIndividualFrame(0, 0, 26, 10, 8, 8);
	health_animation.AddIndividualFrame(0, 0, 42, 10, 8, 8);
	health_animation.CombineFrames(30);

	attack_animation.AddIndividualFrame(0, 0, 26, 48, 8, 8);
	attack_animation.AddIndividualFrame(0, 0, 26, 10, 8, 8);
	attack_animation.CombineFrames(30);

	special_attack_animation.AddIndividualFrame(0, 0, 26, 86, 8, 8);
	special_attack_animation.AddIndividualFrame(0, 0, 34, 86, 8, 8);
	special_attack_animation.CombineFrames(30);

	defence_animation.AddIndividualFrame(0, 0, 160, 102, 8, 8);
	defence_animation.AddIndividualFrame(0, 0, 168, 102, 8, 8);
	defence_animation.CombineFrames(30);

	special_defence_animation.AddIndividualFrame(0, 0, 248, 102, 8, 8);
	special_defence_animation.AddIndividualFrame(0, 0, 240, 102, 8, 8);
	special_defence_animation.CombineFrames(30);

	speed_animation.AddIndividualFrame(0, 0, 216, 102, 8, 8);
	speed_animation.AddIndividualFrame(0, 0, 224, 102, 8, 8);
	speed_animation.CombineFrames(30);

	vincentio_font12.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 12);
	health_text_image.CreateTextRender(vincentio_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Health:");
	attack_text_image.CreateTextRender(vincentio_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Attack:");
	special_attack_text_image.CreateTextRender(vincentio_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Sp. Attack:");
	defence_text_image.CreateTextRender(vincentio_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Defence:");
	special_defence_text_image.CreateTextRender(vincentio_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Sp. Def:");
	speed_text_image.CreateTextRender(vincentio_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Speed:");
}

void Stats::SetUpAnimationEnemies(int type)
{
	switch (type)
	{
	case 1:		// Loading Ninja enemy character
		character_image.LoadBitmap("Content/Enemies/DS DSi - Kirby Super Star Ultra - Bio Spark.png");

		character_animation.AddIndividualFrame(0, 0, 5, 705, 24, 25);
		character_animation.AddIndividualFrame(0, 0, 35, 706, 26, 24);
		character_animation.AddIndividualFrame(0, 0, 66, 707, 26, 23);
		character_animation.AddIndividualFrame(0, 0, 97, 703, 27, 26);
		character_animation.AddIndividualFrame(0, 0, 128, 704, 27, 26);
		character_animation.AddIndividualFrame(0, 0, 160, 705, 27, 25);
		character_animation.AddIndividualFrame(0, 0, 192, 703, 23, 27);
		character_animation.AddIndividualFrame(0, 0, 220, 704, 25, 26);
		character_animation.AddIndividualFrame(0, 0, 250, 705, 25, 25);
		character_animation.CombineFrames(10);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);

		break;
	case 2:		// Loading Fighter kirby character
		character_image.LoadBitmap("Content/Enemies/DS DSi - Kirby Super Star Ultra - Sword Knight.png");

		character_animation.AddIndividualFrame(0, 0, 120, 263, 34, 44);
		character_animation.AddIndividualFrame(0, 0, 161, 270, 43, 37);
		character_animation.AddIndividualFrame(0, 0, 209, 275, 68, 40);
		character_animation.AddIndividualFrame(0, 0, 293, 284, 40, 34);
		character_animation.AddIndividualFrame(0, 0, 347, 284, 43, 33);
		character_animation.AddIndividualFrame(0, 0, 396, 284, 48, 29);
		character_animation.AddIndividualFrame(0, 0, 459, 283, 28, 24);
		character_animation.CombineFrames(8);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 3:		// Loading Wheelie Enemy
		character_image.LoadBitmap("Content/Enemies/DS DSi - Kirby Super Star Ultra - Wheelie Bike.png");

		character_animation.AddIndividualFrame(0, 0, 9, 58, 27, 25);
		character_animation.AddIndividualFrame(0, 0, 38, 57, 27, 26);
		character_animation.AddIndividualFrame(0, 0, 67, 57, 27, 26);
		character_animation.AddIndividualFrame(0, 0, 96, 58, 27, 25);
		character_animation.CombineFrames(10);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 4:		// Loading Gim Character
		character_image.LoadBitmap("Content/Enemies/DS DSi - Kirby Super Star Ultra - Gim.png");

		character_animation.AddIndividualFrame(0, 0, 37, 129, 26, 26);
		character_animation.AddIndividualFrame(0, 0, 78, 129, 34, 26);
		character_animation.AddIndividualFrame(0, 0, 117, 129, 29, 26);
		character_animation.AddIndividualFrame(0, 0, 151, 129, 26, 26);
		character_animation.AddIndividualFrame(0, 0, 182, 130, 27, 25);
		character_animation.AddIndividualFrame(0, 0, 214, 130, 29, 25);
		character_animation.CombineFrames(10);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 5:		// Boss #1 Mr. Tic-Tock
		character_image.LoadBitmap("Content/Enemies/DS DSi - Kirby Super Star Ultra - True Mid-Bosses.png");

		character_animation.AddIndividualFrame(0, 0, 24, 46, 52, 40);
		character_animation.AddIndividualFrame(0, 0, 81, 47, 58, 39);
		character_animation.AddIndividualFrame(0, 0, 144, 48, 62, 38);
		character_animation.CombineFrames(12);


		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 6:		// Loading Plasma Enemy 
		character_image.LoadBitmap("Content/Enemies/DS DSi - Kirby Super Star Ultra - Plasma Wisp.png");

		character_animation.AddIndividualFrame(0, 0, 21, 51, 43, 37);
		character_animation.AddIndividualFrame(0, 0, 69, 48, 43, 42);
		character_animation.AddIndividualFrame(0, 0, 117, 45, 43, 46);
		character_animation.AddIndividualFrame(0, 0, 165, 44, 44, 46);
		character_animation.AddIndividualFrame(0, 0, 214, 51, 45, 38);
		character_animation.AddIndividualFrame(0, 0, 264, 48, 44, 42);
		character_animation.AddIndividualFrame(0, 0, 313, 45, 43, 46);
		character_animation.AddIndividualFrame(0, 0, 361, 44, 43, 46);
		character_animation.CombineFrames(10);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 7:		// Loading Chilly enemy
		character_image.LoadBitmap("Content/Enemies/DS DSi - Kirby Super Star Ultra - Chilly.png");

		character_animation.AddIndividualFrame(0, 0, 5, 43, 28, 30);
		character_animation.AddIndividualFrame(0, 0, 38, 44, 29, 29);
		character_animation.AddIndividualFrame(0, 0, 72, 45, 31, 28);
		character_animation.AddIndividualFrame(0, 0, 108, 45, 29, 30);
		character_animation.AddIndividualFrame(0, 0, 144, 44, 29, 29);
		character_animation.AddIndividualFrame(0, 0, 178, 43, 28, 30);
		character_animation.CombineFrames(12);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 8:		// Loading Burning Leo Enemy
		character_image.LoadBitmap("Content/Enemies/DS DSi - Kirby Super Star Ultra - Burning Leo.png");

		character_animation.AddIndividualFrame(0, 0, 52, 132, 31, 31);
		character_animation.AddIndividualFrame(0, 0, 88, 131, 30, 32);
		character_animation.AddIndividualFrame(0, 0, 123, 131, 30, 32);
		character_animation.AddIndividualFrame(0, 0, 158, 134, 31, 29);
		character_animation.AddIndividualFrame(0, 0, 194, 134, 32, 29);
		character_animation.AddIndividualFrame(0, 0, 231, 132, 31, 31);
		character_animation.CombineFrames(12);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 9:		// Loading Tac Enemy
		character_image.LoadBitmap("Content/Enemies/DS DSi - Kirby Super Star Ultra - TAC.png");

		character_animation.AddIndividualFrame(0, 0, 28, 208, 39, 36);
		character_animation.AddIndividualFrame(0, 0, 72, 207, 38, 37);
		character_animation.AddIndividualFrame(0, 0, 115, 207, 36, 37);
		character_animation.AddIndividualFrame(0, 0, 156, 208, 34, 36);
		character_animation.AddIndividualFrame(0, 0, 195, 209, 37, 35);
		character_animation.AddIndividualFrame(0, 0, 237, 210, 38, 34);
		character_animation.CombineFrames(8);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);
		break;
	case 10:	// Loading Boss #2 Fire-Lion Enemy
		character_image.LoadBitmap("Content/Enemies/DS DSi - Kirby Super Star Ultra - True Mid-Bosses.png");

		character_animation.AddIndividualFrame(0, 0, 75, 907, 64, 64);
		character_animation.AddIndividualFrame(0, 0, 144, 907, 62, 64);
		character_animation.AddIndividualFrame(0, 0, 211, 908, 63, 63);
		character_animation.AddIndividualFrame(0, 0, 279, 907, 64, 64);
		character_animation.CombineFrames(12);

		vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);

		kirby_name_image.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(255, 255, 255), name);

		break;
	default:
		printf("Kirby type %d does not exist\n", type);
		break;
	}

	stat_background_image.LoadBitmap("Content/Backgrounds/Stat Background.png");

	stats_bar_image.LoadBitmap("Content/Miscellaneous/SNES - Kirby Super Star Kirbys Fun Pak - Dyna Blade Bonus Game.png");

	health_animation.AddIndividualFrame(0, 0, 26, 10, 8, 8);
	health_animation.AddIndividualFrame(0, 0, 42, 10, 8, 8);
	health_animation.CombineFrames(30);

	attack_animation.AddIndividualFrame(0, 0, 26, 48, 8, 8);
	attack_animation.AddIndividualFrame(0, 0, 26, 10, 8, 8);
	attack_animation.CombineFrames(30);

	special_attack_animation.AddIndividualFrame(0, 0, 26, 86, 8, 8);
	special_attack_animation.AddIndividualFrame(0, 0, 34, 86, 8, 8);
	special_attack_animation.CombineFrames(30);

	defence_animation.AddIndividualFrame(0, 0, 160, 102, 8, 8);
	defence_animation.AddIndividualFrame(0, 0, 168, 102, 8, 8);
	defence_animation.CombineFrames(30);

	special_defence_animation.AddIndividualFrame(0, 0, 248, 102, 8, 8);
	special_defence_animation.AddIndividualFrame(0, 0, 240, 102, 8, 8);
	special_defence_animation.CombineFrames(30);

	speed_animation.AddIndividualFrame(0, 0, 216, 102, 8, 8);
	speed_animation.AddIndividualFrame(0, 0, 224, 102, 8, 8);
	speed_animation.CombineFrames(30);

	vincentio_font12.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 12);
	health_text_image.CreateTextRender(vincentio_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Health:");
	attack_text_image.CreateTextRender(vincentio_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Attack:");
	special_attack_text_image.CreateTextRender(vincentio_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Sp. Attack:");
	defence_text_image.CreateTextRender(vincentio_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Defence:");
	special_defence_text_image.CreateTextRender(vincentio_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Sp. Def:");
	speed_text_image.CreateTextRender(vincentio_font12.ObtainFont(), al_map_rgb(255, 255, 255), "Speed:");
}

void Stats::RenderKirbyInfo(float position_x, float position_y, float scale_x, float scale_y)
{
	stat_background_image.DrawScaledImage(
		0, 0,
		stat_background_image.GetBitmapWidth(),
		stat_background_image.GetBitmapHeight(),
		position_x * scale_x,
		position_y * scale_y,
		stat_background_image.GetBitmapWidth() * scale_x,
		stat_background_image.GetBitmapHeight() * scale_y,
		NULL);

	for (int i = 1; i < 5; i++)
	{
		if (i <= hit_points_growth)
		{
			stats_bar_image.DrawScaledImage(
				health_animation.GetCurrentFrame().GetSourceX(),
				health_animation.GetCurrentFrame().GetSourceY(),
				health_animation.GetCurrentFrame().GetWidth(),
				health_animation.GetCurrentFrame().GetHeight(),
				(position_x + 55 + (i * 30)) * scale_x,
				(position_y + 40) * scale_y,
				health_animation.GetCurrentFrame().GetWidth() * scale_x,
				health_animation.GetCurrentFrame().GetHeight() * scale_y,
				NULL);
		}

		if (i <= attack_growth)
		{
			stats_bar_image.DrawScaledImage(
				attack_animation.GetCurrentFrame().GetSourceX(),
				attack_animation.GetCurrentFrame().GetSourceY(),
				attack_animation.GetCurrentFrame().GetWidth(),
				attack_animation.GetCurrentFrame().GetHeight(),
				(position_x + 55 + (i * 30)) * scale_x,
				(position_y + 60) * scale_y,
				attack_animation.GetCurrentFrame().GetWidth() * scale_x,
				attack_animation.GetCurrentFrame().GetHeight() * scale_y,
				NULL);
		}

		if (i <= special_attack_growth)
		{
			stats_bar_image.DrawScaledImage(
				special_attack_animation.GetCurrentFrame().GetSourceX(),
				special_attack_animation.GetCurrentFrame().GetSourceY(),
				special_attack_animation.GetCurrentFrame().GetWidth(),
				special_attack_animation.GetCurrentFrame().GetHeight(),
				(position_x + 55 + (i * 30)) * scale_x,
				(position_y + 80) * scale_y,
				special_attack_animation.GetCurrentFrame().GetWidth() * scale_x,
				special_attack_animation.GetCurrentFrame().GetHeight() * scale_y,
				NULL);
		}

		if (i <= defence_growth)
		{
			stats_bar_image.DrawScaledImage(
				defence_animation.GetCurrentFrame().GetSourceX(),
				defence_animation.GetCurrentFrame().GetSourceY(),
				defence_animation.GetCurrentFrame().GetWidth(),
				defence_animation.GetCurrentFrame().GetHeight(),
				(position_x + 55 + (i * 30)) * scale_x,
				(position_y + 100) * scale_y,
				defence_animation.GetCurrentFrame().GetWidth() * scale_x,
				defence_animation.GetCurrentFrame().GetHeight() * scale_y,
				NULL);
		}

		if (i <= special_defence_growth)
		{
			stats_bar_image.DrawScaledImage(
				special_defence_animation.GetCurrentFrame().GetSourceX(),
				special_defence_animation.GetCurrentFrame().GetSourceY(),
				special_defence_animation.GetCurrentFrame().GetWidth(),
				special_defence_animation.GetCurrentFrame().GetHeight(),
				(position_x + 55 + (i * 30)) * scale_x,
				(position_y + 120) * scale_y,
				special_defence_animation.GetCurrentFrame().GetWidth() * scale_x,
				special_defence_animation.GetCurrentFrame().GetHeight() * scale_y,
				NULL);
		}

		if (i <= speed_growth)
		{
			stats_bar_image.DrawScaledImage(
				speed_animation.GetCurrentFrame().GetSourceX(),
				speed_animation.GetCurrentFrame().GetSourceY(),
				speed_animation.GetCurrentFrame().GetWidth(),
				speed_animation.GetCurrentFrame().GetHeight(),
				(position_x + 55 + (i * 30)) * scale_x,
				(position_y + 140) * scale_y,
				speed_animation.GetCurrentFrame().GetWidth() * scale_x,
				speed_animation.GetCurrentFrame().GetHeight() * scale_y,
				NULL);
		}
	}

	character_image.DrawScaledImage(
		character_animation.GetCurrentFrame().GetSourceX(),
		character_animation.GetCurrentFrame().GetSourceY(),
		character_animation.GetCurrentFrame().GetWidth(),
		character_animation.GetCurrentFrame().GetHeight(),
		(position_x + 10) * scale_x,
		(position_y) * scale_y,
		character_animation.GetCurrentFrame().GetWidth() * scale_x,
		character_animation.GetCurrentFrame().GetHeight() * scale_y,
		NULL);

	kirby_name_image.DrawScaledImage(
		0, 0,
		kirby_name_image.GetBitmapWidth(),
		kirby_name_image.GetBitmapHeight(),
		(position_x + 50) * scale_x,
		(position_y + 10) * scale_y,
		kirby_name_image.GetBitmapWidth() * scale_x,
		kirby_name_image.GetBitmapHeight() * scale_y,
		NULL);

	health_text_image.DrawScaledImage(
		0, 0,
		health_text_image.GetBitmapWidth(),
		health_text_image.GetBitmapHeight(),
		(position_x + 10) * scale_x,
		(position_y + 35) * scale_y,
		health_text_image.GetBitmapWidth() * scale_x,
		health_text_image.GetBitmapHeight() * scale_y,
		NULL);

	attack_text_image.DrawScaledImage(
		0, 0,
		attack_text_image.GetBitmapWidth(),
		attack_text_image.GetBitmapHeight(),
		(position_x + 10) * scale_x,
		(position_y + 55) * scale_y,
		attack_text_image.GetBitmapWidth() * scale_x,
		attack_text_image.GetBitmapHeight() * scale_y,
		NULL);

	special_attack_text_image.DrawScaledImage(
		0, 0,
		special_attack_text_image.GetBitmapWidth(),
		special_attack_text_image.GetBitmapHeight(),
		(position_x + 10) * scale_x,
		(position_y + 75) * scale_y,
		special_attack_text_image.GetBitmapWidth() * scale_x,
		special_attack_text_image.GetBitmapHeight() * scale_y,
		NULL);

	defence_text_image.DrawScaledImage(
		0, 0,
		defence_text_image.GetBitmapWidth(),
		defence_text_image.GetBitmapHeight(),
		(position_x + 10) * scale_x,
		(position_y + 95) * scale_y,
		defence_text_image.GetBitmapWidth() * scale_x,
		defence_text_image.GetBitmapHeight() * scale_y,
		NULL);

	special_defence_text_image.DrawScaledImage(
		0, 0,
		special_defence_text_image.GetBitmapWidth(),
		special_defence_text_image.GetBitmapHeight(),
		(position_x + 10) * scale_x,
		(position_y + 115) * scale_y,
		special_defence_text_image.GetBitmapWidth() * scale_x,
		special_defence_text_image.GetBitmapHeight() * scale_y,
		NULL);

	speed_text_image.DrawScaledImage(
		0, 0,
		speed_text_image.GetBitmapWidth(),
		speed_text_image.GetBitmapHeight(),
		(position_x + 10) * scale_x,
		(position_y + 135) * scale_y,
		speed_text_image.GetBitmapWidth() * scale_x,
		speed_text_image.GetBitmapHeight() * scale_y,
		NULL);

}

void Stats::UpdateAnimation()
{
	character_animation.Update();
	//health_animation.Update();
	//attack_animation.Update();
	//special_attack_animation.Update();
	//defence_animation.Update();
	//special_defence_animation.Update();
	//speed_animation.Update();
}

void Stats::LoadStatEnemies(int type)
{
	std::ifstream file;

	switch(type)
	{
	case 1:			// Load Ninja Enemy 
		file.open("Content/Text Files/Unit Stats/Ninja Enemy.txt");
		break;
	case 2:			// Load Sword Knight
		file.open("Content/Text Files/Unit Stats/Sword Knight Enemy.txt");
		break;
	case 3:			// Load Weele Enemy
		file.open("Content/Text Files/Unit Stats/Wheele Enemy.txt");
		break;
	case 4:			// Load Gim Enemy
		file.open("Content/Text Files/Unit Stats/Gim Enemy.txt");
		break;
	case 5:			// Load Boss #1 Mr. Tic-Tock
		file.open("Content/Text Files/Unit Stats/Mr.Tic-Tock Enemy.txt");
		break;
	case 6:			// Load Plama Enemy
		file.open("Content/Text Files/Unit Stats/Plasma Wisp.txt");
		break;
	case 7:			// Load Chilly Enemy
		file.open("Content/Text Files/Unit Stats/Chilly Enemy.txt");
		break;
	case 8:			// Burning Leo Enemy
		file.open("Content/Text Files/Unit Stats/Burning Leo Enemy.txt");
		break;
	case 9:			// Tac Enemy
		file.open("Content/Text Files/Unit Stats/Tac Enemy.txt");
		break;
	case 10:		// Boss #2 Fire-Lion Enemy
		file.open("Content/Text Files/Unit Stats/Fire-Lion Enemy.txt");
		break;
	default:		
		printf("The type of enemy %d does not exist\n", type);
		break;
	}

	if (file.is_open() == false)
	{
		printf("Enemy stat File has not been opened!\n");
	}
	else
	{
		file >> name;												//  Store the name of kirby
		file >> hit_points_growth;
		file >> attack_growth;
		file >> special_attack_growth;
		file >> defence_growth;
		file >> special_defence_growth;
		file >> speed_growth;
	}

	file.close();
}

void Stats::LoadStatKirbies(int type)
{
	std::ifstream file;

	switch(type)
	{
	case 1:															// Animal Kirby
		file.open("Content/Text Files/Unit Stats/Animal Kirby.txt");
		break;
	case 2:															// Fighter Kirby
		file.open("Content/Text Files/Unit Stats/Backdrop Kirby.txt");
		break;
	case 3:															// Cutter Kirby
		file.open("Content/Text Files/Unit Stats/Cutter Kirby.txt"); 
		break;
	case 4:															// Fire Kirby
		file.open("Content/Text Files/Unit Stats/Fire Kirby.txt");
		break;
	case 5:															// Kirby
		file.open("Content/Text Files/Unit Stats/Kirby.txt");
		break;
	case 6:															// Ninja Kirby
		file.open("Content/Text Files/Unit Stats/Ninja Kirby.txt");
		break;
	case 7:															// Jet Kriby
		file.open("Content/Text Files/Unit Stats/Jet Kirby.txt");
		break;
	case 8:															// Ice Kirby
		file.open("Content/Text Files/Unit Stats/Ice Kirby.txt");
		break;
	case 9:															// Stone Kirby
		file.open("Content/Text Files/Unit Stats/Stone Kirby.txt");
		break;
	case 10:														// Plamsa Kirby
		file.open("Content/Text Files/Unit Stats/Plasma Kirby.txt");
		break;
	default:														
		printf("The type of kirby %d does not exist\n", type);
		break;
	}

	if (file.is_open() == false)
	{
		printf("Kirby stat File has not been opened!\n");
	}
	else
	{
		file >> name;												//  Store the name of kirby
		file >> hit_points_growth;	
		file >> attack_growth;
		file >> special_attack_growth;
		file >> defence_growth;
		file >> special_defence_growth;
		file >> speed_growth;
	}

	file.close();
}

int Stats::GetAttackGrowth()
{
	if (attack_growth == 0)
	{
		return 0;
	}
	else
	{
		//srand(time(NULL));
		return (rand() % (attack_growth + 1));
	}
}

int Stats::GetDefenceGrowth()
{
	if (defence_growth == 0)
	{
		return 0;
	}
	else
	{
		//srand(time(NULL));
		return (rand() % (defence_growth + 1));
	}
	
}

int Stats::GetHitPointsGrowth()
{
	if (hit_points_growth == 0)
	{
		return 0;
	}
	else
	{
		//srand(time(NULL));
		return (rand() % (hit_points_growth + 1));
	}
	
}

int Stats::GetSpecialAttackGrowth()
{
	if (special_attack_growth == 0)
	{
		//srand(time(NULL));
		return 0;
	}
	else
	{
		srand(time(NULL));
		return (rand() % ( special_attack_growth + 1));
	}
}

int Stats::GetSpecialDefenceGrowth()
{
	if (special_defence_growth == 0)
	{
		return 0;
	}
	else
	{
		//srand(time(NULL));
		return (rand() % (special_defence_growth + 1));
	}
}

int Stats::GetSpeedGrowth()
{
	if (speed_growth == 0)
	{
		return 0;
	}
	else
	{
		//srand(time(NULL));
		return (rand() % ( speed_growth + 1 ));
	}
}