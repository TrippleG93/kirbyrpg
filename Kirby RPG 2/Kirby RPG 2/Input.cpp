#include "input.h"

/*
* Does not have mouse support yet
* Have a function call that resets a button pressed so that holding the button no longer counts
* Do this for keyboard support as well
*/

Input::Input()
{

}


Input::~Input()
{
	// May break the code
	SetUpControllers();
}

// NEEDS TO BE TESTED
void Input::SetUpControllers()
{
	if (al_get_num_joysticks() > 0)
	{
		using_controllers = true;
		how_many_controllers = al_get_num_joysticks();

		if (al_get_num_joysticks() > 0)
		{
			using_controllers = true;
			how_many_controllers = al_get_num_joysticks();

			for (int i = 0; i < how_many_controllers; i++)
			{
				joysticks[i] = al_get_joystick(i);
			}
		}

		x_Input *traverse = nullptr;					// Will be used to make additional controllers if need be

		for (int i = 0; i < how_many_controllers; i++)
		{
			if (i == 0)									// If a controller has not been created yet
			{
				head = (x_Input*)malloc(sizeof(x_Input));

				head->left_stick_x = 0.0;
				head->left_stick_y = 0.0;
				head->left_trigger = 0.0;
				head->right_stick_x = 0.0;
				head->right_stick_y = 0.0;
				head->right_trigger = 0.0;
				head->dpad_x = 0.0;
				head->dpad_y = 0.0;

				// Buttons Pressed
				head->button_a = false;
				head->button_b = false;
				head->button_x = false;
				head->button_y = false;
				head->button_left_stick = false;
				head->button_left_stick_left = false;
				head->button_left_stick_right = false;
				head->button_left_stick_up = false;
				head->button_left_stick_down = false;
				head->button_right_stick = false;
				head->button_right_stick_left = false;
				head->button_right_stick_right = false;
				head->button_right_stick_up = false;
				head->button_right_stick_down = false;
				head->button_left_bumper = false;
				head->button_left_trigger = false;
				head->button_right_bumber = false;
				head->button_right_trigger = false;
				head->button_start = false;
				head->button_select = false;
				head->dpad_left = false;
				head->dpad_right = false;
				head->dpad_up = false;
				head->dpad_down = false;

				// Buttons Held
				head->held_button_a = false;
				head->held_button_b = false;
				head->held_button_x = false;
				head->held_button_y = false;
				head->held_button_left_stick = false;
				head->held_button_left_stick_left = false;
				head->held_button_left_stick_right = false;
				head->held_button_left_stick_up = false;
				head->held_button_left_stick_down = false;
				head->held_button_right_stick = false;
				head->held_button_right_stick_left = false;
				head->held_button_right_stick_right = false;
				head->held_button_right_stick_up = false;
				head->held_button_right_stick_down = false;
				head->held_button_left_bumper = false;
				head->held_button_left_trigger = false;
				head->held_button_right_bumber = false;
				head->held_button_right_trigger = false;
				head->held_button_start = false;
				head->held_button_select = false;
				head->held_dpad_left = false;
				head->held_dpad_right = false;
				head->held_dpad_up = false;
				head->held_dpad_down = false;

				head->next = nullptr;						// There is no other controller hooked up

				traverse = head;
			}
			else
			{
				x_Input *node = (x_Input*)malloc(sizeof(x_Input));

				node->left_stick_x = 0.0;
				node->left_stick_y = 0.0;
				node->left_trigger = 0.0;
				node->right_stick_x = 0.0;
				node->right_stick_y = 0.0;
				node->right_trigger = 0.0;
				node->dpad_x = 0.0;
				node->dpad_y = 0.0;

				// Buttons Pressed
				node->button_a = false;
				node->button_b = false;
				node->button_x = false;
				node->button_y = false;
				node->button_left_stick_left = false;
				node->button_left_stick_right = false;
				node->button_left_stick_up = false;
				node->button_left_stick_down = false;
				node->button_right_stick_left = false;
				node->button_right_stick_right = false;
				node->button_right_stick_up = false;
				node->button_right_stick_down = false;
				node->button_left_bumper = false;
				node->button_left_trigger = false;
				node->button_right_bumber = false;
				node->button_right_trigger = false;
				node->button_start = false;
				node->button_select = false;
				node->dpad_left = false;
				node->dpad_right = false;
				node->dpad_up = false;
				node->dpad_down = false;

				// Buttons Held
				node->held_button_a = false;
				node->held_button_b = false;
				node->held_button_x = false;
				node->held_button_y = false;
				node->held_button_left_stick_left = false;
				node->held_button_left_stick_right = false;
				node->held_button_left_stick_up = false;
				node->held_button_left_stick_down = false;
				node->held_button_right_stick_left = false;
				node->held_button_right_stick_right = false;
				node->held_button_right_stick_up = false;
				node->held_button_right_stick_down = false;
				node->held_button_left_bumper = false;
				node->held_button_left_trigger = false;
				node->held_button_right_bumber = false;
				node->held_button_right_trigger = false;
				node->held_button_start = false;
				node->held_button_select = false;
				node->held_dpad_left = false;
				node->held_dpad_right = false;
				node->held_dpad_up = false;
				node->held_dpad_down = false;

				node->next = nullptr;

				traverse->next = node;
				traverse = traverse->next;					// The next contrller has been sucessfully added

			}
		}
	}
	else
	{
		for (int i = 0; i < how_many_controllers; i++)
		{

			x_Input *traverse = head;					// Will be used to make additional controllers if need be
			head = head->next;
			traverse = nullptr;
			free(traverse);
		}
	}
}

void Input::ResyncControllers()
{
	al_reconfigure_joysticks();

	SetUpControllers();

}

ALLEGRO_JOYSTICK* Input::GetJoystick(int controller)
{
	return joysticks[controller];
}

// DONT CARE RIGHT NOW
void Input::UpdateKeyboardInput(const ALLEGRO_EVENT &events)
{
	if (events.type == ALLEGRO_EVENT_KEY_DOWN)			// If a key was pressed on the keyboard
	{
		KeyDownEvent(events);
		//printf("Keycode %d\n", events.keyboard.keycode);
	}
	else if (events.type == ALLEGRO_EVENT_KEY_UP)
	{
		KeyUpEvent(events);
	}
}

// EMPTY FUNCTION
void Input::KeyDownEvent(const ALLEGRO_EVENT& events)
{
	switch (events.keyboard.keycode)
	{
	case KEYCODE_A:
		Keyboard_Controls.a_key = true;
		break;
	case KEYCODE_B:
		Keyboard_Controls.b_key = true;
		break;
	case KEYCODE_C:
		Keyboard_Controls.c_key = true;
		break;
	case KEYCODE_D:
		Keyboard_Controls.d_key = true;
		break;
	case KEYCODE_E:
		Keyboard_Controls.e_key = true;
		break;
	case KEYCODE_F:
		Keyboard_Controls.f_key = true;
		break;
	case KEYCODE_G:
		Keyboard_Controls.g_key = true;
		break;
	case KEYCODE_H:
		Keyboard_Controls.h_key = true;
		break;
	case KEYCODE_I:
		Keyboard_Controls.i_key = true;
		break;
	case KEYCODE_J:
		Keyboard_Controls.j_key = true;
		break;
	case KEYCODE_K:
		Keyboard_Controls.k_key = true;
		break;
	case KEYCODE_L:
		Keyboard_Controls.l_key = true;
		break;
	case KEYCODE_M:
		Keyboard_Controls.m_key = true;
		break;
	case KEYCODE_N:
		Keyboard_Controls.n_key = true;
		break;
	case KEYCODE_O:
		Keyboard_Controls.o_key = true;
		break;
	case KEYCODE_P:
		Keyboard_Controls.p_key = true;
		break;
	case KEYCODE_Q:
		Keyboard_Controls.q_key = true;
		break;
	case KEYCODE_R:
		Keyboard_Controls.r_key = true;
		break;
	case KEYCODE_S:
		Keyboard_Controls.s_key = true;
		break;
	case KEYCODE_T:
		Keyboard_Controls.t_key = true;
		break;
	case KEYCODE_U:
		Keyboard_Controls.u_key = true;
		break;
	case KEYCODE_V:
		Keyboard_Controls.v_key = true;
		break;
	case KEYCODE_W:
		Keyboard_Controls.w_key = true;
		break;
	case KEYCODE_X:
		Keyboard_Controls.x_key = true;
		break;
	case KEYCODE_Y:
		Keyboard_Controls.y_key = true;
		break;
	case KEYCODE_Z:
		Keyboard_Controls.z_key = true;
		break;
	case KEYCODE_0:
		Keyboard_Controls.zero_key = true;
		break;
	case KEYCODE_1:
		Keyboard_Controls.one_key = true;
		break;
	case KEYCODE_2:
		Keyboard_Controls.two_key = true;
		break;
	case KEYCODE_3:
		Keyboard_Controls.three_key = true;
		break;
	case KEYCODE_4:
		Keyboard_Controls.four_key = true;
		break;
	case KEYCODE_5:
		Keyboard_Controls.five_key = true;
		break;
	case KEYCODE_6:
		Keyboard_Controls.six_key = true;
		break;
	case KEYCODE_7:
		Keyboard_Controls.seven_key = true;
		break;
	case KEYCODE_8:
		Keyboard_Controls.eight_key = true;
		break;
	case KEYCODE_9:
		Keyboard_Controls.nine_key = true;
		break;
	case KEYCODE_0_PAD:
		Keyboard_Controls.zero_pad_key = true;
		break;
	case KEYCODE_1_PAD:
		Keyboard_Controls.one_pad_key = true;
		break;
	case KEYCODE_2_PAD:
		Keyboard_Controls.two_pad_key = true;
		break;
	case KEYCODE_3_PAD:
		Keyboard_Controls.three_pad_key = true;
		break;
	case KEYCODE_4_PAD:
		Keyboard_Controls.four_pad_key = true;
		break;
	case KEYCODE_5_PAD:
		Keyboard_Controls.five_pad_key = true;
		break;
	case KEYCODE_6_PAD:
		Keyboard_Controls.six_pad_key = true;
		break;
	case KEYCODE_7_PAD:
		Keyboard_Controls.seven_pad_key = true;
		break;
	case KEYCODE_8_PAD:
		Keyboard_Controls.eight_pad_key = true;
		break;
	case KEYCODE_9_PAD:
		Keyboard_Controls.nine_pad_key = true;
		break;
	case KEYCODE_ESCAPE:
		Keyboard_Controls.escape_key = true;
		break;
	case KEYCODE_TILDE:
		Keyboard_Controls.tilde_key = true;
		break;
	case KEYCODE_DASH:
		Keyboard_Controls.dash_key = true;
		break;
	case KEYCODE_EQUAL:
		Keyboard_Controls.equal_key = true;
		break;
	case KEYCODE_BACKSPACE:
		Keyboard_Controls.backspace_key = true;
		break;
	case KEYCODE_TAB:
		Keyboard_Controls.tab_key = true;
		break;
	case KEYCODE_LEFTBRACKET:
		Keyboard_Controls.left_bracket_key = true;
		break;
	case KEYCODE_RIGHTBRACKET:
		Keyboard_Controls.right_bracket_key = true;
		break;
	case KEYCODE_ENTER:
		Keyboard_Controls.enter_key = true;
		break;
	case KEYCODE_SEMICOLON:
		Keyboard_Controls.semicolon_key = true;
		break;
	case KEYCODE_SINGLEQUOTE:
		Keyboard_Controls.single_quote_key = true;
		break;
	case KEYCODE_SLASH:
		Keyboard_Controls.slash_key = true;
		break;
	case KEYCODE_COMMA:
		Keyboard_Controls.comma_key = true;
		break;
	case KEYCODE_PERIOD:
		Keyboard_Controls.period_key = true;
		break;
	case KEYCODE_QUESTIONMARK:
		Keyboard_Controls.questionmark_key = true;
		break;
	case KEYCODE_SPACE:
		Keyboard_Controls.space_key = true;
		break;
	case KEYCODE_LEFTARROW:
		Keyboard_Controls.left_arrow_key = true;
		break;
	case KEYCODE_RIGHTARROW:
		Keyboard_Controls.right_arrow_key = true;
		break;
	case KEYCODE_UPARROW:
		Keyboard_Controls.up_arrow_key = true;
		break;
	case KEYCODE_DOWNARROW:
		Keyboard_Controls.down_arrow_key = true;
		break;
	case KEYCDOE_LEFTSHIFT:
		Keyboard_Controls.left_shift_key = true;
		break;
	case KEYCODE_RIGHTSHIFT:
		Keyboard_Controls.right_shift_key = true;
		break;
	}
}

void Input::KeyUpEvent(const ALLEGRO_EVENT& events)
{
	switch (events.keyboard.keycode)
	{
	case KEYCODE_A:
		Keyboard_Controls.a_key = false;
		Keyboard_Controls.a_key_held = false;
		break;
	case KEYCODE_B:
		Keyboard_Controls.b_key = false;
		Keyboard_Controls.b_key_held = false;
		break;
	case KEYCODE_C:
		Keyboard_Controls.c_key = false;
		Keyboard_Controls.c_key_held = false;
		break;
	case KEYCODE_D:
		Keyboard_Controls.d_key = false;
		Keyboard_Controls.d_key_held = false;
		break;
	case KEYCODE_E:
		Keyboard_Controls.e_key = false;
		Keyboard_Controls.e_key_held = false;
		break;
	case KEYCODE_F:
		Keyboard_Controls.f_key = false;
		Keyboard_Controls.f_key_held = false;
		break;
	case KEYCODE_G:
		Keyboard_Controls.g_key = false;
		Keyboard_Controls.g_key_held = false;
		break;
	case KEYCODE_H:
		Keyboard_Controls.h_key = false;
		Keyboard_Controls.h_key_held = false;
		break;
	case KEYCODE_I:
		Keyboard_Controls.i_key = false;
		Keyboard_Controls.i_key_held = false;
		break;
	case KEYCODE_J:
		Keyboard_Controls.j_key = false;
		Keyboard_Controls.j_key_held = false;
		break;
	case KEYCODE_K:
		Keyboard_Controls.k_key = false;
		Keyboard_Controls.k_key_held = false;
		break;
	case KEYCODE_L:
		Keyboard_Controls.l_key = false;
		Keyboard_Controls.l_key_held = false;
		break;
	case KEYCODE_M:
		Keyboard_Controls.m_key = false;
		Keyboard_Controls.m_key_held = false;
		break;
	case KEYCODE_N:
		Keyboard_Controls.n_key = false;
		Keyboard_Controls.n_key_held = false;
		break;
	case KEYCODE_O:
		Keyboard_Controls.o_key = false;
		Keyboard_Controls.o_key_held = false;
		break;
	case KEYCODE_P:
		Keyboard_Controls.p_key = false;
		Keyboard_Controls.p_key_held = false;
		break;
	case KEYCODE_Q:
		Keyboard_Controls.q_key = false;
		Keyboard_Controls.q_key_held = false;
		break;
	case KEYCODE_R:
		Keyboard_Controls.r_key = false;
		Keyboard_Controls.r_key_held = false;
		break;
	case KEYCODE_S:
		Keyboard_Controls.s_key = false;
		Keyboard_Controls.s_key_held = false;
		break;
	case KEYCODE_T:
		Keyboard_Controls.t_key = false;
		Keyboard_Controls.t_key_held = false;
		break;
	case KEYCODE_U:
		Keyboard_Controls.u_key = false;
		Keyboard_Controls.u_key_held = false;
		break;
	case KEYCODE_V:
		Keyboard_Controls.v_key = false;
		Keyboard_Controls.v_key_held = false;
		break;
	case KEYCODE_W:
		Keyboard_Controls.w_key = false;
		Keyboard_Controls.w_key_held = false;
		break;
	case KEYCODE_X:
		Keyboard_Controls.x_key = false;
		Keyboard_Controls.x_key_held = false;
		break;
	case KEYCODE_Y:
		Keyboard_Controls.y_key = false;
		Keyboard_Controls.y_key_held = false;
		break;
	case KEYCODE_Z:
		Keyboard_Controls.z_key = false;
		Keyboard_Controls.z_key_held = false;
		break;
	case KEYCODE_0:
		Keyboard_Controls.zero_key = false;
		Keyboard_Controls.zero_key_held = false;
		break;
	case KEYCODE_1:
		Keyboard_Controls.one_key = false;
		Keyboard_Controls.one_key_held = false;
		break;
	case KEYCODE_2:
		Keyboard_Controls.two_key = false;
		Keyboard_Controls.two_key_held = false;
		break;
	case KEYCODE_3:
		Keyboard_Controls.three_key = false;
		Keyboard_Controls.three_key_held = false;
		break;
	case KEYCODE_4:
		Keyboard_Controls.four_key = false;
		Keyboard_Controls.four_key_held = false;
		break;
	case KEYCODE_5:
		Keyboard_Controls.five_key = false;
		Keyboard_Controls.five_key_held = false;
		break;
	case KEYCODE_6:
		Keyboard_Controls.six_key = false;
		Keyboard_Controls.six_key_held = false;
		break;
	case KEYCODE_7:
		Keyboard_Controls.seven_key = false;
		Keyboard_Controls.seven_key_held = false;
		break;
	case KEYCODE_8:
		Keyboard_Controls.eight_key = false;
		Keyboard_Controls.eight_key_held = false;
		break;
	case KEYCODE_9:
		Keyboard_Controls.nine_key = false;
		Keyboard_Controls.nine_key_held = false;
		break;
	case KEYCODE_0_PAD:
		Keyboard_Controls.zero_pad_key = false;
		Keyboard_Controls.zero_pad_key_held = false;
		break;
	case KEYCODE_1_PAD:
		Keyboard_Controls.one_pad_key = false;
		Keyboard_Controls.one_pad_key_held = false;
		break;
	case KEYCODE_2_PAD:
		Keyboard_Controls.two_pad_key = false;
		Keyboard_Controls.two_pad_key_held = false;
		break;
	case KEYCODE_3_PAD:
		Keyboard_Controls.three_pad_key = false;
		Keyboard_Controls.three_pad_key_held = false;
		break;
	case KEYCODE_4_PAD:
		Keyboard_Controls.four_pad_key = false;
		Keyboard_Controls.four_pad_key_held = false;
		break;
	case KEYCODE_5_PAD:
		Keyboard_Controls.five_pad_key = false;
		Keyboard_Controls.five_pad_key_held = false;
		break;
	case KEYCODE_6_PAD:
		Keyboard_Controls.six_pad_key = false;
		Keyboard_Controls.six_pad_key_held = false;
		break;
	case KEYCODE_7_PAD:
		Keyboard_Controls.seven_pad_key = false;
		Keyboard_Controls.seven_pad_key_held = false;
		break;
	case KEYCODE_8_PAD:
		Keyboard_Controls.eight_pad_key = false;
		Keyboard_Controls.eight_pad_key_held = false;
		break;
	case KEYCODE_9_PAD:
		Keyboard_Controls.nine_pad_key = false;
		Keyboard_Controls.nine_pad_key_held = false;
		break;
	case KEYCODE_ESCAPE:
		Keyboard_Controls.escape_key = false;
		Keyboard_Controls.escape_key_held = false;
		break;
	case KEYCODE_TILDE:
		Keyboard_Controls.tilde_key = false;
		Keyboard_Controls.tilde_key_held = false;
		break;
	case KEYCODE_DASH:
		Keyboard_Controls.dash_key = false;
		Keyboard_Controls.dash_key_held = false;
		break;
	case KEYCODE_EQUAL:
		Keyboard_Controls.equal_key = false;
		Keyboard_Controls.equal_key_held = false;
		break;
	case KEYCODE_BACKSPACE:
		Keyboard_Controls.backspace_key = false;
		Keyboard_Controls.backspace_key_held = false;
		break;
	case KEYCODE_TAB:
		Keyboard_Controls.tab_key = false;
		Keyboard_Controls.tab_key_held = false;
		break;
	case KEYCODE_LEFTBRACKET:
		Keyboard_Controls.left_bracket_key = false;
		Keyboard_Controls.left_bracket_key_held = false;
		break;
	case KEYCODE_RIGHTBRACKET:
		Keyboard_Controls.right_bracket_key = false;
		Keyboard_Controls.right_bracket_key_held = false;
		break;
	case KEYCODE_ENTER:
		Keyboard_Controls.enter_key = false;
		Keyboard_Controls.enter_key_held = false;
		break;
	case KEYCODE_SEMICOLON:
		Keyboard_Controls.semicolon_key = false;
		Keyboard_Controls.semicolon_key_held = false;
		break;
	case KEYCODE_SINGLEQUOTE:
		Keyboard_Controls.single_quote_key = false;
		Keyboard_Controls.single_quote_key_held = false;
		break;
	case KEYCODE_SLASH:
		Keyboard_Controls.slash_key = false;
		Keyboard_Controls.slash_key_held = false;
		break;
	case KEYCODE_COMMA:
		Keyboard_Controls.comma_key = false;
		Keyboard_Controls.comma_key_held = false;
		break;
	case KEYCODE_PERIOD:
		Keyboard_Controls.period_key = false;
		Keyboard_Controls.period_key_held = false;
		break;
	case KEYCODE_QUESTIONMARK:
		Keyboard_Controls.questionmark_key = false;
		Keyboard_Controls.questionmark_key_held = false;
		break;
	case KEYCODE_SPACE:
		Keyboard_Controls.space_key = false;
		Keyboard_Controls.space_key_held = false;
		break;
	case KEYCODE_LEFTARROW:
		Keyboard_Controls.left_arrow_key = false;
		Keyboard_Controls.left_arrow_key_held = false;
		break;
	case KEYCODE_RIGHTARROW:
		Keyboard_Controls.right_arrow_key = false;
		Keyboard_Controls.right_arrow_key_held = false;
		break;
	case KEYCODE_UPARROW:
		Keyboard_Controls.up_arrow_key = false;
		Keyboard_Controls.up_arrow_key_held = false;
		break;
	case KEYCODE_DOWNARROW:
		Keyboard_Controls.down_arrow_key = false;
		Keyboard_Controls.down_arrow_key_held = false;
		break;
	case KEYCDOE_LEFTSHIFT:
		Keyboard_Controls.left_shift_key = false;
		Keyboard_Controls.left_shift_key_held = false;
		break;
	case KEYCODE_RIGHTSHIFT:
		Keyboard_Controls.right_shift_key = false;
		Keyboard_Controls.right_shift_key_held = false;
		break;
	default:
		break;
	}
}

void Input::ButtonDownEvent(const ALLEGRO_EVENT &events)
{
	int current_controller = MatchController(events);			// Obtain the the numerical value of which controller made the input

	x_Input *traverse = head;									// Start at the beginning of the link list for active controllers

	if (current_controller == -1)
	{
		printf("INPUT ERROR WITH CONTROLLERS AND EVERYTHING IS BROKEN\n");
	}
	else
	{
		for (int i = 0; i < current_controller; i++)			// Find the correct controller that made that input
		{
			traverse = traverse->next;
		}
	}

	switch (events.joystick.button)								// Find what button was pressed
	{
	case AINDEX_A_BUTTON:														// A button
		traverse->button_a = true;
		break;
	case AINDEX_B_BUTTON:														// B button
		traverse->button_b = true;
		break;
	case AINDEX_X_BUTTON:
		traverse->button_x = true;
		break;
	case AINDEX_Y_BUTTON:
		traverse->button_y = true;
		break;
	case AINDEX_RIGHTBUMPER_BUTTON:
		traverse->button_right_bumber = true;
		break;
	case AINDEX_LEFTBUMPER_BUTTON:
		traverse->button_left_bumper = true;
		break;
	case AINDEX_RIGHTTHUMBSTICK_BUTTONS:
		traverse->button_right_stick = true;
		break;
	case AINDEX_LEFTTHUMBSTICK_BUTTONS:
		traverse->button_left_stick = true;
		break;
	case AINDEX_SELECT_BUTTON:
		traverse->button_select = true;
		break;
	case AINDEX_START_BUTTON:
		traverse->button_start = true;
		break;
	case AINDEX_RIGHT_DPAD:
		traverse->dpad_right = true;
		break;
	case AINDEX_LEFT_DPAD:
		traverse->dpad_left = true;
		break;
	case AINDEX_DOWN_DPAD:
		traverse->dpad_down = true;
		break;
	case AINDEX_UP_DPAD:
		traverse->dpad_up = true;
		break;
	default:
		printf("ERROR WITH BUTTON INPUT PASS\n");
		break;

	}
}

void Input::ButtonUpEvent(const ALLEGRO_EVENT &events)
{
	int current_controller = MatchController(events);			// Obtain the the numerical value of which controller made the input

	x_Input *traverse = head;									// Start at the beginning of the link list for active controllers

	if (current_controller == -1)
	{
		printf("INPUT ERROR WITH CONTROLLERS AND EVERYTHING IS BROKEN\n");
	}
	else
	{
		for (int i = 0; i < current_controller; i++)			// Find the correct controller that made that input
		{
			traverse = traverse->next;
		}
	}

	switch (events.joystick.button)								// Find what button was pressed
	{
	case 0:														// A button
		traverse->button_a = false;
		traverse->held_button_a = false;
		break;
	case 1:														// B button
		traverse->button_b = false;
		traverse->held_button_b = false;
		break;
	case 2:
		traverse->button_x = false;
		traverse->held_button_x = false;
		break;
	case 3:
		traverse->button_y = false;
		traverse->held_button_y = false;
		break;
	case 4:
		traverse->button_right_bumber = false;
		traverse->held_button_right_bumber = false;
		break;
	case 5:
		traverse->button_left_bumper = false;
		traverse->held_button_left_bumper = false;
		break;
	case 6:
		traverse->button_right_stick = false;
		traverse->held_button_right_stick = false;
		break;
	case 7:
		traverse->button_left_stick = false;
		traverse->held_button_left_stick = false;
		break;
	case 8:
		traverse->button_select = false;
		traverse->held_button_select = false;
		break;
	case 9:
		traverse->button_start = false;
		traverse->held_button_start = false;
		break;
	case 10:
		traverse->dpad_right = false;
		traverse->held_dpad_right = false;
		break;
	case 11:
		traverse->dpad_left = false;
		traverse->held_dpad_left = false;
		break;
	case 12:
		traverse->dpad_down = false;
		traverse->held_dpad_down = false;
		break;
	case 13:
		traverse->dpad_up = false;
		traverse->held_dpad_up = false;
		break;
	default:
		printf("ERROR WITH BUTTON INPUT PASS\n");
		break;
	}
}

void Input::ButtonHeldEvent()
{
	x_Input *traverse = head;									// Start at the beginning of the link list for active controllers

	for (int i = 0; i < how_many_controllers; i++)				// Goes through all the controllers and updates if a button is being held
	{
		for (int buttons = 0; buttons <= AINDEX_UP_DPAD; buttons++)	// Go through the first 13 buttons
		{
			switch (buttons)								// Find what button was pressed
			{
			case 0:												// A button
				if (traverse->button_a == true)
				{
					traverse->held_button_a = true;
				}
				break;
			case 1:
				if (traverse->button_b == true)
				{
					traverse->held_button_b = true;
				}
				break;
			case 2:
				if (traverse->button_x == true)
				{
					traverse->held_button_x = true;
				}
				break;
			case 3:
				if (traverse->button_y == true)
				{
					traverse->held_button_y = true;
				}
				break;
			case 4:
				if (traverse->button_right_bumber == true)
				{
					traverse->held_button_right_bumber = true;
				}
				break;
			case 5:
				if (traverse->button_left_bumper == true)
				{
					traverse->held_button_left_bumper = true;
				}
				break;
			case 6:
				if (traverse->button_right_stick == true)
				{
					traverse->held_button_right_stick = true;
				}
				break;
			case 7:
				if (traverse->button_left_stick == true)
				{
					traverse->held_button_left_stick = true;
				}
				break;
			case 8:
				if (traverse->button_select == true)
				{
					traverse->held_button_select = true;
				}
				break;
			case 9:
				if (traverse->button_start == true)
				{
					traverse->held_button_start = true;
				}
				break;
			case 10:
				if (traverse->dpad_right == true)
				{
					traverse->held_dpad_right = true;
				}
				break;
			case 11:
				if (traverse->dpad_left == true)
				{
					traverse->held_dpad_left = true;
				}
				break;
			case 12:
				if (traverse->dpad_down == true)
				{
					traverse->held_dpad_down = true;
				}
				break;
			case 13:
				if (traverse->dpad_up == true)
				{
					traverse->held_dpad_up = true;
				}
				break;
			default:
				printf("ERROR WITH BUTTON INPUT PASS\n");
				break;
			} // end switch statement
		} // end for loop
		for (int i = 20; i <= AINDEX_RIGHT_TRIGGER; i++)
		{
			switch (i)
			{
			case 20:
				if (traverse->button_left_stick_left == true)
				{
					traverse->held_button_left_stick_left = true;
				}break;
			case 21:
				if (traverse->button_left_stick_right == true)
				{
					traverse->held_button_left_stick_right = true;
				}break;
			case 22:
				if (traverse->button_left_stick_up == true)
				{
					traverse->held_button_left_stick_up = true;
				}
				break;
			case 23:
				if (traverse->button_left_stick_down == true)
				{
					traverse->held_button_left_stick_down = true;
				}
				break;
			case 24:
				if (traverse->button_right_stick_left == true)
				{
					traverse->held_button_right_stick_left = true;
				}
				break;
			case 25:
				if (traverse->button_right_stick_right == true)
				{
					traverse->held_button_right_stick_right = true;
				}
				break;
			case 26:
				if (traverse->button_right_stick_up == true)
				{
					traverse->held_button_right_stick_up = true;
				}
				break;
			case 27:
				if (traverse->button_right_stick_down == true)
				{
					traverse->held_button_right_stick_down;
				}
				break;
			case 28:
				if (traverse->button_right_trigger == true)
				{
					traverse->held_button_right_trigger = true;
				}
				break;
			case 29:
				if (traverse->button_left_trigger == true)
				{
					traverse->held_button_left_trigger = true;
				}
				break;
			default:
				break;
			}
		}
	}// end if statement

}

void Input::KeyHeldEvent()
{

	if (Keyboard_Controls.a_key == true)
	{
		Keyboard_Controls.a_key_held = true;
	}
	if (Keyboard_Controls.b_key == true)
	{
		Keyboard_Controls.b_key_held = true;
	}
	if (Keyboard_Controls.c_key == true)
	{
		Keyboard_Controls.c_key_held = true;
	}
	if (Keyboard_Controls.d_key == true)
	{
		Keyboard_Controls.d_key_held = true;
	}
	if (Keyboard_Controls.e_key == true)
	{
		Keyboard_Controls.e_key_held = true;
	}
	if (Keyboard_Controls.f_key == true)
	{
		Keyboard_Controls.f_key_held = true;
	}
	if (Keyboard_Controls.g_key == true)
	{
		Keyboard_Controls.g_key_held = true;
	}
	if (Keyboard_Controls.h_key == true)
	{
		Keyboard_Controls.h_key_held = true;
	}
	if (Keyboard_Controls.i_key == true)
	{
		Keyboard_Controls.i_key_held = true;
	}
	if (Keyboard_Controls.j_key == true)
	{
		Keyboard_Controls.j_key_held = true;
	}
	if (Keyboard_Controls.k_key == true)
	{
		Keyboard_Controls.k_key_held = true;
	}
	if (Keyboard_Controls.l_key == true)
	{
		Keyboard_Controls.l_key_held = true;
	}
	if (Keyboard_Controls.m_key == true)
	{
		Keyboard_Controls.m_key_held = true;
	}
	if (Keyboard_Controls.n_key == true)
	{
		Keyboard_Controls.n_key_held = true;
	}
	if (Keyboard_Controls.o_key == true)
	{
		Keyboard_Controls.o_key_held = true;
	}
	if (Keyboard_Controls.p_key == true)
	{
		Keyboard_Controls.p_key_held = true;
	}
	if (Keyboard_Controls.q_key == true)
	{
		Keyboard_Controls.q_key_held = true;
	}
	if (Keyboard_Controls.r_key = true)
	{
		Keyboard_Controls.r_key_held = true;
	}
	if (Keyboard_Controls.s_key == true)
	{
		Keyboard_Controls.s_key_held = true;
	}
	if (Keyboard_Controls.t_key == true)
	{
		Keyboard_Controls.t_key_held = true;
	}
	if (Keyboard_Controls.u_key == true)
	{
		Keyboard_Controls.u_key_held = true;
	}
	if (Keyboard_Controls.v_key == true)
	{
		Keyboard_Controls.v_key_held = true;
	}
	if (Keyboard_Controls.w_key == true)
	{
		Keyboard_Controls.w_key_held = true;
	}
	if (Keyboard_Controls.x_key == true)
	{
		Keyboard_Controls.x_key_held = true;
	}
	if (Keyboard_Controls.y_key == true)
	{
		Keyboard_Controls.y_key_held = true;
	}
	if (Keyboard_Controls.z_key = true)
	{
		Keyboard_Controls.z_key_held = true;
	}
	if (Keyboard_Controls.zero_key = true)
	{
		Keyboard_Controls.zero_key_held = true;
	}
	if (Keyboard_Controls.one_key == true)
	{
		Keyboard_Controls.one_key_held = true;
	}
	if (Keyboard_Controls.two_key == true)
	{
		Keyboard_Controls.two_key_held = true;
	}
	if (Keyboard_Controls.three_key == true)
	{
		Keyboard_Controls.three_key_held = true;
	}
	if (Keyboard_Controls.four_key == true)
	{
		Keyboard_Controls.four_key_held = true;
	}
	if (Keyboard_Controls.five_key == true)
	{
		Keyboard_Controls.five_key_held = true;
	}
	if (Keyboard_Controls.six_key == true)
	{
		Keyboard_Controls.six_key_held = true;
	}
	if (Keyboard_Controls.seven_key == true)
	{
		Keyboard_Controls.seven_key_held = true;
	}
	if (Keyboard_Controls.eight_key == true)
	{
		Keyboard_Controls.eight_key_held = true;
	}
	if (Keyboard_Controls.nine_key == true)
	{
		Keyboard_Controls.nine_key_held = true;
	}
	if (Keyboard_Controls.zero_pad_key == true)
	{
		Keyboard_Controls.zero_pad_key_held = true;
	}
	if (Keyboard_Controls.one_pad_key == true)
	{
		Keyboard_Controls.one_pad_key_held = true;
	}
	if (Keyboard_Controls.two_pad_key == true)
	{
		Keyboard_Controls.two_pad_key_held = true;
	}
	if (Keyboard_Controls.three_pad_key == true)
	{
		Keyboard_Controls.three_pad_key_held = true;
	}
	if (Keyboard_Controls.four_pad_key == true)
	{
		Keyboard_Controls.four_pad_key_held = true;
	}
	if (Keyboard_Controls.five_pad_key == true)
	{
		Keyboard_Controls.five_pad_key_held = true;
	}
	if (Keyboard_Controls.six_pad_key == true)
	{
		Keyboard_Controls.six_pad_key_held = true;
	}
	if (Keyboard_Controls.seven_pad_key == true)
	{
		Keyboard_Controls.seven_pad_key_held = true;
	}
	if (Keyboard_Controls.eight_pad_key == true)
	{
		Keyboard_Controls.eight_pad_key_held = true;
	}
	if (Keyboard_Controls.nine_pad_key == true)
	{
		Keyboard_Controls.nine_pad_key_held = true;
	}
	if (Keyboard_Controls.escape_key == true)
	{
		Keyboard_Controls.escape_key_held = true;
	}
	if (Keyboard_Controls.tilde_key == true)
	{
		Keyboard_Controls.tilde_key_held = true;
	}
	if (Keyboard_Controls.dash_key == true)
	{
		Keyboard_Controls.dash_key_held = true;
	}
	if (Keyboard_Controls.equal_key == true)
	{
		Keyboard_Controls.equal_key_held = true;
	}
	if (Keyboard_Controls.backspace_key == true)
	{
		Keyboard_Controls.backspace_key_held = true;
	}
	if (Keyboard_Controls.tab_key == true)
	{
		Keyboard_Controls.tab_key_held = true;
	}
	if (Keyboard_Controls.left_bracket_key == true)
	{
		Keyboard_Controls.left_bracket_key_held = true;
	}
	if (Keyboard_Controls.right_bracket_key == true)
	{
		Keyboard_Controls.right_bracket_key_held = true;
	}
	if (Keyboard_Controls.enter_key == true)
	{
		Keyboard_Controls.enter_key_held = true;
	}
	if (Keyboard_Controls.semicolon_key == true)
	{
		Keyboard_Controls.semicolon_key_held = true;
	}
	if (Keyboard_Controls.single_quote_key == true)
	{
		Keyboard_Controls.single_quote_key_held = true;
	}
	if (Keyboard_Controls.slash_key == true)
	{
		Keyboard_Controls.slash_key_held = true;
	}
	if (Keyboard_Controls.comma_key == true)
	{
		Keyboard_Controls.comma_key_held = true;
	}
	if (Keyboard_Controls.period_key == true)
	{
		Keyboard_Controls.period_key_held = true;
	}
	if (Keyboard_Controls.questionmark_key == true)
	{
		Keyboard_Controls.questionmark_key_held = true;
	}
	if (Keyboard_Controls.space_key == true)
	{
		Keyboard_Controls.space_key_held = true;
	}
	if (Keyboard_Controls.left_arrow_key == true)
	{
		Keyboard_Controls.left_arrow_key_held = true;
	}
	if (Keyboard_Controls.right_arrow_key == true)
	{
		Keyboard_Controls.right_arrow_key_held = true;
	}
	if (Keyboard_Controls.up_arrow_key == true)
	{
		Keyboard_Controls.up_arrow_key_held = true;
	}
	if (Keyboard_Controls.down_arrow_key == true)
	{
		Keyboard_Controls.down_arrow_key_held = true;
	}
	if (Keyboard_Controls.left_shift_key == true)
	{
		Keyboard_Controls.left_shift_key_held = true;
	}
	if (Keyboard_Controls.right_shift_key == true)
	{
		Keyboard_Controls.right_shift_key_held = true;
	}
}

void Input::AnalogMoveEvent(const ALLEGRO_EVENT &events, AbstractControlIndex analog)
{
	int current_controller = MatchController(events);			// Obtain the the numerical value of which controller made the input

	x_Input *traverse = head;									// Start at the beginning of the link list for active controllers

	if (current_controller == -1)
	{
		printf("INPUT ERROR WITH CONTROLLERS AND EVERYTHING IS BROKEN\n");
	}
	else
	{
		for (int i = 0; i < current_controller; i++)			// Find the correct controller that made that input
		{
			traverse = traverse->next;
		}
	}
	switch (analog)
	{
	case 20:
		traverse->button_left_stick_left = true;
		break;
	case 21:
		traverse->button_left_stick_right = true;
		break;
	case 22:
		traverse->button_left_stick_up = true;
		break;
	case 23:
		traverse->button_left_stick_down = true;
		break;
	case 24:
		traverse->button_right_stick_left = true;
		break;
	case 25:
		traverse->button_right_stick_right = true;
		break;
	case 26:
		traverse->button_right_stick_up = true;
		break;
	case 27:
		traverse->button_right_stick_down = true;
		break;
	case 28:
		traverse->button_right_trigger = true;
		break;
	case 29:
		traverse->button_left_trigger = true;
		break;
	}
}

void Input::AnalogNotMoveEvent(const ALLEGRO_EVENT &events, AbstractControlIndex analog)
{
	int current_controller = MatchController(events);			// Obtain the the numerical value of which controller made the input

	x_Input *traverse = head;									// Start at the beginning of the link list for active controllers

	if (current_controller == -1)
	{
		printf("INPUT ERROR WITH CONTROLLERS AND EVERYTHING IS BROKEN\n");
	}
	else
	{
		for (int i = 0; i < current_controller; i++)			// Find the correct controller that made that input
		{
			traverse = traverse->next;
		}
	}
	switch (analog)
	{
	case 20:
		traverse->button_left_stick_left = false;
		traverse->held_button_left_stick_left = false;
		break;
	case 21:
		traverse->button_left_stick_right = false;
		traverse->held_button_left_stick_right = false;
		break;
	case 22:
		traverse->button_left_stick_up = false;
		traverse->held_button_left_stick_up = false;
		break;
	case 23:
		traverse->button_left_stick_down = false;
		traverse->held_button_left_stick_down = false;
		break;
	case 24:
		traverse->button_right_stick_left = false;
		traverse->held_button_right_stick_left = false;
		break;
	case 25:
		traverse->button_right_stick_right = false;
		traverse->held_button_right_stick_right = false;
		break;
	case 26:
		traverse->button_right_stick_up = false;
		traverse->held_button_right_stick_up = false;
		break;
	case 27:
		traverse->button_right_stick_down = false;
		traverse->held_button_right_stick_down = false;
		break;
	case 28:
		traverse->button_right_trigger = false;
		traverse->held_button_right_trigger = false;
		break;
	case 29:
		traverse->button_left_trigger = false;
		traverse->held_button_right_trigger = false;
		break;
	}
}

void Input::UpdateControllerInput(const ALLEGRO_EVENT &events)
{
	if (events.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN)
	{
		//printf("Button was pressed!\n");

		ButtonDownEvent(events);
	}
	else if (events.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_UP)
	{
		//printf("Button was released!\n");
		ButtonUpEvent(events);
	}
	else if (events.type == ALLEGRO_EVENT_JOYSTICK_AXIS)
	{
		//printf("Analog was Moved!\n");
		switch (events.joystick.stick)
		{
		case 0:														// Left analog stick
			if (events.joystick.axis == 0)							// If the anlog moved from left to right
			{
				//printf("%f\n", events.joystick.pos);
				if (events.joystick.pos == 1)						// Moved right with the analog
				{
					//printf("Moving right\n");
					AnalogMoveEvent(events, AINDEX_RIGHT_LANALOG);
				}
				else if (events.joystick.pos == -1)					// Moved left with the analog
				{
					//printf("Moving Left\n");
					AnalogMoveEvent(events, AINDEX_LEFT_LANALOG);
				}
				else 	// Moved neither left or right
				{
					AnalogNotMoveEvent(events, AINDEX_RIGHT_LANALOG);
					AnalogNotMoveEvent(events, AINDEX_LEFT_LANALOG);
				}
			}
			else if (events.joystick.axis == 1)						// Moved the anlog up or down
			{
				//printf("%f\n", events.joystick.pos);
				if (events.joystick.pos == 1)						// Moved the analog down
				{
					//printf("Moving Down\n");
					AnalogMoveEvent(events, AINDEX_DOWN_LANALOG);
				}

				else if (events.joystick.pos == -1)				// Moved the analog up
				{
					//printf("Moving Up\n");
					AnalogMoveEvent(events, AINDEX_UP_LANALOG);
				}

				else 	// Moved neither up or down
				{
					AnalogNotMoveEvent(events, AINDEX_DOWN_LANALOG);
					AnalogNotMoveEvent(events, AINDEX_UP_LANALOG);
				}
			}
			break;
		case 1:														// Moved the right analog stick
			if (events.joystick.axis == 0)							// If the anlog moved from left to right
			{
				if (events.joystick.pos > .99)						// Moved right with the analog
				{
					AnalogMoveEvent(events, AINDEX_RIGHT_RANALOG);
				}

				else if (events.joystick.pos < .99)				// Moved left with the analog
				{
					AnalogMoveEvent(events, AINDEX_LEFT_RANALOG);
				}

				else  	// Moved neither left or right
				{
					AnalogNotMoveEvent(events, AINDEX_RIGHT_RANALOG);
					AnalogNotMoveEvent(events, AINDEX_LEFT_RANALOG);
				}
			}
			else if (events.joystick.axis == 1)						// Moved the anlog up or down
			{
				if (events.joystick.pos > .99)						// Moved the analog down
				{
					AnalogMoveEvent(events, AINDEX_UP_RANALOG);
				}

				else if (events.joystick.pos < -.99)				// Moved the analog up
				{
					AnalogMoveEvent(events, AINDEX_DOWN_RANALOG);
				}

				else // Moved neither up or down
				{
					AnalogNotMoveEvent(events, AINDEX_UP_RANALOG);
					AnalogNotMoveEvent(events, AINDEX_DOWN_RANALOG);
				}
			}
			break;
		case 2:
			//analogPressed(events.joystick.stick);
			//printf("%f\n", events.joystick.pos);
			if (events.joystick.pos == 1)
			{
				AnalogMoveEvent(events, AINDEX_LEFT_TRIGGER);
			}
			else
			{
				AnalogNotMoveEvent(events, AINDEX_LEFT_TRIGGER);
			}
			break;
		case 3:
			//analogPressed(events.joystick.stick);
			//printf("%f\n", events.joystick.pos);
			if (events.joystick.pos == 1)
			{
				AnalogMoveEvent(events, AINDEX_RIGHT_TRIGGER);
			}
			else
			{
				AnalogNotMoveEvent(events, AINDEX_RIGHT_TRIGGER);
			}
			break;
		default:
			break;
		}
	}
}

// Only call this when you know an input has been made by the controller
int Input::MatchController(const ALLEGRO_EVENT &events)
{
	for (int i = 0; i < how_many_controllers; i++)				// Checks to see which controller made the input
	{
		if (events.joystick.id == joysticks[i])
		{
			return i;
		}
	}
	return -1;
}

bool Input::WasButtonPressed(int current_controller, int button)
{
	x_Input *traverse = head;									// Start at the beginning of the link list for active controllers

	int controller = 0;
	while (controller != current_controller)
	{
		traverse = traverse->next;
		controller++;
	}

	if (traverse == nullptr)									// If the controller does not exist
	{
		return false;
	}
	switch (button)												// Find what button was pressed
	{
	case 0:														// A button
		return traverse->button_a;
		break;
	case 1:														// B button
		return traverse->button_b;
		break;
	case 2:
		return traverse->button_x;
		break;
	case 3:
		return traverse->button_y;
		break;
	case 4:
		return traverse->button_right_bumber;
		break;
	case 5:
		return traverse->button_left_bumper;
		break;
	case 6:
		return traverse->button_right_stick;
		break;
	case 7:
		return traverse->button_left_stick;
		break;
	case 8:
		return traverse->button_select;
		break;
	case 9:
		return traverse->button_start;
		break;
	case 10:
		return traverse->dpad_right;
		break;
	case 11:
		return traverse->dpad_left;
		break;
	case 12:
		return traverse->dpad_down;
		break;
	case 13:
		return traverse->dpad_up;
		break;
	case 20:
		return traverse->button_left_stick_left;
		break;
	case 21:
		return traverse->button_left_stick_right;
		break;
	case 22:
		return traverse->button_left_stick_up;
		break;
	case 23:
		return traverse->button_left_stick_down;
		break;
	case 24:
		return traverse->button_right_stick_left;
		break;
	case 25:
		return traverse->button_right_stick_right;
		break;
	case 26:
		return traverse->button_right_stick_up;
		break;
	case 27:
		return traverse->button_right_stick_down;
		break;
	case 28:
		return traverse->button_right_trigger;
		break;
	case 29:
		return traverse->button_left_trigger;
		break;
	default:
		//printf("ERROR WITH BUTTON INPUT PASS\n");
		return false;
		break;
	}
}

bool Input::WasButtonHeld(int current_controller, int button)
{
	x_Input *traverse = head;									// Start at the beginning of the link list for active controllers

	int controller = 0;
	while (controller != current_controller)
	{
		traverse = traverse->next;
		controller++;
	}

	switch (button)												// Find what button was pressed
	{
	case 0:														// A button
		return traverse->held_button_a;
		break;
	case 1:														// B button
		return traverse->held_button_b;
		break;
	case 2:
		return traverse->held_button_x;
		break;
	case 3:
		return traverse->held_button_y;
		break;
	case 4:
		return traverse->held_button_right_bumber;
		break;
	case 5:
		return traverse->held_button_left_bumper;
		break;
	case 6:
		return traverse->held_button_right_stick;
		break;
	case 7:
		return traverse->held_button_left_stick;
		break;
	case 8:
		return traverse->held_button_select;
		break;
	case 9:
		return traverse->held_button_start;
		break;
	case 10:
		return traverse->held_dpad_right;
		break;
	case 11:
		return traverse->held_dpad_left;
		break;
	case 12:
		return traverse->held_dpad_down;
		break;
	case 13:
		return traverse->held_dpad_up;
		break;
	case 20:
		return traverse->held_button_left_stick_left;
		break;
	case 21:
		return traverse->held_button_left_stick_right;
		break;
	case 22:
		return traverse->held_button_left_stick_up;
		break;
	case 23:
		return traverse->held_button_left_stick_down;
		break;
	case 24:
		return traverse->held_button_right_stick_left;
		break;
	case 25:
		return traverse->held_button_right_stick_right;
		break;
	case 26:
		return traverse->held_button_right_stick_up;
		break;
	case 27:
		return traverse->held_button_right_stick_down;
		break;
	case 28:
		return traverse->held_button_right_trigger;
		break;
	case 29:
		return traverse->held_button_left_trigger;
		break;
	default:
		//printf("ERROR WITH BUTTON INPUT PASS\n");
		return false;
		break;
	}

}

bool Input::WasKeyPressed(int key)
{
	switch (key)
	{
	case KEYCODE_A:
		return Keyboard_Controls.a_key;
		break;
	case KEYCODE_B:
		return Keyboard_Controls.b_key;
		break;
	case KEYCODE_C:
		return Keyboard_Controls.c_key;
		break;
	case KEYCODE_D:
		return Keyboard_Controls.d_key;
		break;
	case KEYCODE_E:
		return Keyboard_Controls.e_key;
		break;
	case KEYCODE_F:
		return Keyboard_Controls.f_key;
		break;
	case KEYCODE_G:
		return Keyboard_Controls.g_key;
		break;
	case KEYCODE_H:
		return Keyboard_Controls.h_key;
		break;
	case KEYCODE_I:
		return Keyboard_Controls.i_key;
		break;
	case KEYCODE_J:
		return Keyboard_Controls.j_key;
		break;
	case KEYCODE_K:
		return Keyboard_Controls.k_key;
		break;
	case KEYCODE_L:
		return Keyboard_Controls.l_key;
		break;
	case KEYCODE_M:
		return Keyboard_Controls.m_key;
		break;
	case KEYCODE_N:
		return Keyboard_Controls.n_key;
		break;
	case KEYCODE_O:
		return Keyboard_Controls.o_key;
		break;
	case KEYCODE_P:
		return Keyboard_Controls.p_key;
		break;
	case KEYCODE_Q:
		return Keyboard_Controls.q_key;
		break;
	case KEYCODE_R:
		return Keyboard_Controls.r_key;
		break;
	case KEYCODE_S:
		return Keyboard_Controls.s_key;
		break;
	case KEYCODE_T:
		return Keyboard_Controls.t_key;
		break;
	case KEYCODE_U:
		return Keyboard_Controls.u_key;
		break;
	case KEYCODE_V:
		return Keyboard_Controls.v_key;
		break;
	case KEYCODE_W:
		return Keyboard_Controls.w_key;
		break;
	case KEYCODE_X:
		return Keyboard_Controls.x_key;
		break;
	case KEYCODE_Y:
		return Keyboard_Controls.y_key;
		break;
	case KEYCODE_Z:
		return Keyboard_Controls.z_key;
		break;
	case KEYCODE_0:
		return Keyboard_Controls.zero_key;
		break;
	case KEYCODE_1:
		return Keyboard_Controls.one_key;
		break;
	case KEYCODE_2:
		return Keyboard_Controls.two_key;
		break;
	case KEYCODE_3:
		return Keyboard_Controls.three_key;
		break;
	case KEYCODE_4:
		return Keyboard_Controls.four_key;
		break;
	case KEYCODE_5:
		return Keyboard_Controls.five_key;
		break;
	case KEYCODE_6:
		return Keyboard_Controls.six_key;
		break;
	case KEYCODE_7:
		return Keyboard_Controls.seven_key;
		break;
	case KEYCODE_8:
		return Keyboard_Controls.eight_key;
		break;
	case KEYCODE_9:
		return Keyboard_Controls.nine_key;
		break;
	case KEYCODE_0_PAD:
		return Keyboard_Controls.zero_pad_key;
		break;
	case KEYCODE_1_PAD:
		return Keyboard_Controls.one_pad_key;
		break;
	case KEYCODE_2_PAD:
		return Keyboard_Controls.two_pad_key;
		break;
	case KEYCODE_3_PAD:
		return Keyboard_Controls.three_pad_key;
		break;
	case KEYCODE_4_PAD:
		return Keyboard_Controls.four_pad_key;
		break;
	case KEYCODE_5_PAD:
		return Keyboard_Controls.five_pad_key;
		break;
	case KEYCODE_6_PAD:
		return Keyboard_Controls.six_pad_key;
		break;
	case KEYCODE_7_PAD:
		return Keyboard_Controls.seven_pad_key;
		break;
	case KEYCODE_8_PAD:
		return Keyboard_Controls.eight_pad_key;
		break;
	case KEYCODE_9_PAD:
		return Keyboard_Controls.nine_pad_key;
		break;
	case KEYCODE_ESCAPE:
		return Keyboard_Controls.escape_key;
		break;
	case KEYCODE_TILDE:
		return Keyboard_Controls.tilde_key;
		break;
	case KEYCODE_DASH:
		return Keyboard_Controls.dash_key;
		break;
	case KEYCODE_EQUAL:
		return Keyboard_Controls.equal_key;
		break;
	case KEYCODE_BACKSPACE:
		return Keyboard_Controls.backspace_key;
		break;
	case KEYCODE_TAB:
		return Keyboard_Controls.tab_key;
		break;
	case KEYCODE_LEFTBRACKET:
		return Keyboard_Controls.left_bracket_key;
		break;
	case KEYCODE_RIGHTBRACKET:
		return Keyboard_Controls.right_bracket_key;
		break;
	case KEYCODE_ENTER:
		return Keyboard_Controls.enter_key;
		break;
	case KEYCODE_SEMICOLON:
		return Keyboard_Controls.semicolon_key;
		break;
	case KEYCODE_SINGLEQUOTE:
		return Keyboard_Controls.single_quote_key;
		break;
	case KEYCODE_SLASH:
		return Keyboard_Controls.slash_key;
		break;
	case KEYCODE_COMMA:
		return Keyboard_Controls.comma_key;
		break;
	case KEYCODE_PERIOD:
		return Keyboard_Controls.period_key;
		break;
	case KEYCODE_QUESTIONMARK:
		return Keyboard_Controls.questionmark_key;
		break;
	case KEYCODE_SPACE:
		return Keyboard_Controls.space_key;
		break;
	case KEYCODE_LEFTARROW:
		return Keyboard_Controls.left_arrow_key;
		break;
	case KEYCODE_RIGHTARROW:
		return Keyboard_Controls.right_arrow_key;
		break;
	case KEYCODE_UPARROW:
		return Keyboard_Controls.up_arrow_key;
		break;
	case KEYCODE_DOWNARROW:
		return Keyboard_Controls.down_arrow_key;
		break;
	case KEYCDOE_LEFTSHIFT:
		return Keyboard_Controls.left_shift_key;
		break;
	case KEYCODE_RIGHTSHIFT:
		return Keyboard_Controls.right_shift_key;
		break;
	default:
		return false;
	}
}

bool Input::WasKeyHeld(int key)
{
	switch (key)
	{
	case KEYCODE_A:
		return Keyboard_Controls.a_key_held;
		break;
	case KEYCODE_B:
		return Keyboard_Controls.b_key_held;
		break;
	case KEYCODE_C:
		return Keyboard_Controls.c_key_held;
		break;
	case KEYCODE_D:
		return Keyboard_Controls.d_key_held;
		break;
	case KEYCODE_E:
		return Keyboard_Controls.e_key_held;
		break;
	case KEYCODE_F:
		return Keyboard_Controls.f_key_held;
		break;
	case KEYCODE_G:
		return Keyboard_Controls.g_key_held;
		break;
	case KEYCODE_H:
		return Keyboard_Controls.h_key_held;
		break;
	case KEYCODE_I:
		return Keyboard_Controls.i_key_held;
		break;
	case KEYCODE_J:
		return Keyboard_Controls.j_key_held;
		break;
	case KEYCODE_K:
		return Keyboard_Controls.k_key_held;
		break;
	case KEYCODE_L:
		return Keyboard_Controls.l_key_held;
		break;
	case KEYCODE_M:
		return Keyboard_Controls.m_key_held;
		break;
	case KEYCODE_N:
		return Keyboard_Controls.n_key_held;
		break;
	case KEYCODE_O:
		return Keyboard_Controls.o_key_held;
		break;
	case KEYCODE_P:
		return Keyboard_Controls.p_key_held;
		break;
	case KEYCODE_Q:
		return Keyboard_Controls.q_key_held;
		break;
	case KEYCODE_R:
		return Keyboard_Controls.r_key_held;
		break;
	case KEYCODE_S:
		return Keyboard_Controls.s_key_held;
		break;
	case KEYCODE_T:
		return Keyboard_Controls.t_key_held;
		break;
	case KEYCODE_U:
		return Keyboard_Controls.u_key_held;
		break;
	case KEYCODE_V:
		return Keyboard_Controls.v_key_held;
		break;
	case KEYCODE_W:
		return Keyboard_Controls.w_key_held;
		break;
	case KEYCODE_X:
		return Keyboard_Controls.x_key_held;
		break;
	case KEYCODE_Y:
		return Keyboard_Controls.y_key_held;
		break;
	case KEYCODE_Z:
		return Keyboard_Controls.z_key_held;
		break;
	case KEYCODE_0:
		return Keyboard_Controls.zero_key_held;
		break;
	case KEYCODE_1:
		return Keyboard_Controls.one_key_held;
		break;
	case KEYCODE_2:
		return Keyboard_Controls.two_key_held;
		break;
	case KEYCODE_3:
		return Keyboard_Controls.three_key_held;
		break;
	case KEYCODE_4:
		return Keyboard_Controls.four_key_held;
		break;
	case KEYCODE_5:
		return Keyboard_Controls.five_key_held;
		break;
	case KEYCODE_6:
		return Keyboard_Controls.six_key_held;
		break;
	case KEYCODE_7:
		return Keyboard_Controls.seven_key_held;
		break;
	case KEYCODE_8:
		return Keyboard_Controls.eight_key_held;
		break;
	case KEYCODE_9:
		return Keyboard_Controls.nine_key_held;
		break;
	case KEYCODE_0_PAD:
		return Keyboard_Controls.zero_pad_key_held;
		break;
	case KEYCODE_1_PAD:
		return Keyboard_Controls.one_pad_key_held;
		break;
	case KEYCODE_2_PAD:
		return Keyboard_Controls.two_pad_key_held;
		break;
	case KEYCODE_3_PAD:
		return Keyboard_Controls.three_pad_key_held;
		break;
	case KEYCODE_4_PAD:
		return Keyboard_Controls.four_pad_key_held;
		break;
	case KEYCODE_5_PAD:
		return Keyboard_Controls.five_pad_key_held;
		break;
	case KEYCODE_6_PAD:
		return Keyboard_Controls.six_pad_key_held;
		break;
	case KEYCODE_7_PAD:
		return Keyboard_Controls.seven_pad_key_held;
		break;
	case KEYCODE_8_PAD:
		return Keyboard_Controls.eight_pad_key_held;
		break;
	case KEYCODE_9_PAD:
		return Keyboard_Controls.nine_pad_key_held;
		break;
	case KEYCODE_ESCAPE:
		return Keyboard_Controls.escape_key_held;
		break;
	case KEYCODE_TILDE:
		return Keyboard_Controls.tilde_key_held;
		break;
	case KEYCODE_DASH:
		return Keyboard_Controls.dash_key_held;
		break;
	case KEYCODE_EQUAL:
		return Keyboard_Controls.equal_key_held;
		break;
	case KEYCODE_BACKSPACE:
		return Keyboard_Controls.backspace_key_held;
		break;
	case KEYCODE_TAB:
		return Keyboard_Controls.tab_key_held;
		break;
	case KEYCODE_LEFTBRACKET:
		return Keyboard_Controls.left_bracket_key_held;
		break;
	case KEYCODE_RIGHTBRACKET:
		return Keyboard_Controls.right_bracket_key_held;
		break;
	case KEYCODE_ENTER:
		return Keyboard_Controls.enter_key_held;
		break;
	case KEYCODE_SEMICOLON:
		return Keyboard_Controls.semicolon_key_held;
		break;
	case KEYCODE_SINGLEQUOTE:
		return Keyboard_Controls.single_quote_key_held;
		break;
	case KEYCODE_SLASH:
		return Keyboard_Controls.slash_key_held;
		break;
	case KEYCODE_COMMA:
		return Keyboard_Controls.comma_key_held;
		break;
	case KEYCODE_PERIOD:
		return Keyboard_Controls.period_key_held;
		break;
	case KEYCODE_QUESTIONMARK:
		return Keyboard_Controls.questionmark_key_held;
		break;
	case KEYCODE_SPACE:
		return Keyboard_Controls.space_key_held;
		break;
	case KEYCODE_LEFTARROW:
		return Keyboard_Controls.left_arrow_key_held;
		break;
	case KEYCODE_RIGHTARROW:
		return Keyboard_Controls.right_arrow_key_held;
		break;
	case KEYCODE_UPARROW:
		return Keyboard_Controls.up_arrow_key_held;
		break;
	case KEYCODE_DOWNARROW:
		return Keyboard_Controls.down_arrow_key_held;
		break;
	case KEYCDOE_LEFTSHIFT:
		return Keyboard_Controls.left_shift_key_held;
		break;
	case KEYCODE_RIGHTSHIFT:
		return Keyboard_Controls.right_shift_key_held;
		break;
	default:
		return false;
		break;
	}
}

void Input::ClearKeyBoardInputs()
{
	Keyboard_Controls.a_key = false;
	Keyboard_Controls.a_key_held = false;
	Keyboard_Controls.b_key = false;
	Keyboard_Controls.b_key_held = false;
	Keyboard_Controls.c_key = false;
	Keyboard_Controls.c_key_held = false;
	Keyboard_Controls.d_key = false;
	Keyboard_Controls.d_key_held = false;
	Keyboard_Controls.e_key = false;
	Keyboard_Controls.e_key_held = false;
	Keyboard_Controls.f_key = false;
	Keyboard_Controls.f_key_held = false;
	Keyboard_Controls.g_key = false;
	Keyboard_Controls.g_key_held = false;
	Keyboard_Controls.h_key = false;
	Keyboard_Controls.h_key_held = false;
	Keyboard_Controls.i_key = false;
	Keyboard_Controls.i_key_held = false;
	Keyboard_Controls.j_key = false;
	Keyboard_Controls.j_key_held = false;
	Keyboard_Controls.k_key = false;
	Keyboard_Controls.k_key_held = false;
	Keyboard_Controls.l_key = false;
	Keyboard_Controls.l_key_held = false;
	Keyboard_Controls.m_key = false;
	Keyboard_Controls.m_key_held = false;
	Keyboard_Controls.n_key = false;
	Keyboard_Controls.n_key_held = false;
	Keyboard_Controls.o_key = false;
	Keyboard_Controls.o_key_held = false;
	Keyboard_Controls.p_key = false;
	Keyboard_Controls.p_key_held = false;
	Keyboard_Controls.q_key = false;
	Keyboard_Controls.q_key_held = false;
	Keyboard_Controls.r_key = false;
	Keyboard_Controls.r_key_held = false;
	Keyboard_Controls.s_key = false;
	Keyboard_Controls.s_key_held = false;
	Keyboard_Controls.t_key = false;
	Keyboard_Controls.t_key_held = false;
	Keyboard_Controls.u_key = false;
	Keyboard_Controls.u_key_held = false;
	Keyboard_Controls.v_key = false;
	Keyboard_Controls.v_key_held = false;
	Keyboard_Controls.w_key = false;
	Keyboard_Controls.w_key_held = false;
	Keyboard_Controls.x_key = false;
	Keyboard_Controls.x_key_held = false;
	Keyboard_Controls.y_key = false;
	Keyboard_Controls.y_key_held = false;
	Keyboard_Controls.z_key = false;
	Keyboard_Controls.z_key_held = false;
	Keyboard_Controls.zero_key = false;
	Keyboard_Controls.zero_key_held = false;
	Keyboard_Controls.one_key = false;
	Keyboard_Controls.one_key_held = false;
	Keyboard_Controls.two_key = false;
	Keyboard_Controls.two_key_held = false;
	Keyboard_Controls.three_key = false;
	Keyboard_Controls.three_key_held = false;
	Keyboard_Controls.four_key = false;
	Keyboard_Controls.four_key_held = false;
	Keyboard_Controls.five_key = false;
	Keyboard_Controls.five_key_held = false;
	Keyboard_Controls.six_key = false;
	Keyboard_Controls.six_key_held = false;
	Keyboard_Controls.seven_key = false;
	Keyboard_Controls.seven_key_held = false;
	Keyboard_Controls.eight_key = false;
	Keyboard_Controls.eight_key_held = false;
	Keyboard_Controls.nine_key = false;
	Keyboard_Controls.nine_key_held = false;
	Keyboard_Controls.zero_pad_key = false;
	Keyboard_Controls.zero_pad_key_held = false;
	Keyboard_Controls.one_pad_key = false;
	Keyboard_Controls.one_pad_key_held = false;
	Keyboard_Controls.two_pad_key = false;
	Keyboard_Controls.two_pad_key_held = false;
	Keyboard_Controls.three_pad_key = false;
	Keyboard_Controls.three_pad_key_held = false;
	Keyboard_Controls.four_pad_key = false;
	Keyboard_Controls.four_pad_key_held = false;
	Keyboard_Controls.five_pad_key = false;
	Keyboard_Controls.five_pad_key_held = false;
	Keyboard_Controls.six_pad_key = false;
	Keyboard_Controls.six_pad_key_held = false;
	Keyboard_Controls.seven_pad_key = false;
	Keyboard_Controls.seven_pad_key_held = false;
	Keyboard_Controls.eight_pad_key = false;
	Keyboard_Controls.eight_pad_key_held = false;
	Keyboard_Controls.nine_pad_key = false;
	Keyboard_Controls.nine_pad_key_held = false;
	Keyboard_Controls.escape_key = false;
	Keyboard_Controls.escape_key_held = false;
	Keyboard_Controls.tilde_key = false;
	Keyboard_Controls.tilde_key_held = false;
	Keyboard_Controls.dash_key = false;
	Keyboard_Controls.dash_key_held = false;
	Keyboard_Controls.equal_key = false;
	Keyboard_Controls.equal_key_held = false;
	Keyboard_Controls.backspace_key = false;
	Keyboard_Controls.backspace_key_held = false;
	Keyboard_Controls.tab_key = false;
	Keyboard_Controls.tab_key_held = false;
	Keyboard_Controls.left_bracket_key = false;
	Keyboard_Controls.left_bracket_key_held = false;
	Keyboard_Controls.right_bracket_key = false;
	Keyboard_Controls.right_bracket_key_held = false;
	Keyboard_Controls.enter_key = false;
	Keyboard_Controls.enter_key_held = false;
	Keyboard_Controls.semicolon_key = false;
	Keyboard_Controls.semicolon_key_held = false;
	Keyboard_Controls.single_quote_key = false;
	Keyboard_Controls.single_quote_key_held = false;
	Keyboard_Controls.slash_key = false;
	Keyboard_Controls.slash_key_held = false;
	Keyboard_Controls.comma_key = false;
	Keyboard_Controls.comma_key_held = false;
	Keyboard_Controls.period_key = false;
	Keyboard_Controls.period_key_held = false;
	Keyboard_Controls.questionmark_key = false;
	Keyboard_Controls.questionmark_key_held = false;
	Keyboard_Controls.space_key = false;
	Keyboard_Controls.space_key_held = false;
	Keyboard_Controls.left_arrow_key = false;
	Keyboard_Controls.left_arrow_key_held = false;
	Keyboard_Controls.right_arrow_key = false;
	Keyboard_Controls.right_arrow_key_held = false;
	Keyboard_Controls.up_arrow_key = false;
	Keyboard_Controls.up_arrow_key_held = false;
	Keyboard_Controls.down_arrow_key = false;
	Keyboard_Controls.down_arrow_key_held = false;
	Keyboard_Controls.left_shift_key = false;
	Keyboard_Controls.left_shift_key_held = false;
	Keyboard_Controls.right_shift_key = false;
	Keyboard_Controls.right_shift_key_held = false;

}