#include "Audio.h"



Audio::Audio()
{
}


Audio::~Audio()
{
	DestroyAudio();
}

void Audio::LoadAudioFiles(std::string file_path)
{
	audio_sample = al_load_sample(file_path.c_str());							// Load the audio file using the filePath
}

void Audio::PlaySongLoop()
{
	al_play_sample(audio_sample,
		1.0, 0, 1.0, ALLEGRO_PLAYMODE_LOOP,
		&audio_sample_id);
}

void Audio::PlaySongOnce()
{
	al_play_sample(audio_sample,
		1.0, 0, 1.0, ALLEGRO_PLAYMODE_ONCE,
		&audio_sample_id);
}

void Audio::StopSong()
{
	al_stop_sample(&audio_sample_id);
}

void Audio::StopAllSound()
{
	al_stop_samples();
}

void Audio::DestroyAudio()
{
	al_destroy_sample(audio_sample);									// Destroy the sample file
}