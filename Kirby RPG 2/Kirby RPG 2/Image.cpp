#include "Image.h"

#include "allegro5\allegro.h"
#include "allegro5\allegro_image.h"
#include "allegro5\allegro_font.h"
#include "allegro5\allegro_color.h"

Image::Image()
{
}

Image::~Image()
{
	DestroyBitmap();
}

void Image::LoadBitmap(std::string file_path)
{
	bitmap = al_load_bitmap(file_path.c_str());

	if (bitmap == NULL)
	{
		printf("%s did not load!\n", file_path);
	}
}

void Image::LoadSubBitmap(int x_pos, int y_pos, int width, int height)
{
	bitmap = al_create_sub_bitmap(bitmap, x_pos, y_pos, width, height);
}

int Image::GetBitmapWidth()
{
	return al_get_bitmap_width(bitmap);
}

int Image::GetBitmapHeight()
{
	return al_get_bitmap_height(bitmap);
}

void Image::CreateTextRender(ALLEGRO_FONT *font, ALLEGRO_COLOR color, std::string text)
{
	ALLEGRO_STATE previous_state;
	al_store_state(&previous_state, ALLEGRO_STATE_TARGET_BITMAP);
	int bitmap_w = al_get_text_width(font, text.c_str());
	int bitmap_h = al_get_font_line_height(font);
	bitmap = al_create_bitmap(bitmap_w, bitmap_h);

	al_set_target_bitmap(bitmap);
	al_clear_to_color(al_map_rgba_f(0.0, 0.0, 0.0, 0.0));

	al_draw_text(font, color, 0, 0, NULL, text.c_str());

	al_restore_state(&previous_state);

}

void Image::UpdateTextImage(ALLEGRO_FONT *font,std::string text, int value)
{
	std::string textString = text + std::to_string(value);
	CreateTextRender(font, al_map_rgb(0, 0, 0), textString.c_str());
}

ALLEGRO_BITMAP* Image::GetImage()
{
	return bitmap;
}

void Image::DestroyBitmap()
{
	al_destroy_bitmap(bitmap);
}