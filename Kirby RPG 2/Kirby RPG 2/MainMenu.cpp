#include "MainMenu.h"



MainMenu::MainMenu()
{
	SetUpMainMenu();
}


MainMenu::~MainMenu()
{
	main_menu_song.StopSong();
}

void MainMenu::SetUpMainMenu()
{
	cursor_y_position = 0;												// Default is the first option which is new game

	background_image.LoadBitmap("Content/Backgrounds/Main Menu Background.png");

	background_animation.AddIndividualFrame(0, 0, 5, 175, 240, 160);
	background_animation.AddIndividualFrame(0, 0, 250, 175, 240, 160);
	background_animation.AddIndividualFrame(0, 0, 495, 175, 240, 160);
	background_animation.AddIndividualFrame(0, 0, 5, 340, 240, 160);
	background_animation.AddIndividualFrame(0, 0, 250, 340, 240, 160);
	background_animation.AddIndividualFrame(0, 0, 495,340, 240, 160);
	background_animation.CombineFrames(5);

	vincentio_font36.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 36);

	new_game_text.CreateTextRender(vincentio_font36.ObtainFont(), al_map_rgb(255, 255, 255), "New Game");
	load_game_text.CreateTextRender(vincentio_font36.ObtainFont(), al_map_rgb(255, 255, 255), "Load Game");
	option_text.CreateTextRender(vincentio_font36.ObtainFont(), al_map_rgb(255, 255, 255), "Options");
	enemies_text.CreateTextRender(vincentio_font36.ObtainFont(), al_map_rgb(255, 255, 255), "Enemy Infos");
	kirby_text.CreateTextRender(vincentio_font36.ObtainFont(), al_map_rgb(255, 255, 255), "Owned Kirbies");
	quit_text.CreateTextRender(vincentio_font36.ObtainFont(), al_map_rgb(255, 255, 255), "Quit Game");

	cursor_image.LoadBitmap("Content/Miscellaneous/SNES - Kirby Super Star Kirbys Fun Pak - Game Over & Continue Screens.png");
	
	cursor_animation.AddIndividualFrame(50, 110, 7, 237, 24, 24);
	cursor_animation.CombineFrames(0);

	main_menu_song.LoadAudioFiles("Content/Music/Fluffy Main Menu.wav");
	main_menu_song.PlaySongLoop();
	moving_cursor.LoadAudioFiles("Content/Sound Effects/Moving Cursor.wav");
	select_option.LoadAudioFiles("Content/Sound Effects/menu-select.wav");

}

void MainMenu::UpdateMainMenu()
{
	background_animation.Update();
}

void MainMenu::RenderMainMenu(float scale_x, float scale_y)
{
	background_image.DrawScaledImage(background_animation.GetCurrentFrame().GetSourceX(),
		background_animation.GetCurrentFrame().GetSourceY(),
		background_animation.GetCurrentFrame().GetWidth(),
		background_animation.GetCurrentFrame().GetHeight(),
		0, 0,
		(float)background_animation.GetCurrentFrame().GetWidth() * 5.33 * scale_x,
		(float)background_animation.GetCurrentFrame().GetHeight() * 4.5 * scale_y, 
		NULL);

	new_game_text.DrawScaledImage(0, 0,
		new_game_text.GetBitmapWidth(),
		new_game_text.GetBitmapHeight(),
		100 * scale_x, 100 * scale_y,
		(float)new_game_text.GetBitmapWidth() * scale_x,
		(float)new_game_text.GetBitmapHeight() * scale_y,
		NULL);

	load_game_text.DrawScaledImage(0, 0,
		load_game_text.GetBitmapWidth(),
		load_game_text.GetBitmapHeight(),
		100 * scale_x, 200 * scale_y,
		(float)load_game_text.GetBitmapWidth() * scale_x,
		(float)load_game_text.GetBitmapHeight() * scale_y,
		NULL);

	option_text.DrawScaledImage(0, 0,
		option_text.GetBitmapWidth(),
		option_text.GetBitmapHeight(),
		100 * scale_x, 300 * scale_y,
		(float)option_text.GetBitmapWidth() * scale_x,
		(float)option_text.GetBitmapHeight() * scale_y,
		NULL);

	enemies_text.DrawScaledImage(0, 0,
		enemies_text.GetBitmapWidth(),
		enemies_text.GetBitmapHeight(),
		100 * scale_x, 400 * scale_y,
		(float)enemies_text.GetBitmapWidth() * scale_x,
		(float)enemies_text.GetBitmapHeight() * scale_y,
		NULL);

	kirby_text.DrawScaledImage(0, 0,
		kirby_text.GetBitmapWidth(),
		kirby_text.GetBitmapHeight(),
		100 * scale_x, 500 * scale_y,
		(float)kirby_text.GetBitmapWidth() * scale_x,
		(float)kirby_text.GetBitmapHeight() * scale_y,
		NULL);

	quit_text.DrawScaledImage(0, 0,
		quit_text.GetBitmapWidth(),
		quit_text.GetBitmapHeight(),
		100 * scale_x, 600 * scale_y,
		(float)quit_text.GetBitmapWidth() * scale_x,
		(float)quit_text.GetBitmapHeight() * scale_y,
		NULL);

	cursor_image.DrawScaledImage(cursor_animation.GetCurrentFrame().GetSourceX(),
		cursor_animation.GetCurrentFrame().GetSourceY(),
		cursor_animation.GetCurrentFrame().GetWidth(),
		cursor_animation.GetCurrentFrame().GetHeight(),
		cursor_animation.GetCurrentFrame().GetPositionX() * scale_x,
		(cursor_animation.GetCurrentFrame().GetPositionY() + (cursor_y_position * 100)) * scale_y,
		(float)cursor_animation.GetCurrentFrame().GetWidth() * scale_x,
		(float)cursor_animation.GetCurrentFrame().GetHeight() * scale_y,
		NULL);
}

void MainMenu::MoveCursorDown()
{
	if (cursor_y_position + 1 < 6)
	{
		cursor_y_position += 1;
		moving_cursor.PlaySongOnce();
	}
}

void MainMenu::MoveCursorUp()
{
	if (cursor_y_position - 1 > -1)
	{
		cursor_y_position -= 1;
		moving_cursor.PlaySongOnce();
	}
}

int MainMenu::GetCursorSelection()
{
	select_option.PlaySongOnce(); 
	return cursor_y_position;
}