#include "Unit.h"

#include "Stats.h"

#include <iostream>
#include <fstream>
#include <string.h>

Unit::Unit()
{
}


Unit::~Unit()
{
}

void Unit::SetUpAnimationHeroStats(int type)
{
	vincentio_font16.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 16);
	vincentio_font24.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 24);

	health_stat_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), std::to_string(hit_points));

	health_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), "Health");

	level_stat_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), std::to_string(level));

	level_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), "Level: ");

	name_text.CreateTextRender(vincentio_font24.ObtainFont(), al_map_rgb(0, 0, 0), name);

	attack_stat_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), std::to_string(attack_points));
	
	attack_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), "Attack: ");

	special_attack_stat_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), std::to_string(special_attack_points));

	special_attack_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), "Sp. Atk: ");

	defence_stat_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), std::to_string(defence_points));

	defence_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), "Defence: ");

	special_defence_special_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), std::to_string(special_defence_points));

	special_defence_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), "Sp. Def: ");

	speed_stat_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), std::to_string(speed_points));

	speed_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0),"Speed: ");

	experience_stat_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), std::to_string(experience_points));

	experience_text.CreateTextRender(vincentio_font16.ObtainFont(), al_map_rgb(0, 0, 0), "Exp: ");

	background_stat_animation.AddIndividualFrame(0, 0, 3, 69, 248, 224);
	background_stat_animation.AddIndividualFrame(0, 0, 267, 69, 248, 224);
	background_stat_animation.CombineFrames(10);

	background_stat_image.LoadBitmap("Content/Miscellaneous/SNES - Kirby Super Star Kirbys Fun Pak - Beginners Show.png");
}

void Unit::SetUpAnimationHeroUnit(int type)
{
	switch (type)
	{
	case 1:
		walking_animation.AddIndividualFrame(0, 0, 8, 51, 27, 25);
		walking_animation.AddIndividualFrame(0, 0, 40, 51, 23, 24);
		walking_animation.AddIndividualFrame(0, 0, 71, 50, 22, 25);
		walking_animation.AddIndividualFrame(0, 0, 100, 50, 22, 25);
		break;
	case 2:
		break;
	case 3:
		break;
	case 4:
		break;
	case 5:
		break;
	case 6:
		break;
	case 7:
		break;
	case 8:
		break;
	case 9:
		break;
	case 10:
		break;
	default:
		printf("Hero type %d does not Exist!\n", type);
		break;
	}
}

void Unit::CreateHeroUnit(int type)
{
	Stats animal_kirby_stats;

	animal_kirby_stats.LoadStatKirbies(type);

	// Loads in all the base stats for the hero unit
	name = animal_kirby_stats.GetName();							
	level = 1;
	experience_points = 0;
	hit_points = 10 + animal_kirby_stats.GetHitPointsGrowth();
	max_hit_points = hit_points;
	attack_points = animal_kirby_stats.GetAttackGrowth();
	special_attack_points = animal_kirby_stats.GetSpecialAttackGrowth();
	defence_points = animal_kirby_stats.GetDefenceGrowth();
	special_defence_points = animal_kirby_stats.GetSpecialDefenceGrowth();
	speed_points = animal_kirby_stats.GetSpeedGrowth();
}

void Unit::CreateEnemyUnit(int type, int level)
{

	Stats enemy_kirby_stats;

	enemy_kirby_stats.LoadStatEnemies(type);

	// Loads in all the base stats for the hero unit
	name = enemy_kirby_stats.GetName();
	Unit::level = level;
	experience_points = 0;
	hit_points = enemy_kirby_stats.GetHitPointsGrowth() * level;
	attack_points = enemy_kirby_stats.GetAttackGrowth() * level;
	special_attack_points = enemy_kirby_stats.GetSpecialAttackGrowth() * level;
	defence_points = enemy_kirby_stats.GetDefenceGrowth() * level;
	special_defence_points = enemy_kirby_stats.GetSpecialDefenceGrowth() * level;
	speed_points = enemy_kirby_stats.GetSpeedGrowth() * level;
}

void Unit::SaveHeroUnit(int type)
{
	std::ofstream  unit_file;

	switch (type)
	{
	case 1:
		remove("Content/Text Files/Owned Units/Animal Kirby.txt");
		unit_file.open("Content/Text Files/Owned Units/Animal Kirby.txt");
		break;
	case 2:
		remove("Content/Text Files/Owned Units/Fighter Kirby.txt");
		unit_file.open("Content/Text Files/Owned Units/Fighter Kirby.txt");
		break;
	case 3:
		remove("Content/Text Files/Owned Units/Cutter Kirby.txt");
		unit_file.open("Content/Text Files/Owned Units/Cutter Kirby.txt");
		break;
	case 4:
		remove("Content/Text Files/Owned Units/Fire Kirby.txt");
		unit_file.open("Content/Text Files/Owned Units/Fire Kirby.txt");
		break;
	case 5:
		remove("Content/Text Files/Owned Units/Kirby.txt");
		unit_file.open("Content/Text Files/Owned Units/Kirby.txt");
		break;
	case 6:
		remove("Content/Text Files/Owned Units/Ninja Kirby.txt");
		unit_file.open("Content/Text Files/Owned Units/Ninja Kirby.txt");
		break;
	case 7:
		remove("Content/Text Files/Owned Units/Jet Kirby.txt");
		unit_file.open("Content/Text Files/Owned Units/Jet Kirby.txt");
		break;
	case 8:
		remove("Content/Text Files/Owned Units/Ice Kirby.txt");
		unit_file.open("Content/Text Files/Owned Units/Ice Kirby.txt");
		break;
	case 9:
		remove("Content/Text Files/Owned Units/Stone Kirby.txt");
		unit_file.open("Content/Text Files/Owned Units/Stone Kirby.txt");
		break;
	case 10:
		remove("Content/Text Files/Owned Units/Plasma Kirby.txt");
		unit_file.open("Content/Text Files/Owned Units/Plasma Kirby.txt");
		break;
	default:
		printf("Invalid Hero Unit Can Not Create File!!!\n");
		break;
	}

	unit_file << name << std::endl;
	unit_file << level << std::endl;
	unit_file << max_hit_points << std::endl;
	unit_file << hit_points << std::endl;
	unit_file << attack_points << std::endl;
	unit_file << special_attack_points << std::endl;
	unit_file << defence_points << std::endl;
	unit_file << special_defence_points << std::endl;
	unit_file << speed_points << std::endl;
	unit_file << experience_points << std::endl;

}

void Unit::SaveEnemyUnit(int type)
{
	std::ofstream  unit_file;

	switch (type)
	{
	case 1:
		unit_file.open("Content/Text Files/Enemy Units/Ninja Enemy.txt");
		break;
	case 2:
		unit_file.open("Content/Text Files/Enemy Units/Sword Knight Enemy.txt");
		break;
	case 3:
		unit_file.open("Content/Text Files/Enemy Units/Wheele Enemy.txt");
		break;
	case 4:
		unit_file.open("Content/Text Files/Enemy Units/Gim Enemy.txt");
		break;
	case 5:
		unit_file.open("Content/Text Files/Enemy Units/Mr.Tick Tock Enemy.txt");
		break;
	case 6:
		unit_file.open("Content/Text Files/Enemy Units/Plasma Enemy.txt");
		break;
	case 7:
		unit_file.open("Content/Text Files/Enemy Units/Chilly Enemy.txt");
		break;
	case 8:
		unit_file.open("Content/Text Files/Enemy Units/Burning Leo Enemy.txt");
		break;
	case 9:
		unit_file.open("Content/Text Files/Enemy Units/Tac Enemy.txt");
		break;
	case 10:
		unit_file.open("Content/Text Files/Enemy Units/Fire Lion Enemy.txt");
		break;
	default:
		printf("Invalid Hero Unit Can Not Create File!!!\n");
		break;
	}

	unit_file << name << std::endl;
	unit_file << level << std::endl;
	unit_file << hit_points << std::endl;
	unit_file << attack_points << std::endl;
	unit_file << special_attack_points << std::endl;
	unit_file << defence_points << std::endl;
	unit_file << special_defence_points << std::endl;
	unit_file << speed_points << std::endl;
}

void Unit::LoadHeroUnit(int type)
{
	std::ifstream  unit_file;

	switch (type)
	{
	case 1:
		unit_file.open("Content/Text Files/Owned Units/Animal Kirby.txt");
		break;
	case 2:
		unit_file.open("Content/Text Files/Owned Units/Fighter Kirby.txt");
		break;
	case 3:
		unit_file.open("Content/Text Files/Owned Units/Cutter Kirby.txt");
		break;
	case 4:
		unit_file.open("Content/Text Files/Owned Units/Fire Kirby.txt");
		break;
	case 5:
		unit_file.open("Content/Text Files/Owned Units/Kirby.txt");
		break;
	case 6:
		unit_file.open("Content/Text Files/Owned Units/Ninja Kirby.txt");
		break;
	case 7:
		unit_file.open("Content/Text Files/Owned Units/Jet Kirby.txt");
		break;
	case 8:
		unit_file.open("Content/Text Files/Owned Units/Ice Kirby.txt");
		break;
	case 9:
		unit_file.open("Content/Text Files/Owned Units/Stone Kirby.txt");
		break;
	case 10:
		unit_file.open("Content/Text Files/Owned Units/Plasma Kirby.txt");
		break;
	default:
		printf("Invalid Hero Unit Can Not Create File!!!\n");
		break;
	}

	unit_file >> name;												// Load the name of the unit
	unit_file >> level;												// Load the level of the unit
	unit_file >> max_hit_points;									// Load how much total hit points the unit has
	unit_file >> hit_points;										// Load how much hit points the unit has
	unit_file >> attack_points;										// Load how much attack points the unit has
	unit_file >> special_attack_points;								// Load how much special attack points the unit has
	unit_file >> defence_points;									// Load how much defence points the unit has
	unit_file >> special_defence_points;							// Load how much special defence points the unit has
	unit_file >> speed_points;										// Load how much speed points the unit has
	unit_file >> experience_points;									// Loads how much experience the unit as has
}

void Unit::LoadEnemyUnit(int type)
{
	std::ifstream unit_file;

	switch (type)
	{
	case 1:
		unit_file.open("Content/Text Files/Enemy Units/Ninja Enemy.txt");
		break;
	case 2:
		unit_file.open("Content/Text Files/Enemy Units/Sword Knight Enemy.txt");
		break;
	case 3:
		unit_file.open("Content/Text Files/Enemy Units/Wheele Enemy.txt");
		break;
	case 4:
		unit_file.open("Content/Text Files/Enemy Units/Gim Enemy.txt");
		break;
	case 5:
		unit_file.open("Content/Text Files/Enemy Units/Mr.Tick Tock Enemy.txt");
		break;
	case 6:
		unit_file.open("Content/Text Files/Enemy Units/Plasma Enemy.txt");
		break;
	case 7:
		unit_file.open("Content/Text Files/Enemy Units/Chilly Enemy.txt");
		break;
	case 8:
		unit_file.open("Content/Text Files/Enemy Units/Burning Leo Enemy.txt");
		break;
	case 9:
		unit_file.open("Content/Text Files/Enemy Units/Tac Enemy.txt");
		break;
	case 10:
		unit_file.open("Content/Text Files/Enemy Units/Fire Lion Enemy.txt");
		break;
	default:
		printf("Invalid Hero Unit Can Not Create File!!!\n");
		break;
	}

	unit_file >> name;												// Load the name of the unit
	unit_file >> level;												// Load what level the unit is
	unit_file >> hit_points;										// Load how much hit points the unit has
	unit_file >> attack_points;										// Load how much attack points the unit has
	unit_file >> special_attack_points;								// Load how much special attack points the unit has
	unit_file >> defence_points;									// Load how much defence points the unit has
	unit_file >> special_defence_points;							// Load how much special defence points the unit has
	unit_file >> speed_points;										// Load how much speed points the unit has
}

void Unit::RenderUnitInfo(float  position_x, float position_y, float scale_x, float scale_y)
{
	background_stat_image.DrawScaledImage(background_stat_animation.GetCurrentFrame().GetSourceX(),
		background_stat_animation.GetCurrentFrame().GetSourceY(),
		background_stat_animation.GetCurrentFrame().GetWidth(),
		background_stat_animation.GetCurrentFrame().GetHeight(),
		position_x * scale_x,
		position_y * scale_y,
		background_stat_animation.GetCurrentFrame().GetWidth() * scale_x,
		background_stat_animation.GetCurrentFrame().GetHeight() * scale_y,
		NULL);

	name_text.DrawScaledImage(0,0,
		name_text.GetBitmapWidth(),
		name_text.GetBitmapHeight(),
		(position_x  - name_text.GetBitmapWidth() + 200) * scale_x,
		(position_y + 35) * scale_y,
		name_text.GetBitmapWidth() * scale_x,
		name_text.GetBitmapHeight() * scale_y,
		NULL);

	level_text.DrawScaledImage(0, 0,
		level_text.GetBitmapWidth(),
		level_text.GetBitmapHeight(),
		(position_x + 50) * scale_x,
		(position_y + 60) * scale_y,
		level_text.GetBitmapWidth(),
		level_text.GetBitmapHeight(),
		NULL);

	level_stat_text.DrawScaledImage(0, 0,
		level_stat_text.GetBitmapWidth(),
		level_stat_text.GetBitmapHeight(),
		(position_x + 100) * scale_x,
		(position_y + 60) * scale_y,
		level_stat_text.GetBitmapWidth(),
		level_stat_text.GetBitmapHeight(),
		NULL);

	experience_text.DrawScaledImage(0, 0,
		experience_text.GetBitmapWidth(),
		experience_text.GetBitmapHeight(),
		(position_x + 135) * scale_x,
		(position_y + 60) * scale_y,
		experience_text.GetBitmapWidth(),
		experience_text.GetBitmapHeight(),
		NULL);

	experience_stat_text.DrawScaledImage(0, 0,
		experience_stat_text.GetBitmapWidth(),
		experience_stat_text.GetBitmapHeight(),
		(position_x + 170) * scale_x,
		(position_y + 60) * scale_y,
		experience_stat_text.GetBitmapWidth(),
		experience_stat_text.GetBitmapHeight(),
		NULL);

	attack_text.DrawScaledImage(0, 0,
		attack_text.GetBitmapWidth(),
		attack_text.GetBitmapHeight(),
		(position_x + 80) * scale_x,
		(position_y + 80) * scale_y,
		attack_text.GetBitmapWidth(),
		attack_text.GetBitmapHeight(),
		NULL);

	attack_stat_text.DrawScaledImage(0, 0,
		attack_stat_text.GetBitmapWidth(),
		attack_stat_text.GetBitmapHeight(),
		(position_x + 150) * scale_x,
		(position_y + 77) * scale_y,
		attack_stat_text.GetBitmapWidth(),
		attack_stat_text.GetBitmapHeight(),
		NULL);

	special_attack_text.DrawScaledImage(0, 0,
		special_attack_text.GetBitmapWidth(),
		special_attack_text.GetBitmapHeight(),
		(position_x + 80) * scale_x,
		(position_y + 100) * scale_y,
		special_attack_text.GetBitmapWidth(),
		special_attack_text.GetBitmapHeight(),
		NULL);

	special_attack_stat_text.DrawScaledImage(0, 0,
		special_attack_stat_text.GetBitmapWidth(),
		special_attack_stat_text.GetBitmapHeight(),
		(position_x + 150) * scale_x,
		(position_y + 97) * scale_y,
		special_attack_stat_text.GetBitmapWidth(),
		special_attack_stat_text.GetBitmapHeight(),
		NULL);

	defence_text.DrawScaledImage(0, 0,
		defence_text.GetBitmapWidth(),
		defence_text.GetBitmapHeight(),
		(position_x + 80) * scale_x,
		(position_y + 120) * scale_y,
		defence_text.GetBitmapWidth(),
		defence_text.GetBitmapHeight(),
		NULL);

	defence_stat_text.DrawScaledImage(0, 0,
		defence_stat_text.GetBitmapWidth(),
		defence_stat_text.GetBitmapHeight(),
		(position_x + 155) * scale_x,
		(position_y + 120) * scale_y,
		defence_stat_text.GetBitmapWidth(),
		defence_stat_text.GetBitmapHeight(),
		NULL);

	special_defence_text.DrawScaledImage(0, 0,
		special_defence_text.GetBitmapWidth(),
		special_defence_text.GetBitmapHeight(),
		(position_x + 80) * scale_x,
		(position_y + 140) * scale_y,
		special_defence_text.GetBitmapWidth(),
		special_defence_text.GetBitmapHeight(),
		NULL);

	special_defence_special_text.DrawScaledImage(0, 0,
		special_defence_special_text.GetBitmapWidth(),
		special_defence_special_text.GetBitmapHeight(),
		(position_x + 150) * scale_x,
		(position_y + 140) * scale_y,
		special_defence_special_text.GetBitmapWidth(),
		special_defence_special_text.GetBitmapHeight(),
		NULL);

	speed_text.DrawScaledImage(0, 0,
		speed_text.GetBitmapWidth(),
		speed_text.GetBitmapHeight(),
		(position_x + 80) * scale_x,
		(position_y + 160) * scale_y,
		speed_text.GetBitmapWidth(),
		speed_text.GetBitmapHeight(),
		NULL);

	speed_stat_text.DrawScaledImage(0, 0,
		speed_stat_text.GetBitmapWidth(),
		speed_stat_text.GetBitmapHeight(),
		(position_x + 150) * scale_x,
		(position_y + 160) * scale_y,
		speed_stat_text.GetBitmapWidth(),
		speed_stat_text.GetBitmapHeight(),
		NULL);



}

void Unit::UpdateUnitAnimation()
{
	background_stat_animation.Update();
}