#include "TitleScreen.h"

TitleScreen::TitleScreen()
{
	SetUpTitleScreen();
}


TitleScreen::~TitleScreen()
{
	title_intro_song.StopSong();
}

void TitleScreen::SetUpTitleScreen()
{
	title_image.LoadBitmap("Content/Backgrounds/SNES - Kirby Super Star Kirbys Fun Pak - Bubbly Clouds.png");
	
	title_animation.AddIndividualFrame(0, 0, 3, 2, 256, 223);
	title_animation.AddIndividualFrame(0, 0, 264, 2, 256, 223);
	title_animation.AddIndividualFrame(0, 0, 525, 2, 256, 223);

	title_animation.CombineFrames(20);

	kirby_image.LoadBitmap("Content/Kirby Characters/SNES - Kirby Super Star Kirbys Fun Pak - Kirby.png");

	kirby_dancing.AddIndividualFrame(0, 0, 62, 26, 21, 24);
	kirby_dancing.AddIndividualFrame(0, 0, 88, 26, 21, 24);
	kirby_dancing.AddIndividualFrame(0, 0, 114, 26, 21, 24);
	kirby_dancing.AddIndividualFrame(0, 0, 141, 26, 21, 24);
	kirby_dancing.AddIndividualFrame(0, 0, 167, 26, 21, 24);
	kirby_dancing.AddIndividualFrame(0, 0, 193, 31, 24, 19);
	kirby_dancing.AddIndividualFrame(0, 0, 226, 26, 24, 24);

	kirby_dancing.CombineFrames(10);

	title_intro_song.LoadAudioFiles("Content/Music/Tspeiro_-_Kirby_-_Green_Greens_Tspeiro_REMIX_.wav");
	title_intro_song.PlaySongLoop();

	vincentio_font24.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 24);

	start_text.CreateTextRender(vincentio_font24.ObtainFont(), al_map_rgb(0, 0, 0), "Press Enter/Start");

	vincentio_font76.CreateFont("Content/Fonts/vincentio/Vincentio.ttf", 76);

	title_text.CreateTextRender(vincentio_font76.ObtainFont(), al_map_rgb(0, 0, 0), "Kirby RPG");
}

void TitleScreen::UpdateTitleScreen()
{
	title_animation.Update();
	kirby_dancing.Update();
}

void TitleScreen::RenderTitleScreen(float scale_x, float scale_y)
{
	title_image.DrawScaledImage(title_animation.GetCurrentFrame().GetSourceX(),
		title_animation.GetCurrentFrame().GetSourceY(),
		title_animation.GetCurrentFrame().GetWidth(),
		title_animation.GetCurrentFrame().GetHeight(), 
		title_animation.GetCurrentFrame().GetPositionX(), 
		title_animation.GetCurrentFrame().GetPositionY(),
		(float) title_animation.GetCurrentFrame().GetWidth() * 5 * scale_x, 
		(float) title_animation.GetCurrentFrame().GetHeight() * 3.22 * scale_y, NULL);

	kirby_image.DrawScaledImage(kirby_dancing.GetCurrentFrame().GetSourceX(),
		kirby_dancing.GetCurrentFrame().GetSourceY(),
		kirby_dancing.GetCurrentFrame().GetWidth(),
		kirby_dancing.GetCurrentFrame().GetHeight(),
		400 * scale_x, 500 * scale_y,
		(float)kirby_dancing.GetCurrentFrame().GetWidth() * 3 * scale_x,
		(float)kirby_dancing.GetCurrentFrame().GetHeight() * 3 * scale_y,
		NULL);

	kirby_image.DrawScaledImage(kirby_dancing.GetCurrentFrame().GetSourceX(),
		kirby_dancing.GetCurrentFrame().GetSourceY(),
		kirby_dancing.GetCurrentFrame().GetWidth(),
		kirby_dancing.GetCurrentFrame().GetHeight(),
		800 * scale_x, 500 * scale_y,
		(float)kirby_dancing.GetCurrentFrame().GetWidth() * 3 * scale_x,
		(float)kirby_dancing.GetCurrentFrame().GetHeight() * 3 * scale_y,
		NULL);

	start_text.DrawScaledImage(0, 0,
		start_text.GetBitmapWidth(),
		start_text.GetBitmapHeight(),
		550 * scale_x, 500 * scale_y,
		(float)start_text.GetBitmapWidth() * scale_x,
		(float)start_text.GetBitmapHeight() * scale_y,
		NULL);

	title_text.DrawScaledImage(0, 0,
		title_text.GetBitmapWidth(),
		title_text.GetBitmapHeight(),
		450 * scale_x, 200 * scale_y,
		(float)title_text.GetBitmapWidth() * scale_x,
		(float)title_text.GetBitmapHeight() * scale_y,
		NULL);
}

