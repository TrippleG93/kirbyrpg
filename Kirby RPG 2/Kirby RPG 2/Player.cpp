#include "Player.h"
#include "Unit.h"

#include <fstream>
#include <iostream>
#include <cstdlib>
#include <cstdio>

Player::Player()
{
	ReadEnemiesEncounteredFile();
	ReadKirbiesUnlockedFile();
}


Player::~Player()
{
	enemies_encountered.clear();
	kirbies_unlocked.clear();
}

void Player::LoadPlayer()
{

}

void Player::ReadEnemiesEncounteredFile()
{
	std::ifstream file;

	file.open("Content/Text Files/Enemies Encountered/Enemies Encountered.txt");

	if (file.is_open() == false)
	{
		printf("Enemies Unlocked File has not been opened!\n");
	}
	else
	{
		file >> max_enemy_types;

		int enemy_type;
		int exist;
		while (!file.eof())
		{
			file >> enemy_type;								// Holds the enemy type
			file >> exist;									// Holds if the enemy has been encountered or not

			if (exist == 1)									// If the player has unlocked the enemy
			{
				enemies_encountered[enemy_type] = true;		// Then mark true
			}
			else
			{
				enemies_encountered[enemy_type] = false;	// If not then mark false
			}

		}
	}

	file.close();
}

void Player::ReadKirbiesUnlockedFile()
{
	std::ifstream file;

	file.open("Content/Text Files/Kirbies Unlocked/Kirbies Unlocked.txt");

	if (file.is_open() == false)
	{
		printf("Kirbies Unlocked File has not been opened!\n");
	}
	else
	{
		file >> max_kirbie_types;

		int kirby_type;
		int exist;
		while (!file.eof())
		{
			file >> kirby_type;								// Holds in the type of kirby
			file >> exist;									// Holds if the kirby exist or not

			if (exist == 1)									// If the player has unlocked the kirby
			{
				kirbies_unlocked[kirby_type] = true;		// Then mark true
			}
			else
			{
				kirbies_unlocked[kirby_type] = false;		// If not then mark false
			}
			
		}
	}
}

bool Player::DidPlayerEncounterEnemy(int enemy_type)
{
	if (enemies_encountered.count(enemy_type) != 0)
	{
		return enemies_encountered[enemy_type];
	}
	else
	{
		printf("Enemy type: %d does not exist\n", enemy_type);
		return false;
	}
}

bool Player::DidPlayerUnlockKirby(int kirby_type)
{
	if (kirbies_unlocked.count(kirby_type) != 0)
	{
		return kirbies_unlocked[kirby_type];
	}
	else
	{
		printf("Kirby type: %ddoes not exist\n", kirby_type);
	}
}

bool Player::HasPlayerUnlockedAKirby()
{
	for (auto const& x : kirbies_unlocked)
	{
		if (x.second == true)
		{
			return true;
		}
	} // End for loop

	return false;
}

void Player::UnlockRandomKirby()
{
}

void Player::UnlockAKirby(int type)
{
	kirbies_unlocked[type] = true;

	Unit unlock_hero;
	unlock_hero.CreateHeroUnit(type);
	unlock_hero.SaveHeroUnit(type);
	SaveUnlockedKirbiesFile();
}

void Player::UnlockAnEnemy(int type)
{
	enemies_encountered[type] = true;
}

void Player::SaveUnlockedKirbiesFile()
{
	std::ofstream  unit_file;

	remove("Content/Text Files/Kirbies Unlocked/Kirbies Unlocked.txt");
	unit_file.open("Content/Text Files/Kirbies Unlocked/Kirbies Unlocked.txt");

	unit_file << max_kirbie_types << std::endl;

	for (auto const& x : kirbies_unlocked)
	{
		unit_file << x.first;
		unit_file << "\t";
		unit_file << x.second << std::endl;
	} // End for loop
}

void Player::SaveEnemiesEncounteredFile()
{
	std::ofstream  unit_file;

	remove("Content/Text Files/Enemies Encountered/Enemies Encountered.txt");
	unit_file.open("Content/Text Files/Enemies Encountered/Enemies Encountered.txt");

	unit_file << max_enemy_types << std::endl;

	for (auto const& x : enemies_encountered)
	{
		unit_file << x.first;
		unit_file << "\t" << std::endl;
		unit_file << x.second << std::endl;
	} // End for loop
}