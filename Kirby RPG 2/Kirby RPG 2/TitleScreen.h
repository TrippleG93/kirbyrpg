#pragma once

#include "SpriteAnimation.h"
#include "Render.h"
#include "Audio.h"
#include "Font.h"

class TitleScreen
{
public:
	TitleScreen();
	~TitleScreen();

	/* SetUpTitleScreen
	 * Loads everything needed for the start menu
	 */
	void SetUpTitleScreen();

	/* RenderTitleScreen
	 * Draws everything for the title screen
	 */
	void RenderTitleScreen(float scale_x, float scale_y);

	/* UpdateTitleScreen
	 * Updates animations and everything else
	 */
	void UpdateTitleScreen();


private:

	SpriteAnimation title_animation;
	SpriteAnimation kirby_dancing;

	Render start_text;
	Render title_text;
	Render kirby_image;
	Render title_image;

	Audio title_intro_song;

	Font vincentio_font24;
	Font vincentio_font76;
};

